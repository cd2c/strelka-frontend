const sm = require('sitemap'),
      fs = require('fs');

const sitemap = sm.createSitemap({
  hostname: 'http://strelka-osago.ru',
  cacheTime: 600000,
  urls: [
    { url: '/' },
    { url: '/vehicle' },
  ],
});

fs.writeFileSync('./app/Sitemap.xml', sitemap.toString());
