import moment from 'moment';

export const validator = {
  required: (value) => (
    value && value.length !== 0
  ),
  carYear: (value) => (
    !moment(value, 'YYYY').isAfter(moment(), 'year') &&
    !moment(value, 'YYYY').isBefore(moment().subtract(50, 'y'), 'year')
  ),
  licensePlate: (value) => (
    value !== false &&
    (value.length === 11 || value.length === 12) &&
    (/[а-яА-ЯЁё] [0-9]{3} [а-яА-ЯЁё]{2} [0-9]{2,}/.test(value))
  ),
  serial: (value) => (
    value !== false &&
    value.length === 4
  ),
  number: (value) => (
    value !== false &&
    /[0-9]{6}/.test(value)
  ),
  carDocDate: (value, carYear) => (
    moment(value, 'DD.MM.YYYY').isValid() &&
    !moment(value, 'DD.MM.YYYY').isAfter(moment()) &&
    !moment(value, 'DD.MM.YYYY').isBefore(moment(carYear, 'YYYY'), 'year')
  ),
  vin: (value) => (
    value !== false
  ),
  engCap: (value) => (
    value !== false &&
    /[0-9]{2,}/.test(value)
  ),
  ticketDate: (value, year) => (
    (moment(year, 'YYYY').isAfter(moment().subtract(3, 'y'))) ||
    (moment(value, 'DD.MM.YYYY').isValid() &&
    moment(value, 'DD.MM.YYYY').isAfter(moment()))
  ),
  ticketNumber: (value, year) => (
    (moment(year, 'YYYY').isAfter(moment().subtract(3, 'y'))) ||
    /[0-9]{12,15}/.test(value)
  ),
  name: (value) => (
    value !== false &&
    /[A-ЯЁ]{1}[а-яA-ЯЁё]+/.test(value)
  ),
  surname: (value) => (
    value !== false &&
    /[A-ЯЁ]{1}[а-яA-ЯЁё]+/.test(value)
  ),
  patronymic: (value) => (
    value === false ||
    value.length === 0 ||
    /[A-ЯЁ]{1}[а-яA-ЯЁё]+/.test(value)
  ),
  ownerBirthday: (value) => (
    moment(value, 'DD.MM.YYYY').isValid() &&
    moment(value, 'DD.MM.YYYY').isBefore(moment())
  ),
  ownerSerial: (value) => (
    value !== false &&
    /[0-9]{4}/.test(value)
  ),
  ownerNumber: (value) => (
    value !== false &&
    /[0-9]{6}/.test(value)
  ),
  driverBirthday: (value) => (
    value !== false &&
    moment(value, 'DD.MM.YYYY').isValid() &&
    moment().diff(moment(value, 'DD.MM.YYYY'), 'y') >= 18
  ),
  driverDocDate: (value, birthday) => (
    value !== false &&
    moment(value, 'DD.MM.YYYY').isValid() &&
    moment(value, 'DD.MM.YYYY').diff(moment(birthday, 'DD.MM.YYYY'), 'y') >= 18
  ),
  driverSerial: (value) => (
    value !== false && (value.length === 3 || value.length === 4)
  ),
  driverNumber: (value) => (
    value !== false &&
    value.length === 6
  ),
  dateStart: (value) => (
    value !== false &&
    moment(value, 'DD.MM.YYYY').isAfter(moment())
  ),
  datePoliceEnd: (value) => (
    value !== false &&
    /[0-9]{2}.[0-9]{2}.[0-9]{4}/.test(value) &&
    moment(value, 'DD.MM.YYYY').isAfter(moment())
  ),
  email: (value) => (
    value !== false &&
    /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value) // eslint-disable-line no-use-before-define
  ),
  phone: (value) => (
    value !== false &&
    /\+7 9[0-9]{2} [0-9]{3}-[0-9]{2}-[0-9]{2}/.test(value)
  ),
};
