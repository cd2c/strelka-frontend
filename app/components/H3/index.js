import styled from 'styled-components';

const H3 = styled.h3`
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  letter-spacing: -0.5px;
  margin: 40px 0 10px;

  &.centered {
    text-align: center;
  }

  @media (max-width: 840px) {
    margin: 0;
  }
`;

export default H3;
