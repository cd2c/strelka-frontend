import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import isUndefined from 'lodash/isUndefined';

const Block = styled.div`
  background: #FFFFFF;
  box-shadow: 0 0 24px 0 rgba(0, 0, 0, .1);
  display: flex;
  margin: 20px auto;
  padding: 40px 0 8px;
  min-width: 320px;
  max-width: 800px;
  width: 100%;

  @media (max-width: 840px) {
    box-shadow: none;
    display: ${(props) => props.hideOnMobile ? 'none' : 'flex'};
    margin: 0 auto;

    &:nth-last-child(2) {
      margin-bottom: 40px;
    }
  }
`;

const BlockInner = styled.div`
  width: ${(props) => props.withImage ? '400px' : '100%'};

  @media (max-width: 840px) {
    width: 100%;
  }
`;

const BlockImage = styled.div`
  background-image: url(${(props) => isUndefined(props.image) ? 'none' : props.image});
  background-position: center bottom;
  background-size: contain;
  height: auto;
  margin-bottom: 11px;
  width: ${(props) => isUndefined(props.image) ? '0' : '200px'};

  @media (max-width: 840px) {
    display: none;
  }
`;

const Inner = styled.div`
  display: flex;
  margin: auto;
  width: calc((100px + 100%) / 1.5);
`;

const LandingBlock = (props) => (
  <Block id={props.id} hideOnMobile={props.hideOnMobile}>
    <Inner>
      <BlockInner withImage={!isUndefined(props.image)}>
        {props.children}
      </BlockInner>
      <BlockImage image={props.image} />
    </Inner>
  </Block>
);

LandingBlock.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  id: PropTypes.string,
  image: PropTypes.string,
  hideOnMobile: PropTypes.bool,
};

export default LandingBlock;
