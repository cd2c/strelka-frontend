import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import NormalH3 from 'components/H3';

const StepWrapper = styled.div`
  align-items: center;
  background-color: #FFFFFF;
  border-bottom: 1px solid #ddd;
  display: flex;
  flex-direction: row;
  height: 80px;
  justify-content: space-between;
  margin: 0 auto;
  max-width: 800px;
  min-width: 320px;
  padding: 0 100px;
  position: ${(props) => props.position};
  top: 0;
  width: 100%;
  z-index: 1000;

  @media (max-width: 840px) {
    padding: 0;
    width: 100%;
  }
`;

const Inner = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  height: 100%;
  justify-content: space-between;
  width: 100%;

  @media (max-width: 840px) {
    margin: 0 auto;
    width: calc((100px + 100%) / 1.5);
  }
`;

const Icon = styled.div`
  height: 40px;
  display: flex;
  flex-direction: row;
  align-items: center;

  &.success {
    height: 62px;
    align-self: flex-end;
  }

  & svg {
    width: 40px;
    height: 40px;
  }

  & .vehicle-cls-1 {
    fill: #ddd;
    fill-rule: evenodd;

    &.active {
      fill: #666;
    }
  }

  & .vehicle-cls-2 {
    fill: none;
    fill-rule: evenodd;
    stroke: #ddd;
    stroke-width: 2px;

    &.active {
      stroke: #666;
    }
  }

  & .owner-cls-1 {
    height: 40px;
    fill: none;
    stroke: #ddd;
    stroke-width: 2px;

    &.active {
      stroke: #666;
    }
  }

  & .owner-cls-2 {
    fill: #ddd;
    fill-rule: evenodd;

    &.active {
      fill: #666;
    }
  }

  & .driver-cls-1 {
    fill: none;
    stroke: #ddd;
    stroke-width: 2px;

    &.active {
      stroke: #666;
    }
  }

  & .driver-cls-2 {
    fill: #ddd;
    fill-rule: evenodd;

    &.active {
      fill: #666;
    }
  }

  & .payment-cls-1 {
    fill: none;
    stroke: #ddd;
    stroke-width: 2px;

    &.active {
      stroke: #666;
    }
  }

  & .payment-cls-2 {
    fill: #ddd;
    fill-rule: evenodd;

    &.active {
      fill: #666;
    }
  }

  & .success-svg {
    width: 62px;
    height: 62px;
  }

  & .success-cls-1 {
    fill: none;
    stroke: #666;
    stroke-width: 2px;
  }

  & .success-cls-2 {
    fill: #666;
    fill-rule: evenodd;
  }

  & .error-cls-1 {
    fill: none;
    stroke: #666;
    stroke-width: 4px;
  }

  & .error-cls-2 {
    fill: #666;
    fill-rule: evenodd;
  }
`;

const Text = styled.p`
  margin: 0 0 0 7px;
  padding: 0;
  max-width: 100px;
  font-size: 12px;
  font-family: 'Ubuntu', sans-serif;
  font-weight: 300;
  color: #666;
  line-height: normal;
`;

const H3 = styled(NormalH3)`
  margin: 0 0 0 23px;
  color: #07a1f4;
`;


class Step extends React.PureComponent {
  state = {
    position: (120 - window.scrollY < 0) ? 'fixed' : 'absolute',
  };

  componentDidMount = () => {
    document.addEventListener('scroll', this.onScrollListener);
  };

  componentWillUnmount = () => {
    document.removeEventListener('scroll', this.onScrollListener);
  };

  onScrollListener = () => {
    if (window.innerWidth <= 840) {
      this.setState({
        position: (90 - window.scrollY < 0) ? 'fixed' : 'absolute',
      });
    } else {
      this.setState({
        position: (120 - window.scrollY < 0) ? 'fixed' : 'absolute',
      });
    }
  };

  render() {
    let result;
    if (this.props.step === 'success') {
      result = (
        <StepWrapper position={this.state.position}>
          <Inner>
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" width="84" height="84" viewBox="0 0 84 84">
                <circle className="payment-cls-1 active" cx="42" cy="42" r="40" />
                <path className="payment-cls-2 active" d="M3094.05,2858h0a5.187,5.187,0,0,1-3.61-1.49l-12.99-12.96a5.116,5.116,0,0,1,7.24-7.23l9.36,9.35,20.23-20.19a5.112,5.112,0,0,1,7.23,7.23l-23.84,23.8A5.1,5.1,0,0,1,3094.05,2858Z" transform="translate(-3058.03 -2798)" />
              </svg>
              <Text>Полис оформлен</Text>
            </Icon>
          </Inner>
        </StepWrapper>
      );
    } else if (this.props.step === 'fail') {
      result = (
        <StepWrapper position={this.state.position}>
          <Inner>
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" width="124" height="124" viewBox="0 0 124 124">
                <circle id="Ellipse_1_copy_3" data-name="Ellipse 1 copy 3" className="error-cls-1" cx="62" cy="62" r="60" />
                <path className="error-cls-2" d="M9883.26,1899.5l30.54-30.28a7.83,7.83,0,0,1,10.94,0,7.659,7.659,0,0,1,0,10.83l-36.02,35.7a7.721,7.721,0,0,1-5.46,2.22h0a7.869,7.869,0,0,1-5.47-2.22" transform="translate(-9835.03 -1834)" />
                <path className="error-cls-2" d="M9909.85,1899.5l-30.62-30.28a7.855,7.855,0,0,0-10.96,0,7.641,7.641,0,0,0,0,10.83l36.1,35.7a7.769,7.769,0,0,0,5.48,2.22h0a7.875,7.875,0,0,0,5.48-2.22" transform="translate(-9835.03 -1834)" />
                <path className="error-cls-2" d="M9883.04,1894.47l30.35,30.28a7.672,7.672,0,0,0,10.86-10.84l-35.78-35.69a7.642,7.642,0,0,0-5.43-2.23h0a7.778,7.778,0,0,0-5.42,2.23" transform="translate(-9835.03 -1834)" />
                <path className="error-cls-2" d="M9909.53,1895.47l-30.42,30.28a7.769,7.769,0,0,1-10.89,0,7.689,7.689,0,0,1,0-10.84l35.87-35.69a7.666,7.666,0,0,1,5.44-2.23h0a7.807,7.807,0,0,1,5.44,2.23" transform="translate(-9835.03 -1834)" />
              </svg>
              <Text>Ошибка</Text>
            </Icon>
          </Inner>
        </StepWrapper>
      );
    } else {
      result = (
        <StepWrapper position={this.state.position}>
          <Inner>
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" width="83.97" height="84" viewBox="0 0 83.97 84">
                <path className={this.props.step === 'vehicle' ? 'vehicle-cls-1 active' : 'vehicle-cls-1'} d="M2505.21,2822.56a32.366,32.366,0,0,1,9.53,0c2.18,0.45,4.38.81,6.52,1.39a4.677,4.677,0,0,1,3.3,3.1q1.35,3.81,2.83,7.58a1.714,1.714,0,0,0,1.03,1.01c2.08-.24,3.06.9,3.7,2.58a2.9,2.9,0,0,1,0,1.38c-0.87.23-1.74,0.47-2.54,0.68a20.349,20.349,0,0,1,.32,2.69c0.04,4.1.07,8.2-.02,12.31a3.4,3.4,0,0,1-.75,2.05c-0.28.48-6.61,0.46-7.1,0.13-1.65-1.05-.63-2.81-1.16-4.24h-21.51c-0.41,1.51-.66,4.04-1.16,4.24-1.1.41-6.97,0.42-7.29,0a3.991,3.991,0,0,1-.81-2.36c-0.12-1.72-.05-3.46-0.03-5.2,0.03-3.22.08-6.45,0.12-9.69-0.61-.16-1.49-0.39-2.37-0.61a2.539,2.539,0,0,1,0-1.1c0.61-1.78,1.5-3.15,3.79-2.87a1.535,1.535,0,0,0,.93-0.93,60.143,60.143,0,0,0,2.18-6.07c0.93-3.48,3.5-4.88,6.77-5.5,1.23-.23,2.48-0.38,3.72-0.57h0Zm-9.83,13.74a70.02,70.02,0,0,0,29.08,0c-0.81-2.46-1.63-4.65-2.25-6.9a3.682,3.682,0,0,0-3.69-2.95c-4.61-.21-9.22-0.37-13.83-0.4a19.654,19.654,0,0,0-4.55.69,2.488,2.488,0,0,0-1.63,1.12c-1.1,2.69-2.04,5.45-3.13,8.44h0Zm1.9,10.49c0.01-.05,0,0.05,0,0a11.092,11.092,0,0,0,2.38-.2,2.2,2.2,0,0,0,1.41-1.02,1.643,1.643,0,0,0-.84-1.36,52.784,52.784,0,0,0-5.55-2.18c-0.94-.3-2-0.24-2.3,1.07-0.44,1.93-.03,3.01,1.44,3.31,1.13,0.23,2.31.26,3.46,0.38h0Zm25.61-.01a30.678,30.678,0,0,0,3.5-.44c1.66-.37,1.31-1.74,1.27-2.9-0.03-1-.66-1.74-1.57-1.47a49.627,49.627,0,0,0-6.7,2.54c-0.98.47-.63,1.77,0.49,2.01a24.865,24.865,0,0,0,3.01.26h0Z" transform="translate(-2468 -2798)" />
                <path className={this.props.step === 'vehicle' ? 'vehicle-cls-2 active' : 'vehicle-cls-2'} d="M2509.99,2800a40,40,0,1,1-39.99,40A39.991,39.991,0,0,1,2509.99,2800Z" transform="translate(-2468 -2798)" />
              </svg>
              { this.props.step === 'vehicle' ? (<Text>Информация об&nbsp;автомобиле</Text>) : ''}
            </Icon>
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" width="83.97" height="84" viewBox="0 0 83.97 84">
                <circle className={this.props.step === 'owner' ? 'owner-cls-1 active' : 'owner-cls-1'} cx="41.985" cy="42" r="39.985" />
                <path className={this.props.step === 'owner' ? 'owner-cls-2 active' : 'owner-cls-2'} d="M2911.83,2834.84c0,7.52-5.56,14.63-12.42,14.63s-12.42-7.11-12.42-14.63A12.422,12.422,0,1,1,2911.83,2834.84Zm-0.92,14.17a14.059,14.059,0,0,1-22.71-.18c-5.46,3.35-9.44,6.07-10.63,12.81,1.88,2.42,6.53,10.47,21.77,10.47,15.59,0,18.98-7.17,21.85-10.8C2919.95,2854.83,2916.14,2852.33,2910.91,2849.01Z" transform="translate(-2857.44 -2798)" />
              </svg>
              { this.props.step === 'owner' ? (<Text>Информация о&nbsp;собственнике и&nbsp;страхователе</Text>) : ''}
            </Icon>
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" width="83.97" height="84" viewBox="0 0 83.97 84">
                <circle className={this.props.step === 'driver' ? 'driver-cls-1 active' : 'driver-cls-1'} cx="41.985" cy="42" r="39.985" />
                <path className={this.props.step === 'driver' ? 'driver-cls-2 active' : 'driver-cls-2'} d="M2979.45,2824a4.43,4.43,0,0,0-4.45,4.4v24.2a4.43,4.43,0,0,0,4.45,4.4h40.1a4.43,4.43,0,0,0,4.45-4.4v-24.2a4.43,4.43,0,0,0-4.45-4.4h-40.1Zm2.23,4.4h13.36a2.213,2.213,0,0,1,2.23,2.2v17.19a33.621,33.621,0,0,0-4.8-2,1.488,1.488,0,0,1-1.53-1.58,8.635,8.635,0,0,0,2.71-4.6c0.8-.66,1.4-2.42.35-3.3-0.51-.88-0.54-2-1.25-3.1a5.636,5.636,0,0,0-4.39-2.4,6.287,6.287,0,0,0-3.89,1.78,8.781,8.781,0,0,0-1.68,3.51,2.263,2.263,0,0,0-.13,3.09c0.73,0.88.69,2,1.25,3.1,0.23,0.88,1.6,1.52,1.6,2.4,0.33,1.54-1.57,1.08-2.44,1.52a9.973,9.973,0,0,0-3.62,1.78V2830.6a2.213,2.213,0,0,1,2.23-2.2h0Zm20.05,2.2h8.91v2.2h-8.91v-2.2Zm0,4.4h15.59v2.2h-15.59V2835Zm0,4.4h15.59v2.2h-15.59v-2.2Z" transform="translate(-2957.72 -2798)" />
              </svg>
              { this.props.step === 'driver' ? (<Text>Информация о&nbsp;водителях</Text>) : ''}
            </Icon>
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" width="84" height="84" viewBox="0 0 84 84">
                <circle className={this.props.step === 'payment' ? 'payment-cls-1 active' : 'payment-cls-1'} cx="42" cy="42" r="40" />
                <path className={this.props.step === 'payment' ? 'payment-cls-2 active' : 'payment-cls-2'} d="M3094.05,2858h0a5.187,5.187,0,0,1-3.61-1.49l-12.99-12.96a5.116,5.116,0,0,1,7.24-7.23l9.36,9.35,20.23-20.19a5.112,5.112,0,0,1,7.23,7.23l-23.84,23.8A5.1,5.1,0,0,1,3094.05,2858Z" transform="translate(-3058.03 -2798)" />
              </svg>
              { this.props.step === 'payment' ? (<Text>Расчет и&nbsp;выбор страховой компании</Text>) : ''}
            </Icon>
          </Inner>
        </StepWrapper>
      );
    }

    return result;
  }
}

Step.propTypes = {
  step: PropTypes.string,
};

export default Step;
