import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import styled from 'styled-components';

import LogoImg from './Strelka_White.svg';

const HeaderInner = styled.div`
  display: flex;
  height: 50px;
  margin: auto;
  padding: 0 100px;
  width: 800px;

  @media (max-width: 840px) {
    margin: auto;
    width: calc((100px + 100%) / 1.5);
  }
`;

const HeaderWrapper = styled.div`
  background-color: #781E92;
  display: flex;
  height: 90px;
  width: 100%;
`;

const Logo = styled(Link)`
  background-image: url(${LogoImg});
  background-size: contain;
  display: block;
  height: 50px;
  margin-right: 74px;
  width: 176px;
`;

const Menu = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-left: auto;
  width: 350px;
`;

const MenuItemLink = styled(Link)`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  text-decoration: none;
  transition: opacity .3s ease-out;

  &:hover {
    opacity: .9;
  }

  &:only-child {
    margin-left: auto;
  }
`;

const MenuItemHashLink = styled(HashLink)`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  text-decoration: none;
  transition: opacity .3s ease-out;

  &:hover {
    opacity: .9;
  }
`;

const LandingHeader = (props) => {
  let menu = (
    <Menu>
      <MenuItemLink to="/answers">Вопрос-ответ</MenuItemLink>
    </Menu>
  );

  if (props.isMain) {
    menu = (
      <Menu>
        <MenuItemHashLink smooth to="/#about">О нас</MenuItemHashLink>
        <MenuItemHashLink smooth to="/#how">Как оформить полис?</MenuItemHashLink>
        <MenuItemLink to="/answers">Вопрос-ответ</MenuItemLink>
      </Menu>
    );
  }

  return (
    <HeaderWrapper>
      <HeaderInner>
        <Logo to="/" />
        {menu}
      </HeaderInner>
    </HeaderWrapper>
  );
};

LandingHeader.propTypes = {
  isMain: PropTypes.bool,
};

export default LandingHeader;
