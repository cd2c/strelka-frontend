import React from 'react';
import SwitchNormal from 'react-toggle-switch';
import styled from 'styled-components';
import 'react-toggle-switch/dist/css/switch.min.css';

const SwitchWrapper = styled.div`
  .equal-insurer {
    width: 45px;
    height: 30px;
    background: #bbbbbb;
    padding: 4px;
    border: none;
    border-radius: 30px;
    display: flex;
    transition: background .1s ease-in-out;

    &.on {
      background: #782490;
    }

    & .switch-toggle {
      box-shadow: none;
      border: none;
      width: 22px;
      height: 22px;
      transition: left .1s ease-in-out;
    }

    &.on .switch-toggle {
      left: 15px;
    }
  }
`;

const Switch = (props) =>
  (
    <SwitchWrapper>
      <SwitchNormal {...props} />
    </SwitchWrapper>
  );

export default Switch;
