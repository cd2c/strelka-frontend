import styled from 'styled-components';

const Wrapper = styled.footer`
  width: 100%;
  display: flex;
  justify-content: center;
  justify-self: end;
  padding: 0 20px;
  margin: auto;

  &.non-main {
    background-color: #772A8E;
  }
`;

export default Wrapper;
