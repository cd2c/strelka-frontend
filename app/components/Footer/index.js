import React from 'react';
import { Link as NormalLink } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: #781E92;
  height: 60px;
  width: 100%;

  @media (max-width: 840px) {
    height: auto;
  }
`;

const Inner = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: space-between;
  margin: auto;
  padding: 0 100px;
  width: 800px;

  @media (max-width: 840px) {
    flex-direction: column;
    justify-content: center;
    padding: 0;
    margin: auto;
    width: calc((100px + 100%) / 1.5);
  }
`;

const Text = styled.div`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  font-weight: 300;
  letter-spacing: -0.2px;
`;

const Link = styled(NormalLink)`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  font-weight: 300;
  letter-spacing: -0.2px;
  text-decoration: underline;
  transition: opacity .3s ease-in-out;

  &:hover {
    opacity: .9;
  }

  @media (max-width: 840px) {
    display: ${(props) => props.hideOnMobile ? 'none' : 'inherit'};
  }
`;

const Div = styled.div`
  @media (max-width: 840px) {
    margin: 5px 0;
    order: ${(props) => props.order ? props.order : '0'};
    width: 100%;

    &:nth-child(2) {
      margin-bottom: 10px;
    }

    &:last-child {
      margin-top: 10px;
    }
  }
`;

const LinkWrapper = styled.div`
  height: 18px;
  display: flex;
  align-items: center;
`;

const Footer = () => (
  <Wrapper>
    <Inner>
      <Div>
        <Text>
          2018&nbsp;&nbsp;&copy; ООО &laquo;КД2К&raquo;
        </Text>
        <Text>
          ОГРН&nbsp;1185476043334
        </Text>
      </Div>
      <Div>
        <Text>
          г. Новосибирск
        </Text>
        <Text>
          ул. Немировича-Данченко, 128, офис 311
        </Text>
      </Div>
      <Div order={-1}>
        <LinkWrapper>
          <Link to="/feedback">
            Обратная связь
          </Link>
        </LinkWrapper>
        <LinkWrapper>
          <Link to="/policy">
            Пользовательское соглашение
          </Link>
        </LinkWrapper>
      </Div>
    </Inner>
  </Wrapper>
);

export default Footer;
