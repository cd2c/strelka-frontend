import styled from 'styled-components';

const FormGroup = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  position: relative;

  margin: 10px 0;

  &:last-of-type {
    margin: 10px 0 16px;
  }

  &.indented {
    margin-top: 45px;
  }

  &.indented-bottom {
    margin-bottom: 33px;
  }
`;

export default FormGroup;
