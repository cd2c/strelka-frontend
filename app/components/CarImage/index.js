import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import isUndefined from 'lodash/isUndefined';

import audi from './images/50011.jpg';
import gaz from './images/50238.jpg';
import kamaz from './images/50247.jpg';
import uaz from './images/50279.jpg';
import bmw from './images/50028.jpg';
import cadillac from './images/50035.jpg';
import chery from './images/50044.jpg';
import chevrolet from './images/50045.jpg';
import chrysler from './images/50047.jpg';
import citroen from './images/50048.jpg';
import daewoo from './images/50054.jpg';
import datsun from './images/50362.jpg';
import dodge from './images/50064.jpg';
import fiat from './images/50072.jpg';
import ford from './images/50074.jpg';
import geely from './images/50307.jpg';
import honda from './images/50089.jpg';
import hummer from './images/50091.jpg';
import hyundai from './images/50093.jpg';
import infiniti from './images/50098.jpg';
import isuzu from './images/50104.jpg';
import jaguar from './images/50106.jpg';
import jeep from './images/50109.jpg';
import kia from './images/50115.jpg';
import lada from './images/50120.jpg';
import landrover from './images/50123.jpg';
import lexus from './images/50125.jpg';
import lifan from './images/50309.jpg';
import mazda from './images/50138.jpg';
import mercedes from './images/50142.jpg';
import mini from './images/50148.jpg';
import mitsubishi from './images/50149.jpg';
import nissan from './images/50156.jpg';
import opel from './images/50159.jpg';
import peugeot from './images/50168.jpg';
import pontiac from './images/50170.jpg';
import porsche from './images/50171.jpg';
import renault from './images/50177.jpg';
import saab from './images/50182.jpg';
import seat from './images/50189.jpg';
import skoda from './images/50193.jpg';
import smart from './images/50194.jpg';
import ssangyoung from './images/50197.jpg';
import subaru from './images/50201.jpg';
import suzuki from './images/50202.jpg';
import toyota from './images/50211.jpg';
import volkswagen from './images/50221.jpg';
import volvo from './images/50222.jpg';
import vortex from './images/50354.jpg';
import zaz from './images/50333.jpg';
import defaultAuto from './images/default-car.png';

import mercedesCclass from './images/Mercedes-Benz_C-Class_2002.jpg';
import jetta from './images/volkswagen_jetta_2015.jpg';
import tiida from './images/nissan_tiida_2011.jpg';
import logan from './images/Renault_Logan_2011.jpg';

const images = {
  50011: audi,
  50238: gaz,
  50247: kamaz,
  50279: uaz,
  50028: bmw,
  50035: cadillac,
  50044: chery,
  50045: chevrolet,
  50047: chrysler,
  50048: citroen,
  50054: daewoo,
  50362: datsun,
  50064: dodge,
  50072: fiat,
  50074: ford,
  50307: geely,
  50089: honda,
  50091: hummer,
  50093: hyundai,
  50098: infiniti,
  50104: isuzu,
  50106: jaguar,
  50109: jeep,
  50115: kia,
  50120: lada,
  50123: landrover,
  50125: lexus,
  50309: lifan,
  50138: mazda,
  50142: mercedes,
  50148: mini,
  50149: mitsubishi,
  50156: nissan,
  50159: opel,
  50168: peugeot,
  50170: pontiac,
  50171: porsche,
  50177: renault,
  50182: saab,
  50189: seat,
  50193: skoda,
  50194: smart,
  50197: ssangyoung,
  50201: subaru,
  50202: suzuki,
  50211: toyota,
  50221: volkswagen,
  50222: volvo,
  50354: vortex,
  50333: zaz,
};

const CartImageWrapper = styled.div`
  background-image: url(${(props) => props.image});
  background-size: contain;
  background-position: center;
  display: flex;
  align-items: center;
  width: 100%;
  height: 170px;
  margin-bottom: 30px;
`;

const CarImage = (props) => {
  let image = defaultAuto;

  if (!isUndefined(images[props.carId])) {
    image = images[props.carId];
  }

  if (props.carId === 50142 && props.modelId === 51775 && props.year >= 2000 && props.year <= 2006) {
    image = mercedesCclass;
  } else if (props.carId === 50221 && props.modelId === 52750 && props.year >= 2015 && props.year <= 2018) {
    image = jetta;
  } else if (props.carId === 50156 && props.modelId === 52117 && props.year >= 2004 && props.year <= 2014) {
    image = tiida;
  } else if (props.carId === 50177 && props.modelId === 52325 && props.year >= 2004 && props.year <= 2013) {
    image = logan;
  }

  return <CartImageWrapper image={image} />;
};

CarImage.propTypes = {
  carId: PropTypes.number,
  modelId: PropTypes.number,
  year: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
};

export default CarImage;
