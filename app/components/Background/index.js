import styled from 'styled-components';
import Bg from './images/bg.png';

const Background = styled.div`
  background-color: ${(props) => props.isNotFound ? '#07A1F4' : 'none'};
  background-image: ${(props) => props.isNotFound ? `url(${Bg})` : 'none'};
  background-size: ${(props) => props.isNotFound ? 'contain' : 'none'};
  background-position: ${(props) => props.isNotFound ? 'center' : 'none'};
`;

export default Background;
