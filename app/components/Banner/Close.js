import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { darken } from 'polished';

const Wrapper = styled.button`
  cursor: pointer;
  height: 30px;
  padding: 0;
  position: absolute;
  outline: none;
  right: 8px;
  top: 8px;
  width: 30px;

  & .colored-g {
    fill: #d8d8d8;
    transition: all .3s ease-out;
  }

  &:hover .colored-g {
    fill: ${darken(0.1, '#d8d8d8')}; 
  }
`;

const Close = (props) => (
  <Wrapper onClick={props.onClick}>
    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g className="colored-g" transform="translate(-969.000000, -675.000000)" fillRule="nonzero">
          <g transform="translate(969.000000, 675.000000)">
            <rect transform="translate(7.778175, 7.778175) rotate(45.000000) translate(-7.778175, -7.778175) " x="-2.22182541" y="6.77817459" width="20" height="2" />
            <rect transform="translate(7.778175, 7.778175) rotate(-45.000000) translate(-7.778175, -7.778175) " x="-2.22182541" y="6.77817459" width="20" height="2" />
          </g>
        </g>
      </g>
    </svg>
  </Wrapper>
);

Close.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Close;
