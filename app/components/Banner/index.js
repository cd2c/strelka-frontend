import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import noScroll from 'no-scroll';

import RemindPopup from 'containers/RemindPopup';
import { changeWasSend } from 'containers/RemindPopup/actions';

import BannerInner from './styled/BannerInner';
import BannerWrapper from './styled/BannerWrapper';
import Button from './styled/Button';
import ButtonSeparator from './styled/ButtonSeparator';
import ButtonWrapper from './styled/ButtonWrapper';
import Headline from './styled/Headline';
import Icon from './styled/Icon';
import Text from './styled/Text';

class Banner extends React.PureComponent {
  state = {
    remindPopup: false,
  };

  onCalcClick = () => {
    if (process.env.NODE_ENV !== 'development') {
      try {
        window.yaCounter47277876.reachGoal('calcButton');
      } catch (e) {
        console.log(e);
      }
    }

    this.props.history.push('/vehicle');
  };

  toggleRemindPopup = () => {
    const result = !this.state.remindPopup;

    if (result) {
      noScroll.on();
      document.addEventListener('mousedown', this.handleClickOutsidePopup);
    } else {
      noScroll.off();
      document.removeEventListener('mousedown', this.handleClickOutsidePopup);

      setTimeout(() => (
        this.props.changeWasSend({
          wasSend: false,
        })
      ), 300);
    }

    this.setState({
      remindPopup: result,
    });
  };

  handleClickOutsidePopup = (e) => {
    if (!ReactDOM.findDOMNode(this.popupInner).contains(e.target)) {  // eslint-disable-line react/no-find-dom-node
      this.toggleRemindPopup();
    }
  };

  render() {
    return (
      <BannerWrapper>
        <BannerInner>
          <Headline>Оформление<br />полиса ОСАГО</Headline>
          <Text>
            От ведущих страховых компаний, без очередей и навязчивых услуг
          </Text>
          <ButtonWrapper>
            <Button primary onClick={this.onCalcClick}>Рассчитать стоимость</Button>
            <ButtonSeparator>или</ButtonSeparator>
            <Button onClick={this.toggleRemindPopup}>Напомнить продлить ОСАГО</Button>
          </ButtonWrapper>
          <Icon>
            <span className="left-bar" />
            <span className="right-bar" />
          </Icon>
        </BannerInner>
        <RemindPopup
          ref={(node) => { this.popupInner = node; }}
          isActive={this.state.remindPopup}
          onClose={this.toggleRemindPopup}
        />
      </BannerWrapper>
    );
  }
}

Banner.propTypes = {
  changeWasSend: PropTypes.func,
  history: PropTypes.object,
};

const mapDispatchToProps = (dispatch) => ({
  changeWasSend: (value) => dispatch(changeWasSend(value)),
});

const mapStateToProps = createStructuredSelector({});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withRouter,
  withConnect,
)(Banner);
