import styled from 'styled-components';

const ButtonWrapper = styled.div`
  margin-top: 25px;
  width: 335px;

  @media (max-width: 840px) {
    width: 100%;
  }
`;

export default ButtonWrapper;
