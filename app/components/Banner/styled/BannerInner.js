import styled from 'styled-components';

const BannerInner = styled.div`
  width: 600px;

  @media (max-width: 840px) {
    margin: auto auto 70px;
    width: calc((100px + 100%) / 1.5);
  }
`;

export default BannerInner;
