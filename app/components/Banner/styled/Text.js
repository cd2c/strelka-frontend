import styled from 'styled-components';

const Text = styled.div`
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  color: #FFFFFF;
  letter-spacing: -0.5px;
  line-height: 20px;
  margin-top: 10px;
  width: 300px;

  @media (max-width: 840px) {
    width: 100%;
  }
`;

export default Text;
