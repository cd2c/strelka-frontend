import styled from 'styled-components';

import BgImage from './bg.jpg';

const BannerWrapper = styled.div`
  align-items: center;
  background-image: url(${BgImage});
  background-position: center;
  background-size: cover;
  display: flex;
  height: 400px;
  justify-content: center;
  position: relative;
  width: 100%;

  @media (max-width: 840px) {
    background-position: 75%;
    height: calc(100vh - 90px);
  }
`;

export default BannerWrapper;
