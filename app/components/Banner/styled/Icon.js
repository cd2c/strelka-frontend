import styled from 'styled-components';

const Icon = styled.div`
  animation: action 1s infinite  alternate;
  height: 18px;
  width: 34px;
  display: none;
  cursor: pointer;
  border-radius: 4px;
  position: absolute;
  bottom: 10px;
  left: calc(50% - 17px);

  @media (max-width: 840px) {
    display: block;
  }

  & .left-bar {
    position: absolute;
    background-color: transparent;
    top: calc(50% - 2px);
    left: 0;
    width: 20px;
    height: 2px;
    display: block;
    transform: rotate(40deg);
    float: right;
    border-radius: 2px;

    &:after {
      content:"";
      background-color: #ffffff;
      width: 20px;
      height: 2px;
      display: block;
      float: right;
      border-radius: 6px 10px 10px 6px;
      transition: all .5s cubic-bezier(.25,1.7,.35,.8);;
      z-index: -1;
    }
  }

  & .right-bar {
    position: absolute;
    background-color: transparent;
    top: calc(50% - 2px);
    left: 14px;
    width: 20px;
    height: 2px;
    display: block;
    transform: rotate(-40deg);
    float: right;
    border-radius: 2px;

    &:after {
      content:"";
      background-color: #ffffff;
      width: 20px;
      height: 2px;
      display: block;
      float: right;
      border-radius: 10px 6px 6px 10px;
      transition: all .5s cubic-bezier(.25,1.7,.35,.8);;
      z-index: -1;
    }
  }

  &.open {
    & .left-bar:after {
      transform-origin: center center;
      transform: rotate(-80deg);
    }
    & .right-bar:after {
      transform-origin: center center;
      transform: rotate(80deg);
    }
  }

  @keyframes action {
    0% {
      transform: translateY(0);
    }
    100% {
      transform: translateY(-10px);
    }
  }
`;

export default Icon;
