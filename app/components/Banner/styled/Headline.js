import styled from 'styled-components';

const Headline = styled.div`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 30px;
  font-weight: 300;
  letter-spacing: -0.75px;
  line-height: 34px;
  width: 335px;

  @media (max-width: 840px) {
    width: 100%;
  }
`;

export default Headline;
