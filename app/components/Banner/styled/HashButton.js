import styled from 'styled-components';
import { HashLink } from 'react-router-hash-link';
import { lighten } from 'polished';

const HashButton = styled(HashLink)`
  align-items: center;
  background-color: ${(props) => props.backgroundColor ? props.backgroundColor : '#782490'};
  border-radius: 12px;
  display: flex;
  cursor: pointer;
  position: relative;
  
  color: #fff;
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  height: 60px;
  justify-content: center;
  letter-spacing: -0.5px;
  text-decoration: none;
  transition: background-color .3s ease;
  width: 100%;

  &:hover {
    background-color: ${(props) => props.backgroundColor ? lighten(0.1, props.backgroundColor) : lighten(0.1, '#782490')};
  }
  
  &:active, &:focus {
    border: none;
    outline: none;
  }
`;

export default HashButton;
