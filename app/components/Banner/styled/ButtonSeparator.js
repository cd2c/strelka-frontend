import styled from 'styled-components';

const ButtonSeparator = styled.div`
  color: #FFFFFF;
  display: flex;
  font-size: 16px;
  justify-content: space-between;
  letter-spacing: -0.5px;
  line-height: 18px;
  margin: 13px 0;

  &:before, &:after {
    background-color: #ffffff;
    content: '';
    height: 1px;
    margin-top: 10px;
    width: 126px;
  }
`;

export default ButtonSeparator;
