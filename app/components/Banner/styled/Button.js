import styled from 'styled-components';
import { lighten } from 'polished';

const Button = styled.button`
  align-items: center;
  background-color: ${(props) => props.primary ? '#782490' : '#07a1f4'};
  border-radius: 12px;
  display: flex;
  cursor: pointer;
  position: relative;

  color: #fff;
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  height: 60px;
  justify-content: center;
  letter-spacing: -0.5px;
  padding: 0 30px;
  text-decoration: none;
  transition: background-color 0.3s ease;
  width: 100%;

  &:hover {
    background-color: ${(props) => props.primary ? lighten(0.1, '#782490') : lighten(0.1, '#07A1F4')};
  }

  &:active,
  &:focus {
    border: none;
    outline: none;
  }
`;

export default Button;
