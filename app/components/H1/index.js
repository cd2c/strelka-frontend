import styled from 'styled-components';

const H1 = styled.h1`
  font-family: 'Next Art';
  font-size: 28px;
  font-weight: bold;
  color: #fff;
  line-height: 33px;
`;

export default H1;
