import styled from 'styled-components';

const FormElement = styled.div`
  align-items: center;
  display: ${(props) => props.block ? 'block' : 'flex'};
  width: 100%;
`;

export default FormElement;
