import styled from 'styled-components';
import { lighten } from 'polished';

const Button = styled.button`
  &.landing {
    width: 100%;
    height: 60px;
    background-color: #782490;
    position: relative;
    border-radius: 12px;
    cursor: pointer;
  
    color: #fff;
    font-family: 'Ubuntu', sans-serif;
    font-size: 20px;
    font-weight: 300;
    letter-spacing: -0.5px;
    transition: background-color .3s ease;

    &:hover {
      background-color: ${lighten(0.1, '#782490')};
    }
  
    &:active, &:focus {
      border: none;
      outline: none;
    }
  }

  &.next {
    width: 400px;
    height: 60px;
    background-color: #782490;
    position: relative;
    border-radius: 12px;
    cursor: pointer;
  
    color: #fff;
    font-family: 'Ubuntu', sans-serif;
    font-size: 20px;
    font-weight: 300;
    letter-spacing: -0.5px;
    transition: background-color .3s ease;

    &:hover {
      background-color: ${lighten(0.1, '#782490')};
    }
  
    &:active, &:focus {
      border: none;
      outline: none;
    }

    &--onload {
      background-color: #f0f0f0;

      &:hover {
        background-color: #f0f0f0;
      }
    }

    &--disabled {
      background-color: #b7b7b7;

      &:hover {
        background-color: #b7b7b7;
      }
    }
  }

  &.prev {
    width: 190px;

    height: 60px;
    background-color: transparent;
    position: relative;
    border: 1px solid #ddd;
    border-radius: 12px;
    cursor: pointer;
    transition: border-color .3s ease;

    color: #666;
    font-family: 'Ubuntu', sans-serif;
    font-size: 20px;
    font-weight: 300;
    letter-spacing: -0.5px;

    &:hover {
      border-color: #666;
    }

    &:active, &:focus {
      border-color: #666;
      outline: none;
    }

    @media (max-width: 840px) {
      display: none;
    }
  }

  &.disabled {
    width: 30%;
    height: 40px;
    background-color: #ffffff;
    position: relative;
    border: 1px solid #791e92;
    border-radius: 6px;
  
    color: #791e92;
    font-family: 'Ubuntu', sans-serif;
    font-size: 14px;
    font-weight: bold;

    &:active, &:focus {
      border: 2px solid #791e92;
      outline: none;
    }
  }

  &.pay-btn {
    margin-left: auto;
  }
`;

export default Button;
