import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import isFunction from 'lodash/isFunction';
import Label from './Label';

const NormalInput = styled.textarea`
  background: ${(props) => props.disabled ? '#f0f0f0' : '#ffffff'};
  border: 1px solid #dddddd;
  border-radius: 2px;
  color: ${(props) => props.disabled ? '#666666' : '#404040'};
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  height: 120px;
  letter-spacing: -0.5px;
  line-height: 18px;
  outline: none;
  padding: 31px 12px 0;
  resize: none;
  transition: all .3s ease-out 0s;
  width: 100%;

  &:active, &:focus {
    border: 1px solid #07a1f4;

    &::placeholder {
      opacity: 1;
    }
  }

  &::placeholder {
    color: #f0f0f0;
    opacity: 0;
    transition: opacity .3s ease-out;
  }

  &.error {
    color: red;
    border: 1px solid red;
  }

  &.uppercase {
    text-transform: uppercase;
  }

  &.disabled {
    border-color: #791e92;
    font-size: 14px;
    color: #791e92;
    text-align: center;
  }
`;

export class Textarea extends React.PureComponent {
  state = {
    inputValue: this.props.value ? this.props.value : '',
    inputFocused: false,
  };

  onBlur = () => {
    this.setState({
      inputFocused: false,
    });

    if (isFunction(this.props.onBlur)) {
      this.props.onBlur();
    }
  };

  onFocus = () => {
    this.setState({
      inputFocused: true,
    });
  };

  setFocus = () => {
    this.input.querySelector('input').focus();
  };

  render() {
    const inputValue = this.props.value ? this.props.value : '';
    const inputClassName = classNames(
      'inputWrapper',
      {
        'inputWrapper-focused': this.state.inputFocused || this.props.value.length > 0,
      }
    );

    let wrapper;

    const small = {
      width: '40%',
      display: 'inline-block',
    };

    const middle = {
      width: 'calc(60% - 10px)',
      display: 'inline-block',
    };

    const simple = {
      width: '100%',
      display: 'inline-block',
    };

    if (
      this.props.className === 'uppercase small' ||
      this.props.className === 'error uppercase small' ||
      this.props.className === 'small' ||
      this.props.className === 'error small'
    ) {
      wrapper = (
        <label htmlFor={this.props.id} className={inputClassName} ref={(input) => { this.input = input; }} style={small}>
          <Label>{this.props.label}</Label>
          <NormalInput
            {...this.props}
            onBlur={this.onBlur}
            onFocus={this.onFocus}
          >
            {inputValue}
          </NormalInput>
        </label>
      );
    } else if (
      this.props.className === 'uppercase middle' ||
      this.props.className === 'error uppercase middle' ||
      this.props.className === 'middle' ||
      this.props.className === 'error middle'
    ) {
      wrapper = (
        <label htmlFor={this.props.id} className={inputClassName} ref={(input) => { this.input = input; }} style={middle}>
          <Label>{this.props.label}</Label>
          <NormalInput
            {...this.props}
            onBlur={this.onBlur}
            onFocus={this.onFocus}
          >
            {inputValue}
          </NormalInput>
        </label>
      );
    } else {
      wrapper = (
        <label htmlFor={this.props.id} className={inputClassName} ref={(input) => { this.input = input; }} style={simple}>
          <Label>{this.props.label}</Label>
          <NormalInput
            {...this.props}
            onBlur={this.onBlur}
            onFocus={this.onFocus}
          >
            {inputValue}
          </NormalInput>
        </label>
      );
    }

    return wrapper;
  }
}

Textarea.propTypes = {
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  id: PropTypes.string,
  onBlur: PropTypes.func,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
};

export default Textarea;
