import React from 'react';
import { Link as NormalLink } from 'react-router-dom';
import styled from 'styled-components';

const Footer = styled.div`
  background-color: #781E92;
  height: 40px;
  width: 100%;

  @media (max-width: 840px) {
    height: 45px;
  }
`;

const Inner = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: space-between;
  margin: auto;
  padding: 0 100px;
  width: 800px;

  @media (max-width: 840px) {
    flex-direction: column;
    justify-content: center;
    padding: 0;
    width: 100%;
  }
`;

const Text = styled.div`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  font-weight: 300;
  letter-spacing: -0.2px;
`;

const Link = styled(NormalLink)`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  font-weight: 300;
  letter-spacing: -0.2px;
  text-decoration: underline;
  transition: opacity .3s ease-in-out;

  &:hover {
    opacity: .9;
  }

  @media (max-width: 840px) {
    display: ${(props) => props.hideOnMobile ? 'none' : 'inherit'};
  }
`;

const LandingFooter = () => (
  <Footer>
    <Inner>
      <Text>
        2018&nbsp;&nbsp;&copy; ООО &laquo;КД2К&raquo;
      </Text>
      <Link to="/answers" hideOnMobile>
        Вопрос-ответ
      </Link>
      <Link to="/feedback" hideOnMobile>
        Обратная связь
      </Link>
      <Link to="/policy">
        Пользовательское соглашение
      </Link>
    </Inner>
  </Footer>
);

export default LandingFooter;
