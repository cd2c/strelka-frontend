import styled from 'styled-components';
import NormalLabel from 'components/Label';

const Label = styled(NormalLabel)`
  color: #666;
  font-family: 'Ubuntu', sans-serif;
  font-weight: 300;
  font-size: 16px;
  left: 12px;
  letter-spacing: -0.2px;
  line-height: 18px;
  position: absolute;
  text-align: left;
  top: 21px;
  transition: font-size .1s ease-in-out, top .1s ease-in-out;
  width: auto;
  z-index: 100;


  .inputWrapper-focused & {
    font-size: 12px;
    line-height: 13px;
    top: 9px;
  }

  @media (max-width: 840px) {
    display: block;
    font-size: 14px;
    width: calc(100% - 12px);
  }
`;

export default Label;
