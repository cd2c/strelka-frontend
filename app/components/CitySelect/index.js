import React from 'react';
import PropTypes from 'prop-types';
import { injectGlobal } from 'styled-components';
import Select from 'react-select';
import classNames from 'classnames';
import isUndefined from 'lodash/isUndefined';

import Label from './Label';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  .inputWrapper {
    .error {
      .react-select__control {
        border-color: red;
      }
    }
  }
`;

const control = (state) => ({
  backgroundColor: state.isDisabled ? '#f0f0f0' : '#ffffff',
  border: state.isFocused ? '1px solid #1EA3F1' : '1px solid #dddddd',
  borderRadius: '2px',
  color: state.isDisabled ? '#666666' : '#07a1f4',
  cursor: 'default',
  display: 'flex',
  borderSpacing: '0',
  borderCollapse: 'separate',
  boxShadow: 'none',
  height: '60px',
  outline: 'none',
  overflow: 'hidden',
  position: 'relative',
  width: '100%',
});

const colourStyles = {
  container: (styles) => ({
    ...styles,
    border: 'none',
    fontFamily: '\'Ubuntu\', sans-serif',
    outline: 'none',
  }),
  control: (styles, state) => ({ ...styles, ...control(state) }),
  menu: (styles) => ({
    ...styles,
    backgroundColor: '#ffffff',
    position: 'relative',
    zIndex: 1000,
  }),
  option: (styles) => ({
    ...styles,
    padding: 10,
  }),
  input: (styles, state) => ({ ...styles,
    alignItems: 'center',
    color: state.isDisabled ? '#666666' : '#404040',
    display: 'flex',
    fontFamily: '\'Ubuntu\', sans-serif',
    fontSize: 16,
    height: 50,
    letterSpacing: -0.5,
    lineHeight: 18,
    padding: '11px 0 0',
  }),
  placeholder: (styles) => ({ ...styles, display: 'none' }),
  indicatorSeparator: (styles) => ({ ...styles, display: 'none' }),
  dropdownIndicator: (styles) => ({ ...styles, display: 'none' }),
  clearIndicator: (styles) => ({ ...styles, display: 'none' }),
  singleValue: (styles, state) => ({
    ...styles,
    alignItems: 'center',
    color: state.isDisabled ? '#666666' : '#404040',
    display: 'flex',
    fontFamily: '\'Ubuntu\', sans-serif',
    fontSize: 16,
    height: 60,
    letterSpacing: -0.5,
    lineHeight: 18,
    padding: '11px 0 0',
  }),
};

class CitySelect extends React.Component {
  state = {
    error: this.props.isError,
    selectValue: '',
    inputFocused: false,
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      error: nextProps.isError,
    });
  }

  onBlur = () => {
    this.setState({
      inputFocused: false,
    });
  };

  updateValue = (newValue) => {
    if (this.props.onChange) {
      this.props.onChange(newValue);
    }

    this.setState({
      error: false,
      selectValue: newValue,
    });
  };

  clearError = () => {
    this.setState({
      error: false,
      inputFocused: true,
    });
  };

  changeInput = (e) => {
    this.props.onChangeInput(e);
  };

  render() {
    const options = this.props.options ? this.props.options : [];
    const isSearchable = isUndefined(this.props.isSearchable) ? true : this.props.isSearchable;
    const {
      name,
      label,
      selected,
      loading,
      disabled,
    } = this.props;

    const inputClassName = classNames(
      'inputWrapper',
      {
        'inputWrapper-focused': this.state.inputFocused || this.props.selected !== false,
      }
    );

    const selectClassName = classNames({
      error: this.state.error && this.props.selected === false,
    });

    return (
      <label htmlFor={name} className={inputClassName}>
        <Label>{label}</Label>
        <Select
          className={selectClassName}
          classNamePrefix="react-select"
          getOptionLabel={(option) => (option.title)}
          getOptionValue={(option) => (option.id)}
          value={selected}
          inputId={name}
          isLoading={loading}
          isDisabled={disabled}
          isClearable={false}
          isSearchable={isSearchable}
          loadingMessage={() => ('Загрузка')}
          noOptionsMessage={() => ('Не найдено')}
          onBlur={this.onBlur}
          onChange={this.updateValue}
          onInputChange={this.changeInput}
          onFocus={this.clearError}
          options={options}
          styles={colourStyles}
        />
      </label>
    );
  }
}

CitySelect.propTypes = {
  isError: PropTypes.bool,
  isSearchable: PropTypes.bool,
  options: PropTypes.any,
  selected: PropTypes.any,
  onChange: PropTypes.func,
  onChangeInput: PropTypes.func,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  name: PropTypes.string,
};

export default CitySelect;
