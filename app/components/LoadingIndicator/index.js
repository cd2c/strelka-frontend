import React from 'react';
import Spinner from 'react-spinkit';

import Wrapper from './Wrapper';

const LoadingIndicator = () => (
  <Wrapper>
    <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
  </Wrapper>
);

export default LoadingIndicator;
