import styled from 'styled-components';
import { Link as NormalLink } from 'react-router-dom';
import { lighten } from 'polished';

const Link = styled(NormalLink)`
  display: inline-block;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 18px;
  text-decoration: none;
  color: #07a1f4;
  margin: 10px 0 20px;

  &:hover {
    color: ${lighten(0.1, '#07a1f4')};
  }

  @media (max-width: 840px) {
    margin: 0;
  }
`;

export default Link;
