import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import NormalLabel from 'components/Label';

const Label = styled(NormalLabel)`
  color: #666666;
  font-family: 'Ubuntu', sans-serif;
  font-weight: 300;
  font-size: 12px;
  left: 12px;
  letter-spacing: -0.2px;
  line-height: 13px;
  position: absolute;
  text-align: left;
  top: 9px;
  transition: font-size .1s ease-in-out, top .1s ease-in-out;
  width: calc(100% - 40px);
  z-index: 100;
`;

const Wrapper = styled.div`
  display: inline-block;
  position: relative;
  width: 100%;
`;

const Input = styled.span`
  align-items: center;
  display: flex;
  background: #F0F0F0;
  border: 1px solid #DDDDDD;
  border-radius: 2px;
  color: #666666;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  height: 60px;
  letter-spacing: -.5px;
  line-height: 18px;
  outline: none;
  padding: 11px 12px 0;
  width: 100%;

  & .notify {
    font-size: 16px;
    color: #bbb;
    padding-left: 5px;
  }
`;

const InputValidity = (props) => (
  <Wrapper>
    <Label>{props.label}</Label>
    <Input>
      {props.children}
    </Input>
  </Wrapper>
);

InputValidity.propTypes = {
  label: PropTypes.string,
};

export default InputValidity;
