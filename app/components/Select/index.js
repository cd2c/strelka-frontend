import ReactSelect from 'react-select';
import styled from 'styled-components';

const Select = styled(ReactSelect)`
  & {
    position: relative;
  }

  &,
  & div,
  & input,
  & span {
    box-sizing: border-box;
  }

  &.is-open {
    & .Select-control {
      border-radius: 4px 4px 0 0;
    }
  }

  .Select-control {
    background: #fff;
    border: 1px solid #fff;
    border-bottom: 1px solid #07a1f4;
    border-radius: 4px;
    color: #07a1f4;
    cursor: default;
    display: table;
    border-spacing: 0;
    border-collapse: separate;
    height: 36px;
    outline: none;
    overflow: hidden;
    position: relative;
    width: 100%;
  }

  .Select-value {
    bottom: 0;
    color: #aaa;
    left: 0;
    line-height: 34px;
    padding: 0 10px 0 10px;
    position: absolute;
    right: 0;
    top: 0;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  .Select-value-label {
    color: #07a1f4;
    font-family: 'Ubuntu', sans-serif;
    font-size: 14px;
    font-weight: 700;
  }

  .Select-input {
    height: 34px;
    padding-left: 10px;
    padding-right: 10px;
    vertical-align: middle;
  }
  .Select-input > input {
    width: 100%;
    background: none transparent;
    border: 0 none;
    box-shadow: none;
    cursor: default;
    display: inline-block;
    color: #07a1f4;
    font-family: 'Ubuntu', sans-serif;
    font-size: 14px;
    font-weight: 700;
    margin: 0;
    outline: none;
    line-height: 14px;
    /* For IE 8 compatibility */
    padding: 8px 0 12px;
    /* For IE 8 compatibility */
    -webkit-appearance: none;
  }
  .is-focused .Select-input > input {
    cursor: text;
  }

  .Select-arrow-zone {
    cursor: pointer;
    display: table-cell;
    position: relative;
    text-align: center;
    vertical-align: middle;
    width: 25px;
    padding-right: 5px;
  }

  .Select-arrow {
    border-color: #999 transparent transparent;
    border-style: solid;
    border-width: 5px 5px 2.5px;
    display: inline-block;
    height: 0;
    width: 0;
    position: relative;
  }

  .Select-menu-outer {
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    background-color: #fff;
    border: 1px solid #fff;
    border-top-color: #07a1f4;
    box-shadow: 0 1px 0 rgba(0, 0, 0, 0.06);
    box-sizing: border-box;
    margin-top: -1px;
    max-height: 200px;
    position: absolute;
    top: 100%;
    width: 100%;
    z-index: 1;
    -webkit-overflow-scrolling: touch;
  }

  .Select-menu {
    max-height: 198px;
    overflow-y: auto;
  }

  .Select-option {
    box-sizing: border-box;
    background-color: #fff;
    color: #000000;
    font-family: 'Ubuntu', sans-serif;
    font-size: 14px;
    font-weight: 400;
    cursor: pointer;
    display: block;
    padding: 0 10px;
    height: 36px;
    line-height: 36px;

    &:last-child {
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
    }

    &.is-selected {
      background-color: #f5faff;
      /* Fallback color for IE 8 */
      background-color: rgba(0, 126, 255, 0.04);
      color: #333;
    }

    &.is-focused {
      background-color: #ebf5ff;
      /* Fallback color for IE 8 */
      background-color: rgba(0, 126, 255, 0.08);
      color: #333;
    }

    &.is-disabled {
      color: #cccccc;
      cursor: default;
    }
  }

  .Select-noresults {
    box-sizing: border-box;
    color: #999999;
    font-family: 'Ubuntu', sans-serif;
    font-size: 14px;
    font-weight: 400;
    cursor: default;
    display: block;
    padding: 8px 10px;
  }
`;

export default Select;
