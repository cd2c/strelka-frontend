import styled from 'styled-components';

const H2 = styled.h2`
  color: #fff;
  font-family: 'Ubuntu', sans-serif;
  font-size: 30px;
  font-weight: 300;
  letter-spacing: -0.75px;

  &.black {
    color: #000;
    line-height: normal;
  }

  &.success {
    margin-bottom: 60px;
  }
`;

export default H2;
