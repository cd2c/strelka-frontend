import styled from 'styled-components';

const SmallText = styled.p`
  margin: 0;
  padding: 0;
  font-size: 10px;
  font-family: 'Ubuntu', sans-serif;
  color: #000;
`;

export default SmallText;
