import styled from 'styled-components';

const Text = styled.p`
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 18px;
  margin: 0;
  padding: 0;

  &.success-text {
    margin: 10px 0;
  }
`;

export default Text;
