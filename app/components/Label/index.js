import styled from 'styled-components';

const Label = styled.span`
  width: 75px;
  line-height: 17px;
  text-align: right;
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  font-weight: 400;
  color: #404040;
`;

export default Label;
