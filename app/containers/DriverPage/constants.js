export const CHANGE_DRIVER_NAME = 'e-osago/Drivers/CHANGE_DRIVER_NAME';
export const CHANGE_DRIVER_SURNAME = 'e-osago/Drivers/CHANGE_DRIVER_SURNAME';
export const CHANGE_DRIVER_PATRONYMIC = 'e-osago/Drivers/CHANGE_DRIVER_PATRONYMIC';
export const CHANGE_DRIVER_BIRTHDAY = 'e-osago/Drivers/CHANGE_DRIVER_BIRTHDAY';
export const CHANGE_DRIVER_DOCDATE = 'e-osago/Drivers/CHANGE_DRIVER_DOCDATE';
export const CHANGE_DRIVER_SERIAL = 'e-osago/Drivers/CHANGE_DRIVER_SERIAL';
export const CHANGE_DRIVER_NUMBER = 'e-osago/Drivers/CHANGE_DRIVER_NUMBER';
export const CHANGE_DRIVER_MULTIDRIVE = 'e-osago/Drivers/CHANGE_DRIVER_MULTIDRIVE';
export const ADD_DRIVER = 'e-osago/Drivers/ADD_DRIVER';
export const DELETE_DRIVER = 'e-osago/Drivers/DELETE_DRIVER';

export const CHECK_ALL_DRIVERS = 'e-osago/Drivers/CHECK_ALL_DRIVERS';
export const CHECK_DRIVER = 'e-osago/Drivers/CHECK_DRIVER';
export const CHECK_DRIVER_SUCCESS = 'e-osago/Drivers/CHECK_DRIVER_SUCCESS';
export const CHECK_DRIVER_ERROR = 'e-osago/Drivers/CHECK_DRIVER_ERROR';
