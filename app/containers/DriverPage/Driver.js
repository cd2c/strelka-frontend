import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { darken } from 'polished';

import FormGroup from 'components/FormGroup';
import FormElement from 'components/FormElement';
import Input from 'components/Input';
import H3 from 'components/H3';

const DriverWrapper = styled.div`
  margin-top: 45px;

  &:first-child {
    margin-top: 0;
  }
`;

const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  .delete-svg {
    cursor: pointer;
    margin: 40px 0 10px;

    &:hover {
      .delete-bg {
        fill: ${darken(0.1, '#bbb')};
      }
    }
  }

  .delete-bg {
    fill: #bbb;
    transition: .3s ease-out;
  }

  .delete-text {
    font-size: 28px;
    fill: #fff;
    font-family: 'Ubuntu', sans-serif;
    font-weight: 700;
  }
`;

const Driver = (props) => {
  let driverNumberInput = {
    0: null,
    1: null,
    2: null,
    3: null,
    4: null,
  };

  const {
    id,
    titleId,
    name,
    surname,
    patronymic,
    birthday,
    docDate,
    serial,
    number,
  } = props;

  const changeSerial = (event) => {
    props.onChange(id, 'serial', event);

    if (event.target.value.length === 4) {
      driverNumberInput[id].setFocus();
    }
  };

  return (
    <DriverWrapper>
      <TitleWrapper>
        <H3 className="centered">Информация о водителе №{titleId + 1}</H3>
        { props.isClosable ?
          (
            <svg className="delete-svg" onClick={() => props.onDelete(id)} xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 40 40">
              <rect className="delete-bg" width="40" height="40" rx="20" ry="20" />
              <text className="delete-text" transform="translate(12.003 28.998)">×</text>
            </svg>
          ) : ''
        }
      </TitleWrapper>
      <FormGroup>
        <FormElement>
          <Input
            className={props.surnameInputError ? 'error' : ''}
            id={`surname[${id}]`}
            label="Фамилия"
            onChange={(event) => props.onChange(id, 'surname', event)}
            value={surname}
            disabled={props.isDisabled}
          />
        </FormElement>
      </FormGroup>
      <FormGroup>
        <FormElement>
          <Input
            className={props.nameInputError ? 'error' : ''}
            id={`name[${id}]`}
            label="Имя"
            onChange={(event) => props.onChange(id, 'name', event)}
            value={name}
            disabled={props.isDisabled}
          />
        </FormElement>
      </FormGroup>
      <FormGroup>
        <FormElement>
          <Input
            className={props.patronymicInputError ? 'error' : ''}
            id={`patronymic[${id}]`}
            label="Отчество"
            onChange={(event) => props.onChange(id, 'patronymic', event)}
            value={patronymic}
            disabled={props.isDisabled}
          />
        </FormElement>
      </FormGroup>
      <FormGroup>
        <FormElement>
          <Input
            className={props.birthdayInputError ? 'error' : ''}
            id={`birthday[${id}]`}
            label="Дата рождения"
            mask="99.99.9999"
            maskChar=""
            onChange={(event) => props.onChange(id, 'birthday', event)}
            placeholder="ДД.ММ.ГГГГ"
            value={birthday}
            disabled={props.isDisabled}
          />
        </FormElement>
      </FormGroup>
      <FormGroup className="indented">
        <FormElement>
          <Input
            className={props.docDateInputError ? 'error' : ''}
            id={`docDate[${id}]`}
            label="Дата начала водительского стажа"
            mask="99.99.9999"
            maskChar=""
            onChange={(event) => props.onChange(id, 'docDate', event)}
            placeholder="ДД.ММ.ГГГГ"
            value={docDate}
            disabled={props.isDisabled}
          />
        </FormElement>
      </FormGroup>
      <FormGroup>
        <FormElement className="flex flex-space-between">
          <Input
            className={props.serialInputError ? 'error small' : 'small'}
            id={`serial[${id}]`}
            label="Серия ВУ"
            onChange={changeSerial}
            placeholder="0000"
            value={serial}
            disabled={props.isDisabled}
          />
          <Input
            className={props.numberInputError ? 'error middle' : 'middle'}
            id={`number[${id}]`}
            label="Номер ВУ"
            mask="999999"
            maskChar=""
            onChange={(event) => props.onChange(id, 'number', event)}
            placeholder="000000"
            ref={(serialInput) => { driverNumberInput[id] = serialInput; }}
            value={number}
            disabled={props.isDisabled}
          />
        </FormElement>
      </FormGroup>
    </DriverWrapper>
  );
};

Driver.propTypes = {
  id: PropTypes.number.isRequired,
  titleId: PropTypes.number.isRequired,
  isClosable: PropTypes.bool,
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  surname: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  patronymic: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  birthday: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  docDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  serial: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  number: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  nameInputError: PropTypes.bool,
  surnameInputError: PropTypes.bool,
  patronymicInputError: PropTypes.bool,
  birthdayInputError: PropTypes.bool,
  docDateInputError: PropTypes.bool,
  serialInputError: PropTypes.bool,
  numberInputError: PropTypes.bool,
  onChange: PropTypes.func,
  onDelete: PropTypes.func,
  isDisabled: PropTypes.bool,
};

export default Driver;
