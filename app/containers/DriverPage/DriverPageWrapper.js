import styled from 'styled-components';
import { lighten } from 'polished';

const DriverPageWrapper = styled.section`
  background-color: #ffffff;
  box-shadow: 0 10px 50px 0 rgba(0,0,0,.2);
  padding: 80px 0 30px 0;
  position: relative;
  margin: 30px auto;
  min-width: 320px;
  max-width: 800px;
  width: 100%;

  .plus-bg {
    cursor: pointer;
    fill: #762E8C;
    transition: .3s ease;

    &:hover {
      fill: ${lighten(0.1, '#782490')};
    }
  }

  .plus-icon {
    font-size: 14px;
    fill: #fff;
    font-family: 'Ubuntu', sans-serif;
    font-weight: 700;
  }

  @media (max-width: 840px) {
    margin: 0 auto;
    min-height: calc(100vh - 238px);
  }
`;

export default DriverPageWrapper;
