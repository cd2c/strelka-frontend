import {
  CHANGE_DRIVER_NAME,
  CHANGE_DRIVER_SURNAME,
  CHANGE_DRIVER_PATRONYMIC,
  CHANGE_DRIVER_BIRTHDAY,
  CHANGE_DRIVER_DOCDATE,
  CHANGE_DRIVER_SERIAL,
  CHANGE_DRIVER_NUMBER,
  CHANGE_DRIVER_MULTIDRIVE,
  ADD_DRIVER,
  DELETE_DRIVER,
  CHECK_DRIVER,
  CHECK_DRIVER_SUCCESS,
  CHECK_DRIVER_ERROR,
  CHECK_ALL_DRIVERS,
} from './constants';

export function changeDriverName(driverNum, name) {
  return {
    type: CHANGE_DRIVER_NAME,
    driverNum,
    name,
  };
}

export function changeDriverSurname(driverNum, surname) {
  return {
    type: CHANGE_DRIVER_SURNAME,
    driverNum,
    surname,
  };
}

export function changeDriverPatronymic(driverNum, patronymic) {
  return {
    type: CHANGE_DRIVER_PATRONYMIC,
    driverNum,
    patronymic,
  };
}

export function changeDriverBirthday(driverNum, birthday) {
  return {
    type: CHANGE_DRIVER_BIRTHDAY,
    driverNum,
    birthday,
  };
}

export function changeDriverDocDate(driverNum, docDate) {
  return {
    type: CHANGE_DRIVER_DOCDATE,
    driverNum,
    docDate,
  };
}

export function changeDriverSerial(driverNum, serial) {
  return {
    type: CHANGE_DRIVER_SERIAL,
    driverNum,
    serial,
  };
}

export function changeDriverNumber(driverNum, number) {
  return {
    type: CHANGE_DRIVER_NUMBER,
    driverNum,
    number,
  };
}

export function changeDriverMultidrive(multidrive) {
  return {
    type: CHANGE_DRIVER_MULTIDRIVE,
    multidrive,
  };
}

export function addDriver() {
  return {
    type: ADD_DRIVER,
  };
}

export function deleteDriver(driverNum) {
  return {
    type: DELETE_DRIVER,
    driverNum,
  };
}

export function checkAllDrivers() {
  return {
    type: CHECK_ALL_DRIVERS,
  };
}

export function checkDriver(driverNum) {
  return {
    type: CHECK_DRIVER,
    driverNum,
  };
}

export function checkDriverSuccess(driverNum, apiId) {
  return {
    type: CHECK_DRIVER_SUCCESS,
    driverNum,
    apiId,
  };
}

export function checkDriverError(driverNum, error) {
  return {
    type: CHECK_DRIVER_ERROR,
    driverNum,
    error,
  };
}
