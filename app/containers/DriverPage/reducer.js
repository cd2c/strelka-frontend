import { fromJS } from 'immutable';

import {
  CHANGE_DRIVER_NAME,
  CHANGE_DRIVER_SURNAME,
  CHANGE_DRIVER_PATRONYMIC,
  CHANGE_DRIVER_BIRTHDAY,
  CHANGE_DRIVER_DOCDATE,
  CHANGE_DRIVER_SERIAL,
  CHANGE_DRIVER_NUMBER,
  CHANGE_DRIVER_MULTIDRIVE,
  ADD_DRIVER,
  DELETE_DRIVER,
  CHECK_ALL_DRIVERS,
  CHECK_DRIVER,
  CHECK_DRIVER_SUCCESS,
  CHECK_DRIVER_ERROR,
} from './constants';

const initialState = fromJS({
  list: [
    {
      id: 0,
      name: false,
      surname: false,
      patronymic: false,
      birthday: false,
      docDate: false,
      serial: false,
      number: false,
      apiId: false,
      isShowed: true,
    },
    {
      id: 1,
      name: false,
      surname: false,
      patronymic: false,
      birthday: false,
      docDate: false,
      serial: false,
      number: false,
      apiId: false,
      isShowed: false,
    },
    {
      id: 2,
      name: false,
      surname: false,
      patronymic: false,
      birthday: false,
      docDate: false,
      serial: false,
      number: false,
      apiId: false,
      isShowed: false,
    },
    {
      id: 3,
      name: false,
      surname: false,
      patronymic: false,
      birthday: false,
      docDate: false,
      serial: false,
      number: false,
      apiId: false,
      isShowed: false,
    },
    {
      id: 4,
      name: false,
      surname: false,
      patronymic: false,
      birthday: false,
      docDate: false,
      serial: false,
      number: false,
      apiId: false,
      isShowed: false,
    },
  ],
  driversUnshown: [1, 2, 3, 4],
  multidrive: false,
  loading: false,
  error: false,
});

function driversReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_DRIVER_NAME:
      return state
        .setIn(['list', action.driverNum, 'name'], action.name);
    case CHANGE_DRIVER_SURNAME:
      return state
        .setIn(['list', action.driverNum, 'surname'], action.surname);
    case CHANGE_DRIVER_PATRONYMIC:
      return state
        .setIn(['list', action.driverNum, 'patronymic'], action.patronymic);
    case CHANGE_DRIVER_BIRTHDAY:
      return state
        .setIn(['list', action.driverNum, 'birthday'], action.birthday);
    case CHANGE_DRIVER_DOCDATE:
      return state
        .setIn(['list', action.driverNum, 'docDate'], action.docDate);
    case CHANGE_DRIVER_SERIAL:
      return state
        .setIn(['list', action.driverNum, 'serial'], action.serial);
    case CHANGE_DRIVER_NUMBER:
      return state
        .setIn(['list', action.driverNum, 'number'], action.number);
    case CHANGE_DRIVER_MULTIDRIVE:
      return state
        .set('multidrive', action.multidrive);
    case ADD_DRIVER:
      return state
        .setIn(['list', state.get('driversUnshown').get(0), 'isShowed'], true)
        .set('driversUnshown', state.get('driversUnshown').delete(0));
    case DELETE_DRIVER:
      return state
        .set('driversUnshown', state.get('driversUnshown').push(action.driverNum))
        .setIn(['list', action.driverNum, 'name'], false)
        .setIn(['list', action.driverNum, 'surname'], false)
        .setIn(['list', action.driverNum, 'patronymic'], false)
        .setIn(['list', action.driverNum, 'birthday'], false)
        .setIn(['list', action.driverNum, 'docDate'], false)
        .setIn(['list', action.driverNum, 'serial'], false)
        .setIn(['list', action.driverNum, 'number'], false)
        .setIn(['list', action.driverNum, 'isShowed'], false);
    case CHECK_ALL_DRIVERS:
      return state;
    case CHECK_DRIVER:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['list', action.driverNum, 'apiId'], false);
    case CHECK_DRIVER_SUCCESS:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['list', action.driverNum, 'apiId'], action.apiId);
    case CHECK_DRIVER_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default driversReducer;
