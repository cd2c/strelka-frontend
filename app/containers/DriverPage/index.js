import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import update from 'immutability-helper';
import { Helmet } from 'react-helmet';
import Spinner from 'react-spinkit';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { validator } from 'utils/validation';

import Background from 'components/Background';
import CarImage from 'components/CarImage';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Step from 'components/Step';
import Form from 'components/Form';
import FormGroup from 'components/FormGroup';
import FormElement from 'components/FormElement';
import Switch from 'components/Switch';
import Button from 'components/Button';
import Text from 'components/Text';

import { makeSelectMark, makeSelectModel, makeSelectYear } from 'containers/HomePage/selectors';

import reducer from './reducer';
import {
  makeSelectDriversList,
  makeSelectDriversUnshown,
  makeSelectMultidrive,
} from './selectors';
import {
  addDriver,
  deleteDriver,
  changeDriverName,
  changeDriverSurname,
  changeDriverPatronymic,
  changeDriverBirthday,
  changeDriverDocDate,
  changeDriverSerial,
  changeDriverNumber,
  changeDriverMultidrive,
  checkAllDrivers,
} from './actions';
import saga from './saga';

import ButtonWrapper from './ButtonWrapper';
import Center from './Center';
import DriverPageWrapper from './DriverPageWrapper';
import DriverForm from './DriverForm';
import Driver from './Driver';
import Label from './Label';

export class DriverPage extends React.PureComponent {
  state = {
    formIsDisabled: false,
    drivers: [
      {
        nameInputError: false,
        surnameInputError: false,
        patronymicInputError: false,
        birthdayInputError: false,
        docDateInputError: false,
        serialInputError: false,
        numberInputError: false,
      },
      {
        nameInputError: false,
        surnameInputError: false,
        patronymicInputError: false,
        birthdayInputError: false,
        docDateInputError: false,
        serialInputError: false,
        numberInputError: false,
      },
      {
        nameInputError: false,
        surnameInputError: false,
        patronymicInputError: false,
        birthdayInputError: false,
        docDateInputError: false,
        serialInputError: false,
        numberInputError: false,
      },
      {
        nameInputError: false,
        surnameInputError: false,
        patronymicInputError: false,
        birthdayInputError: false,
        docDateInputError: false,
        serialInputError: false,
        numberInputError: false,
      },
      {
        nameInputError: false,
        surnameInputError: false,
        patronymicInputError: false,
        birthdayInputError: false,
        docDateInputError: false,
        serialInputError: false,
        numberInputError: false,
      },
    ],
  };

  setFormDisabled = () => {
    this.setState({
      formIsDisabled: !this.state.formIsDisabled,
    });
  };

  changeInput = (driverNum, type, event) => {
    switch (type) {
      case 'name':
        this.props.changeDriverName(driverNum, event.target.value);
        break;
      case 'surname':
        this.props.changeDriverSurname(driverNum, event.target.value);
        break;
      case 'patronymic':
        this.props.changeDriverPatronymic(driverNum, event.target.value);
        break;
      case 'birthday':
        this.props.changeDriverBirthday(driverNum, event.target.value);
        break;
      case 'docDate':
        this.props.changeDriverDocDate(driverNum, event.target.value);
        break;
      case 'serial':
        this.props.changeDriverSerial(driverNum, event.target.value.toUpperCase());
        break;
      case 'number':
        this.props.changeDriverNumber(driverNum, event.target.value.toUpperCase());
        break;
      default:
        break;
    }
  };

  changeMultidrive = () => {
    this.props.changeDriverMultidrive(!this.props.multidrive);
  };

  deleteDriver = (driverNum) => {
    const drivers = update(
      this.state.drivers,
      { [driverNum]: {
        $merge: {
          nameInputError: false,
          surnameInputError: false,
          patronymicInputError: false,
          birthdayInputError: false,
          docDateInputError: false,
          serialInputError: false,
          numberInputError: false,
        },
      } }
    );

    this.setState({
      drivers,
    });

    this.props.deleteDriver(driverNum);
  };

  validateForm = (event) => {
    event.preventDefault();
    let isValid = true;

    let drivers = update(
      this.state.drivers,
      { 0: {
        $merge: {
          nameInputError: false,
          surnameInputError: false,
          patronymicInputError: false,
          birthdayInputError: false,
          docDateInputError: false,
          serialInputError: false,
          numberInputError: false,
        },
      } }
    );

    this.props.drivers.filter((item) => item.get('isShowed') === true).map((item, key) => {
      const checkInputs = {
        nameInputError: !validator.name(item.get('name')),
        surnameInputError: !validator.surname(item.get('surname')),
        patronymicInputError: !validator.patronymic(item.get('patronymic')),
        birthdayInputError: !validator.driverBirthday(item.get('birthday')),
        docDateInputError: !validator.driverDocDate(item.get('docDate'), item.get('birthday')),
        serialInputError: !validator.driverSerial(item.get('serial')),
        numberInputError: !validator.driverNumber(item.get('number')),
      };

      drivers = update(
        drivers,
        { [key]: {
          $merge: {
            nameInputError: checkInputs.nameInputError,
            surnameInputError: checkInputs.surnameInputError,
            patronymicInputError: checkInputs.patronymicInputError,
            birthdayInputError: checkInputs.birthdayInputError,
            docDateInputError: checkInputs.docDateInputError,
            serialInputError: checkInputs.serialInputError,
            numberInputError: checkInputs.numberInputError,
          },
        } }
      );

      isValid = Object.values(checkInputs).indexOf(true) === -1;

      this.setState({
        drivers,
      });

      return drivers;
    });

    if (isValid || this.props.multidrive) {
      this.setFormDisabled();
      this.props.checkAllDrivers();
    }
  };

  render() {
    let drivers;
    let buttonText = 'Следующий шаг';
    if (this.state.formIsDisabled) {
      buttonText = (
        <Center>
          <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
        </Center>
      );
    }

    if (this.props.multidrive) {
      drivers = <Text>Действие полиса будет распространено на неограниченное количество водителей. Главное, чтобы они имели право управления транспортным средством.</Text>;
    } else {
      drivers = (
        <div>
          {this.props.drivers.filter((item) => item.get('isShowed') === true).map((item, key) => (
            <Driver
              id={item.get('id')}
              key={item.get('id')}
              titleId={key}
              isClosable={this.props.driversUnshown.size !== 4}
              name={item.get('name')}
              surname={item.get('surname')}
              patronymic={item.get('patronymic')}
              birthday={item.get('birthday')}
              docDate={item.get('docDate')}
              serial={item.get('serial')}
              number={item.get('number')}
              {...this.state.drivers[key]}
              onChange={this.changeInput}
              onDelete={this.deleteDriver}
              isDisabled={this.state.formIsDisabled}
            />))
          }
          <FormGroup>
            <FormElement className="flex">
              <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" onClick={this.props.addDriver}>
                <rect className="plus-bg" width="40" height="40" rx="20" ry="20" />
                <text className="plus-icon" x="16" y="24">+</text>
              </svg>
              <Label htmlFor="addDriver">
                Добавить водителя
              </Label>
            </FormElement>
          </FormGroup>
        </div>
      );
    }

    return (
      <Background className="not-main">
        <Helmet>
          <title>Информация о водителях</title>
        </Helmet>
        <Header isMain={false} />
        <DriverPageWrapper>
          <Step step="driver" />
          <DriverForm>
            <CarImage
              carId={this.props.mark.id}
              modelId={this.props.model.id}
              year={this.props.year}
            />
            <Form onSubmit={this.validateForm}>
              <FormGroup>
                <FormElement className="flex">
                  <Switch className="equal-insurer" onClick={this.changeMultidrive} on={this.props.multidrive} />
                  <Label className={this.props.multidrive ? 'active' : ''} htmlFor="isMultidrive" onClick={this.changeMultidrive}>
                    Неограниченное количество
                    водителей, допущенных к&nbsp;управлению
                  </Label>
                </FormElement>
              </FormGroup>
              {drivers}
              <ButtonWrapper>
                <Button type="button" disabled={this.state.formIsDisabled} className="prev" onClick={this.props.pushBackward}>
                  Назад
                </Button>
                <Button type="submit" disabled={this.state.formIsDisabled} className={this.state.formIsDisabled ? 'next next--onload half' : 'next half'} onClick={this.validateForm}>
                  {buttonText}
                </Button>
              </ButtonWrapper>
            </Form>
          </DriverForm>
        </DriverPageWrapper>
        <Footer isMain={false} />
      </Background>
    );
  }
}

DriverPage.propTypes = {
  mark: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  model: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  year: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  drivers: PropTypes.object,
  driversUnshown: PropTypes.object,
  multidrive: PropTypes.bool,
  addDriver: PropTypes.func,
  deleteDriver: PropTypes.func,
  changeDriverName: PropTypes.func,
  changeDriverSurname: PropTypes.func,
  changeDriverPatronymic: PropTypes.func,
  changeDriverBirthday: PropTypes.func,
  changeDriverDocDate: PropTypes.func,
  changeDriverSerial: PropTypes.func,
  changeDriverNumber: PropTypes.func,
  changeDriverMultidrive: PropTypes.func,
  pushBackward: PropTypes.func,
  pushForward: PropTypes.func,
  checkAllDrivers: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    addDriver: () => dispatch(addDriver()),
    deleteDriver: (driverNum) => dispatch(deleteDriver(driverNum)),
    changeDriverName: (driverNum, value) => dispatch(changeDriverName(driverNum, value)),
    changeDriverSurname: (driverNum, value) => dispatch(changeDriverSurname(driverNum, value)),
    changeDriverPatronymic: (driverNum, value) => dispatch(changeDriverPatronymic(driverNum, value)),
    changeDriverBirthday: (driverNum, value) => dispatch(changeDriverBirthday(driverNum, value)),
    changeDriverDocDate: (driverNum, value) => dispatch(changeDriverDocDate(driverNum, value)),
    changeDriverSerial: (driverNum, value) => dispatch(changeDriverSerial(driverNum, value)),
    changeDriverNumber: (driverNum, value) => dispatch(changeDriverNumber(driverNum, value)),
    changeDriverMultidrive: (multidrive) => dispatch(changeDriverMultidrive(multidrive)),
    pushBackward: () => dispatch(push('/owner')),
    pushForward: () => dispatch(push('/calc')),
    checkAllDrivers: () => dispatch(checkAllDrivers()),
  };
}

const mapStateToProps = createStructuredSelector({
  drivers: makeSelectDriversList(),
  driversUnshown: makeSelectDriversUnshown(),
  multidrive: makeSelectMultidrive(),
  mark: makeSelectMark(),
  model: makeSelectModel(),
  year: makeSelectYear(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'drivers', saga });
const withReducer = injectReducer({ key: 'drivers', reducer });

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(DriverPage);
