import styled from 'styled-components';

const DriverForm = styled.section`
  margin: 30px auto;
  width: calc((100px + 100%) / 1.5);
`;

export default DriverForm;
