import { call, put, select, takeEvery } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import * as Sentry from '@sentry/browser';
import request from 'utils/request';

import {
  CHECK_ALL_DRIVERS,
} from './constants';
import {
  checkDriver,
  checkDriverSuccess,
  checkDriverError,
} from './actions';

import {
  makeSelectDriversList,
  makeSelectMultidrive,
} from './selectors';

export function* checkDriversList() {
  const driversList = yield select(makeSelectDriversList());
  const multidrive = yield select(makeSelectMultidrive());

  if (!multidrive) {
    try {
      yield put(showLoading());
      yield* driversList.filter((item) => item.get('isShowed') === true).map(function* (item, key) {
        yield put(checkDriver(key));

        const driver = {
          surname: item.get('surname'),
          name: item.get('name'),
          patronymic: item.get('patronymic'),
          birthDateText: item.get('birthday'),
          serial: item.get('serial'),
          number: item.get('number'),
          driverDocDateText: item.get('docDate'),
        };

        const requestURL = 'driver';
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(driver),
        };

        try {
          const checkResult = yield call(request, requestURL, options);
          yield put(checkDriverSuccess(key, checkResult.id));
        } catch (err) {
          if (process.env.NODE_ENV === 'production') {
            Sentry.captureException(err);
          }
          yield put(checkDriverError(err));
        }
      });

      if (process.env.NODE_ENV !== 'development') {
        try {
          window.yaCounter47277876.reachGoal('drivers');
        } catch (e) {
          console.log(e);
        }
      }
    } finally {
      yield put(hideLoading());
    }
  }

  yield put(push('/calc'));
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* rootData() {
  yield takeEvery(CHECK_ALL_DRIVERS, checkDriversList);
}
