import { createSelector } from 'reselect';

const selectDrivers = (state) => state.get('drivers');

const makeSelectDriversUnshown = () => createSelector(
  selectDrivers,
  (driversState) => driversState.get('driversUnshown')
);

const makeSelectDriversList = () => createSelector(
  selectDrivers,
  (driversState) => driversState.get('list')
);

const makeSelectMultidrive = () => createSelector(
  selectDrivers,
  (driversState) => driversState.get('multidrive')
);

export {
  selectDrivers,
  makeSelectDriversList,
  makeSelectDriversUnshown,
  makeSelectMultidrive,
};
