import React from 'react';
import { Helmet } from 'react-helmet';

import Background from 'components/Background';
import Header from 'components/Header';
import Text from 'components/Text';
import H2 from 'components/H2';
import Footer from 'components/Footer';
import Link from 'components/Link';


import Dogs from './success.png';
import Image from './Image';
import ButtonLink from './Link';
import SuccessPageWrapper from './SuccessPageWrapper';
import SuccessText from './SuccessText';

const SuccessFeedbackPage = () => (
  <Background className="not-main">
    <Helmet>
      <title>Сообщение отправлено</title>
    </Helmet>
    <Header isMain={false} />
    <SuccessPageWrapper>
      <SuccessText>
        <H2 className="black">Мы уже получили ваше сообщение!</H2>
        <Text className="success-text">В ближайшее время мы свяжемся с вами по указанным контактам.</Text>
        <Text className="success-text">А пока вы ждете, можете <Link to="/vehicle">оформить еще один полис</Link></Text>
        <ButtonLink to="/">На главную</ButtonLink>
      </SuccessText>
      <Image src={Dogs} alt="" />
    </SuccessPageWrapper>
    <Footer isMain={false} />
  </Background>
);

export default SuccessFeedbackPage;
