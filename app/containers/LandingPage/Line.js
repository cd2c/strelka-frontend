import styled from 'styled-components';

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${(props) => props.marginBottom ? props.marginBottom : '0'};
  margin-top: ${(props) => props.marginTop ? props.marginTop : '0'};
  width: 370px;

  @media (max-width: 840px) {
    width: 100%;
  }
`;

export default Line;
