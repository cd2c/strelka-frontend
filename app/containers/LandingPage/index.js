import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { validator } from 'utils/validation';

import AsyncSelect from 'components/AsyncSelect';
import Banner from 'components/Banner';
import Button from 'components/Button';
import Form from 'components/Form';
import FormGroup from 'components/FormGroup';
import FormElement from 'components/FormElement';
import Header from 'components/Header';
import Input from 'components/Input';
import LandingBlock from 'components/LandingBlock';
import Footer from 'components/Footer';

import {
  makeSelectLoading,
  makeSelectError,
  makeSelectMarks,
  makeSelectMark,
  makeSelectModels,
  makeSelectModel,
  makeSelectYear,
} from 'containers/HomePage/selectors';
import {
  loadMarks,
  loadCategories,
  changeMark,
  changeModel,
  changeYear,
} from 'containers/HomePage/actions';
import reducer from 'containers/HomePage/reducer';
import saga from 'containers/HomePage/saga';

import Arrow from './Arrow';
import Answer from './Answer';
import Answers from './Answers';
import ButtonWrapper from './ButtonWrapper';
import Headline from './Headline';
import Icon from './Icon';
import Line from './Line';
import Logos from './Logos';
import LogoAS from './LogoAS';
import LogoIngos from './LogoIngos';
import P from './P';
import SmallHeadline from './SmallHeadline';
import Text from './Text';

import CarImage from './images/man-car.jpg';
import IconCard from './images/icon-card.svg';
import IconClock from './images/icon-clock.svg';
import IconCrown from './images/icon-crown.svg';
import IconMail from './images/icon-mail.svg';
import IconPencil from './images/icon-pencil.svg';
import IconPointer from './images/icon-pointer.svg';
import IconPointer2 from './images/icon-pointer-2.svg';
import IconShield from './images/icon-shield.svg';
import IconTg from './images/icon-tg.svg';
import ManImage from './images/man-phone.jpg';

class LandingPage extends React.PureComponent {
  state = {
    openedAnswer: null,
    markInputError: false,
    modelInputError: false,
    yearInputError: false,
    hash: null,
  };

  componentWillMount() {
    if (this.props.mark.items === false) {
      this.props.loadMarks();
    }
  }

  changeYear = (event) => {
    this.setState({
      yearInputError: event.target.value === '',
    });

    this.props.changeYear(event);
  };

  toggleAnswer = (id) => {
    if (id === this.state.openedAnswer) {
      this.setState({
        openedAnswer: null,
      });
    } else {
      this.setState({
        openedAnswer: id,
      });
    }
  };

  validateForm = (event) => {
    event.preventDefault();
    let isValid = true;

    if (this.props.mark.selected === false) {
      isValid = false;
      this.setState({
        markInputError: true,
      });
    }

    if (this.props.model.selected === false) {
      isValid = false;
      this.setState({
        modelInputError: true,
      });
    }

    if (!validator.required(this.props.year) || !validator.carYear(this.props.year)) {
      isValid = false;
      this.setState({
        yearInputError: true,
      });
    }

    if (isValid === true) {
      this.props.submitForm();
    }
  };

  render() {
    const { loading, error, mark, model, year } = this.props;

    const marksListProps = {
      isError: this.state.markInputError,
      label: 'Марка',
      loading: mark.loading,
      name: 'marks',
      options: mark.items,
      selected: mark.selected,
    };

    const modelsListProps = {
      isError: this.state.modelInputError,
      label: 'Модель',
      loading: model.loading,
      name: 'models',
      options: model.items,
      selected: model.selected,
    };

    return (
      <div>
        <Header isMain />
        <Banner />
        <LandingBlock id="about">
          <Headline>Страховой сервис «Стрелка»</Headline>
          <Text>Официальный сервис по оформлению полисов ОСАГО онлайн от ведущих страховых компаний</Text>
          <Logos>
            <LogoAS />
            <LogoIngos />
          </Logos>
        </LandingBlock>
        <LandingBlock
          id="how"
          image={ManImage}
        >
          <Headline>Как оформить полис?</Headline>
          <Line marginTop="20px">
            <Icon image={IconPencil} />
            <Text width="100%">1. Заполните данные полиса:  об автомобиле, страхователе, водителях</Text>
          </Line>
          <Line>
            <Arrow />
          </Line>
          <Line>
            <Icon image={IconTg} />
            <Text width="100%">2. Мы отправим запрос в страховые компании</Text>
          </Line>
          <Line>
            <Arrow />
          </Line>
          <Line>
            <Icon image={IconPointer} />
            <Text width="100%">3. Дождитесь ответа и выберите страховую компанию</Text>
          </Line>
          <Line>
            <Arrow />
          </Line>
          <Line>
            <Icon image={IconCard} />
            <Text width="100%">4. Оплатите полис</Text>
          </Line>
          <Line>
            <Arrow />
          </Line>
          <Line marginBottom="12px">
            <Icon image={IconMail} />
            <Text width="100%">5. Получите полис на e-mail</Text>
          </Line>
        </LandingBlock>
        <LandingBlock image={CarImage}>
          <Headline>Почему мы?</Headline>
          <Line marginTop="20px">
            <Icon image={IconCrown} />
            <Text width="100%" marginTop="0">
              <SmallHeadline>Самые лучшие</SmallHeadline>
              Только ведущие страховые компании для оформления полиса
            </Text>
          </Line>
          <Line marginTop="20px">
            <Icon image={IconClock} />
            <Text width="100%" marginTop="0">
              <SmallHeadline>Экономия времени</SmallHeadline>
              Оформление полиса занимает около 15 минут
            </Text>
          </Line>
          <Line marginTop="20px">
            <Icon image={IconShield} />
            <Text width="100%" marginTop="0">
              <SmallHeadline>Официальное партнерство</SmallHeadline>
              Документальное подтвержденние партнерства со страховыми компаниями
            </Text>
          </Line>
          <Line marginTop="20px" marginBottom="12px">
            <Icon image={IconPointer2} />
            <Text width="100%" marginTop="0">
              <SmallHeadline>Удобно</SmallHeadline>
              Продуманное пошаговое заполнение данных, доступное на всех устройствах
            </Text>
          </Line>
        </LandingBlock>
        <LandingBlock id="answers">
          <Headline>Вопрос-ответ</Headline>
          <Answers>
            <Answer
              id={1}
              isOpened={this.state.openedAnswer === 1}
              maxHeight="211px"
              onClick={this.toggleAnswer}
              title="Как получить диагностическую карту?"
            >
              <div>
                <P>
                  Найдите в вашем городе ближайший пункт технического осмотра и пройдите проверку состояния вашего автомобиля. При необходимости - устраните все недостатки.
                </P>
                <P>
                  После успешного прохождения техосмотра вам выдадут диагностическую карту на определенный срок. Номер и срок действия диагностической карты необходим для оформления полиса ОСАГО.
                </P>
              </div>
            </Answer>
            <Answer
              id={2}
              isOpened={this.state.openedAnswer === 2}
              maxHeight="191px"
              onClick={this.toggleAnswer}
              title="Что делать, если попал в ДТП?"
            >
              <div>
                <P>
                  Если вы попали в ДТП, сначала убедитесь, что нет пострадавших и жизни людей ничего не угрожает. Обратитесь в ту страховую компанию, в которой вы оформляли полис. Телефоны для обращения указаны в вашем полисе.
                </P>
                <P>
                  АльфаСтрахование - <b>8 800 333 0 999</b>
                  <br />
                  Ингосстрах - <b>8 495 956 55 55</b>
                </P>
              </div>
            </Answer>
            <Answer
              id={3}
              isOpened={this.state.openedAnswer === 3}
              maxHeight="343px"
              onClick={this.toggleAnswer}
              title="Как защитить себя от мошенников?"
            >
              <div>
                <P>К сожалению, с оформлением полисов ОСАГО онлайн связано много мошеннических сайтов.</P>
                <P>
                  Страховой сервис Стрелка имеет официальные договоренности со страховыми компаниями. Мы не принимаем оплату на свой собственный счет. Вы производите оплату на специализированном сайте страховой компании.
                </P>
                <P>
                  Обратите внимание на адрес сайта, на котором производите оплату полиса:
                  для АльфаСтрахование - <b>https://engine.paymentgate.ru/</b><br />
                  для Ингосстрах - <b>https://www.ingos.ru/</b>
                </P>
                <P>
                  Не производите оплату за полис, если адрес сайта отличается или кажется вам подозрительным!
                </P>
              </div>
            </Answer>
          </Answers>
        </LandingBlock>
        <LandingBlock id="calc">
          <Headline>Рассчитать полис ОСАГО</Headline>
          <Form onSubmit={this.validateForm}>
            <FormGroup>
              <FormElement>
                <AsyncSelect
                  onChange={this.props.changeMark}
                  {...marksListProps}
                />
              </FormElement>
            </FormGroup>
            <FormGroup>
              <FormElement>
                <AsyncSelect
                  onChange={this.props.changeModel}
                  {...modelsListProps}
                />
              </FormElement>
            </FormGroup>
            <FormGroup>
              <FormElement>
                <Input
                  className={this.state.yearInputError ? 'error' : ''}
                  id="year"
                  label="Год выпуска"
                  mask="9999"
                  maskChar=""
                  onChange={this.changeYear}
                  ref={(yearInput) => { this.yearInput = yearInput; }}
                  value={year}
                />
              </FormElement>
            </FormGroup>
            <ButtonWrapper>
              <Button type="submit" className="next">
                Следующий шаг
              </Button>
            </ButtonWrapper>
          </Form>
        </LandingBlock>
        <Footer />
      </div>
    );
  }
}

LandingPage.propTypes = {
  loading: PropTypes.bool,
  location: PropTypes.object,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  mark: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  model: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  year: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  loadMarks: PropTypes.func,
  changeMark: PropTypes.func,
  changeModel: PropTypes.func,
  changeYear: PropTypes.func,
  submitForm: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    changeMark: (event) => dispatch(changeMark(event)),
    changeModel: (event) => dispatch(changeModel(event)),
    changeYear: (event) => dispatch(changeYear(event.target.value)),
    loadMarks: () => dispatch(loadMarks()),
    loadCategories: () => dispatch(loadCategories()),
    submitForm: () => dispatch(push('/vehicle')),
  };
}

const mapStateToProps = createStructuredSelector({
  mark: createStructuredSelector({
    items: makeSelectMarks(),
    selected: makeSelectMark(),
  }),
  model: createStructuredSelector({
    items: makeSelectModels(),
    selected: makeSelectModel(),
  }),
  year: makeSelectYear(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'home', saga });
const withReducer = injectReducer({ key: 'car', reducer });

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(LandingPage);
