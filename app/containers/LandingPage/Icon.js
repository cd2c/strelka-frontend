import styled from 'styled-components';

const Icon = styled.div`
  background-image: url(${(props) => props.image});
  background-position: center;
  border: 1px solid #781E92;
  border-radius: 50%;
  flex-shrink: 0;
  height: 60px;
  margin-right: 10px;
  width: 60px;
`;

export default Icon;
