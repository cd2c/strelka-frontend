import styled from 'styled-components';
import Logo from './images/logo_as.jpg';

const LogoAS = styled.div`
  background-image: url(${Logo});
  background-size: contain;
  height: 90px;
  width: 130px;
`;

export default LogoAS;
