import styled from 'styled-components';

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 12px;
  margin-top: 20px;
`;

export default ButtonWrapper;
