import styled from 'styled-components';

const FormElement = styled.div`
  width: calc(100% - 85px);
`;

export default FormElement;
