import styled from 'styled-components';
import Logo from './images/logo_ingos.jpg';

const LogoIngos = styled.div`
  background-image: url(${Logo});
  background-size: contain;
  height: 90px;
  width: 130px;
`;

export default LogoIngos;
