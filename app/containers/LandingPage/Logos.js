import styled from 'styled-components';

const Logos = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px auto 0;
  width: 280px;
`;

export default Logos;
