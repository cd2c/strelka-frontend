import styled from 'styled-components';

const SmallHeadline = styled.p`
  border-bottom: 1px solid #DDDDDD;
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  font-weight: 600;
  letter-spacing: -0.5px;
  line-height: 22px;
  margin: 0 0 10px;
  padding-bottom: 10px;
`;

export default SmallHeadline;
