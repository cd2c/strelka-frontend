import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: ${(props) => props.isOpened ? 'rgba(7, 161, 244, .05)' : '#fff'};
  border: 1px solid #DDDDDD;
  border-bottom: none;
  cursor: pointer;
  height: auto;
  max-height: ${(props) => props.isOpened ? props.maxHeight : '58px'};
  overflow: hidden;
  transition: .5s all ease-in-out;

  @media (max-width: 840px) {
    max-height: ${(props) => props.isOpened ? `calc(${props.maxHeight} + 220px)` : '78px'};
  }

  &:last-child {
    border-bottom: 1px solid #DDDDDD;
  }

  
`;

const Headline = styled.div`
  align-items: center;
  color: #404040;
  display: flex;
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  height: 58px;
  justify-content: space-between;
  letter-spacing: -0.5px;
  padding: 0 20px;

  @media (max-width: 840px) {
    font-size: 16px;
    height: 78px;
    letter-spacing: -0.5px;
    line-height: 20px;
    padding: 10px 20px;
  }
`;

const Icon = styled.div`
  height: 100%;
  width: 18px;
  display:block;
  position: relative;
  cursor: pointer;
  border-radius: 4px;
  margin-left: 20px;

  & .left-bar {
    position: absolute;
    background-color: transparent;
    top: calc(50% - 2px);
    left: 0;
    width: 10px;
    height: 2px;
    display: block;
    transform: rotate(40deg);
    float: right;
    border-radius: 2px;

    &:after {
      content:"";
      background-color: #dddddd;
      width: 10px;
      height: 2px;
      display: block;
      float: right;
      border-radius: 6px 10px 10px 6px;
      transition: all .5s cubic-bezier(.25,1.7,.35,.8);;
      z-index: -1;
    }
  }

  & .right-bar {
    position: absolute;
    background-color: transparent;
    top: calc(50% - 2px);
    left: 7px;
    width: 10px;
    height: 2px;
    display: block;
    transform: rotate(-40deg);
    float: right;
    border-radius: 2px;

    &:after {
      content:"";
      background-color: #dddddd;
      width: 10px;
      height: 2px;
      display: block;
      float: right;
      border-radius: 10px 6px 6px 10px;
      transition: all .5s cubic-bezier(.25,1.7,.35,.8);;
      z-index: -1;
    }
  }

  &.open {
    & .left-bar:after {
      transform-origin: center center;
      transform: rotate(-80deg);
    }
    & .right-bar:after {
      transform-origin: center center;
      transform: rotate(80deg);
    }
  }
`;

class Answer extends React.PureComponent {
  openAnswer = () => {
    this.props.onClick(this.props.id);
  };

  render() {
    return (
      <Wrapper
        isOpened={this.props.isOpened}
        maxHeight={this.props.maxHeight}
        onClick={this.openAnswer}
      >
        <Headline>
          {this.props.title}
          <Icon className={this.props.isOpened ? 'open' : ''}>
            <span className="left-bar" />
            <span className="right-bar" />
          </Icon>
        </Headline>
        {this.props.children}
      </Wrapper>
    );
  }
}

Answer.propTypes = {
  children: PropTypes.element.isRequired,
  id: PropTypes.number,
  isOpened: PropTypes.bool,
  maxHeight: PropTypes.string,
  onClick: PropTypes.func,
  title: PropTypes.string,
};

export default Answer;
