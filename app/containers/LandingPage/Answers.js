import styled from 'styled-components';

const Answers = styled.div`
  margin-bottom: 12px;
  margin-top: 10px;
`;

export default Answers;
