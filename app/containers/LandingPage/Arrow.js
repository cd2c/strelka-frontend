import styled from 'styled-components';
import Bg from './images/icon-arrow.svg';

const Arrow = styled.div`
  background-image: url(${Bg});
  background-position: center;
  background-size: 6px 9px;
  height: 19px;
  width: 60px;
`;

export default Arrow;
