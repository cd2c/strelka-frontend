import styled from 'styled-components';

const Text = styled.div`
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  color: #404040;
  letter-spacing: -0.5px;
  line-height: 20px;
  margin-top: ${(props) => props.marginTop ? props.marginTop : '10px'};
  width: ${(props) => props.width ? props.width : 'auto'};
`;

export default Text;
