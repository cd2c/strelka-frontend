import styled from 'styled-components';

const FormGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  margin: 10px 0;

  &:last-of-type {
    margin: 10px 0 16px;
  }
`;

export default FormGroup;
