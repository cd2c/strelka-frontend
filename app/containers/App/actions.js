import {
  LOAD_MARKS,
  LOAD_MARKS_SUCCESS,
  LOAD_MARKS_ERROR,

  LOAD_MODELS,
  LOAD_MODELS_SUCCESS,
  LOAD_MODELS_ERROR,

  LOAD_CATEGORIES,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_CATEGORIES_ERROR,

  LOAD_TYPES,
  LOAD_TYPES_SUCCESS,
  LOAD_TYPES_ERROR,

  LOAD_PURPOSES,
  LOAD_PURPOSES_SUCCESS,
  LOAD_PURPOSES_ERROR,

  CHANGE_MARKA,
  CHANGE_MODEL,
  CHANGE_YEAR,
  CHANGE_CATEGORY,
  CHANGE_TYPE,
  CHANGE_LICENSE_PLATE,
  CHANGE_CAR_SERIAL,
  CHANGE_CAR_NUMBER,
  CHANGE_CAR_VIN,
  CHANGE_CAR_ENG_CAP,
  CHANGE_CAR_TICKET_NUMBER,
  CHANGE_CAR_TICKET_DATE,
  CHANGE_PURPOSE,
} from './constants';

export function loadMarks() {
  return {
    type: LOAD_MARKS,
  };
}

export function marksLoaded(marks) {
  return {
    type: LOAD_MARKS_SUCCESS,
    marks,
  };
}

export function marksLoadingError(error) {
  return {
    type: LOAD_MARKS_ERROR,
    error,
  };
}

export function loadModels() {
  return {
    type: LOAD_MODELS,
  };
}

export function modelsLoaded(models) {
  return {
    type: LOAD_MODELS_SUCCESS,
    models,
  };
}

export function modelsLoadingError(error) {
  return {
    type: LOAD_MODELS_ERROR,
    error,
  };
}

export function loadCategories() {
  return {
    type: LOAD_CATEGORIES,
  };
}

export function categoriesLoaded(categories) {
  return {
    type: LOAD_CATEGORIES_SUCCESS,
    categories,
  };
}

export function categoriesLoadingError(error) {
  return {
    type: LOAD_CATEGORIES_ERROR,
    error,
  };
}

export function loadPurposes() {
  return {
    type: LOAD_PURPOSES,
  };
}

export function purposesLoaded(purposes) {
  return {
    type: LOAD_PURPOSES_SUCCESS,
    purposes,
  };
}

export function purposesLoadingError(error) {
  return {
    type: LOAD_PURPOSES_ERROR,
    error,
  };
}

export function loadTypes() {
  return {
    type: LOAD_TYPES,
  };
}

export function typesLoaded(types) {
  return {
    type: LOAD_TYPES_SUCCESS,
    types,
  };
}

export function typesLoadingError(error) {
  return {
    type: LOAD_TYPES_ERROR,
    error,
  };
}

export function changeMark(marka) {
  return {
    type: CHANGE_MARKA,
    marka,
  };
}

export function changeModel(model) {
  return {
    type: CHANGE_MODEL,
    model,
  };
}

export function changeYear(year) {
  return {
    type: CHANGE_YEAR,
    year,
  };
}

export function changeCategory(category) {
  return {
    type: CHANGE_CATEGORY,
    category,
  };
}

export function changeType(typeObj) {
  return {
    type: CHANGE_TYPE,
    typeObj,
  };
}

export function changePurpose(purpose) {
  return {
    type: CHANGE_PURPOSE,
    purpose,
  };
}

export function changeLicensePlate(license) {
  return {
    type: CHANGE_LICENSE_PLATE,
    license,
  };
}

export function changeCarSerial(serial) {
  return {
    type: CHANGE_CAR_SERIAL,
    serial,
  };
}

export function changeCarNumber(number) {
  return {
    type: CHANGE_CAR_NUMBER,
    number,
  };
}


export function changeCarVin(vin) {
  return {
    type: CHANGE_CAR_VIN,
    vin,
  };
}

export function changeCarEngCap(engCap) {
  return {
    type: CHANGE_CAR_ENG_CAP,
    engCap,
  };
}

export function changeCarTicketNumber(ticketNumber) {
  return {
    type: CHANGE_CAR_TICKET_NUMBER,
    ticketNumber,
  };
}

export function changeCarTicketDate(ticketDate) {
  return {
    type: CHANGE_CAR_TICKET_DATE,
    ticketDate,
  };
}

