import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { compose } from 'redux';
import { Switch, Route } from 'react-router-dom';

import injectReducer from 'utils/injectReducer';
import reducer from 'containers/FeedbackPage/reducer';

import AnswersPage from 'containers/AnswersPage/Loadable';
import LandingPage from 'containers/LandingPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import VehiclePage from 'containers/VehiclePage/Loadable';
import OwnerPage from 'containers/OwnerPage/Loadable';
import DriverPage from 'containers/DriverPage/Loadable';
import CalcPage from 'containers/CalcPage/Loadable';
import SuccessPage from 'containers/SuccessPage/Loadable';
import FailPage from 'containers/FailPage/Loadable';
import PolicyPage from 'containers/PolicyPage/Loadable';
import FeedbackPage from 'containers/FeedbackPage/Loadable';
import SuccessFeedbackPage from 'containers/SuccessFeedbackPage/Loadable';

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 100vh;
  margin: 0 auto;
`;

class App extends React.Component {
  render() {
    return (
      <AppWrapper>
        <Helmet
          titleTemplate="%s | Страховой сервис «Стрелка»"
          defaultTitle="Купить полис ОСАГО онлайн | Оформить на сайте"
        >
          <meta name="description" content="ОСАГО — купить ОСАГО онлайн" />
        </Helmet>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route path="/answers" component={AnswersPage} />
          <Route path="/vehicle" component={VehiclePage} />
          <Route path="/owner" component={OwnerPage} />
          <Route path="/drivers" component={DriverPage} />
          <Route path="/calc" component={CalcPage} />
          <Route path="/success" component={SuccessPage} />
          <Route path="/fail" component={FailPage} />
          <Route path="/policy" component={PolicyPage} />
          <Route path="/feedback" component={FeedbackPage} />
          <Route path="/feedbackSuccess" component={SuccessFeedbackPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </AppWrapper>
    );
  }
}

const withReducer = injectReducer({ key: 'feedback', reducer });

export default compose(
  withReducer,
)(App);
