/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOAD_MARKS,
  LOAD_MARKS_SUCCESS,
  LOAD_MARKS_ERROR,
  LOAD_MODELS,
  LOAD_MODELS_SUCCESS,
  LOAD_MODELS_ERROR,
  LOAD_CATEGORIES,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_CATEGORIES_ERROR,
  LOAD_TYPES,
  LOAD_TYPES_SUCCESS,
  LOAD_TYPES_ERROR,
  LOAD_PURPOSES,
  LOAD_PURPOSES_SUCCESS,
  LOAD_PURPOSES_ERROR,
  CHANGE_MARKA,
  CHANGE_MODEL,
  CHANGE_YEAR,
  CHANGE_CATEGORY,
  CHANGE_TYPE,
  CHANGE_PURPOSE,
  CHANGE_LICENSE_PLATE,
  CHANGE_CAR_DOC_TYPE,
  CHANGE_CAR_SERIAL,
  CHANGE_CAR_NUMBER,
  CHANGE_CAR_VIN,
  CHANGE_CAR_ENG_CAP,
  CHANGE_CAR_TICKET_NUMBER,
  CHANGE_CAR_TICKET_DATE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default appReducer;
