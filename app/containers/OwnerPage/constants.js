/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_REGIONS = 'e-osago/Owner/LOAD_OWNER_REGIONS';
export const LOAD_REGIONS_SUCCESS = 'e-osago/Owner/LOAD_REGIONS_SUCCESS';
export const LOAD_REGIONS_ERROR = 'e-osago/Owner/LOAD_REGIONS_ERROR';

export const LOAD_OWNER_CITIES = 'e-osago/Owner/LOAD_OWNER_CITIES';
export const LOAD_OWNER_CITIES_SUCCESS = 'e-osago/Owner/LOAD_OWNER_CITIES_SUCCESS';
export const LOAD_OWNER_CITIES_ERROR = 'e-osago/Owner/LOAD_OWNER_CITIES_ERROR';

export const LOAD_INSURER_CITIES = 'e-osago/Owner/LOAD_INSURER_CITIES';
export const LOAD_INSURER_CITIES_SUCCESS = 'e-osago/Owner/LOAD_INSURER_CITIES_SUCCESS';
export const LOAD_INSURER_CITIES_ERROR = 'e-osago/Owner/LOAD_INSURER_CITIES_ERROR';

export const CHANGE_OWNER_NAME = 'e-osago/Owner/CHANGE_OWNER_NAME';
export const CHANGE_OWNER_SURNAME = 'e-osago/Owner/CHANGE_OWNER_SURNAME';
export const CHANGE_OWNER_PATRONYMIC = 'e-osago/Owner/CHANGE_OWNER_PATRONYMIC';
export const CHANGE_OWNER_BIRTHDAY = 'e-osago/Owner/CHANGE_OWNER_BIRTHDAY';
export const CHANGE_OWNER_SERIAL = 'e-osago/Owner/CHANGE_OWNER_SERIAL';
export const CHANGE_OWNER_NUMBER = 'e-osago/Owner/CHANGE_OWNER_NUMBER';
export const CHANGE_OWNER_REGION = 'e-osago/Owner/CHANGE_OWNER_REGION';
export const CHANGE_OWNER_CITY = 'e-osago/Owner/CHANGE_OWNER_CITY';
export const CHANGE_OWNER_CITY_INPUT = 'e-osago/Owner/CHANGE_OWNER_CITY_INPUT';
export const CHECK_OWNER = 'e-osago/Owner/CHECK_OWNER';
export const CHECK_OWNER_SUCCESS = 'e-osago/Owner/CHECK_OWNER_SUCCESS';
export const CHECK_OWNER_ERROR = 'e-osago/Owner/CHECK_OWNER_ERROR';

export const CHANGE_INSURER_NAME = 'e-osago/Owner/CHANGE_INSURER_NAME';
export const CHANGE_INSURER_SURNAME = 'e-osago/Owner/CHANGE_INSURER_SURNAME';
export const CHANGE_INSURER_PATRONYMIC = 'e-osago/Owner/CHANGE_INSURER_PATRONYMIC';
export const CHANGE_INSURER_BIRTHDAY = 'e-osago/Owner/CHANGE_INSURER_BIRTHDAY';
export const CHANGE_INSURER_SERIAL = 'e-osago/Owner/CHANGE_INSURER_SERIAL';
export const CHANGE_INSURER_NUMBER = 'e-osago/Owner/CHANGE_INSURER_NUMBER';
export const CHANGE_INSURER_REGION = 'e-osago/Owner/CHANGE_INSURER_REGION';
export const CHANGE_INSURER_CITY = 'e-osago/Owner/CHANGE_INSURER_CITY';
export const CHANGE_INSURER_CITY_INPUT = 'e-osago/Owner/CHANGE_INSURER_CITY_INPUT';
export const CHECK_INSURER = 'e-osago/Owner/CHECK_INSURER';
export const CHECK_INSURER_SUCCESS = 'e-osago/Owner/CHECK_INSURER_SUCCESS';
export const CHECK_INSURER_ERROR = 'e-osago/Owner/CHECK_INSURER_ERROR';
