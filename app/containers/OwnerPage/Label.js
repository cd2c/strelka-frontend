import styled from 'styled-components';
import NormalLabel from 'components/Label';

const Label = styled(NormalLabel)`
  align-self: center;
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 18px;
  margin-left: 10px;
  text-align: left;
  width: auto;
`;

export default Label;
