import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');
const selectOwner = (state) => state.get('ownerInsurer');
const selectInsurer = (state) => state.get('ownerInsurer');

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

const makeSelectOwnerName = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'name'])
);

const makeSelectOwnerSurname = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'surname'])
);

const makeSelectOwnerPatronymic = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'patronymic'])
);

const makeSelectOwnerBirthday = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'birthday'])
);

const makeSelectOwnerSerial = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'serial'])
);

const makeSelectOwnerNumber = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'number'])
);

const makeSelectOwnerRegion = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'region'])
);

const makeSelectOwnerCity = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'city'])
);

const makeSelectOwnerApiId = () => createSelector(
  selectOwner,
  (ownerState) => ownerState.getIn(['owner', 'apiId'])
);

const makeSelectInsurerName = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'name'])
);

const makeSelectInsurerSurname = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'surname'])
);

const makeSelectInsurerPatronymic = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'patronymic'])
);

const makeSelectInsurerBirthday = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'birthday'])
);

const makeSelectInsurerSerial = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'serial'])
);

const makeSelectInsurerNumber = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'number'])
);

const makeSelectInsurerRegion = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'region'])
);

const makeSelectInsurerCity = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'city'])
);

const makeSelectInsurerApiId = () => createSelector(
  selectInsurer,
  (insurerState) => insurerState.getIn(['insurer', 'apiId'])
);


export {
  selectOwner,
  selectInsurer,
  makeSelectLoading,
  makeSelectError,
  makeSelectOwnerName,
  makeSelectOwnerSurname,
  makeSelectOwnerPatronymic,
  makeSelectOwnerBirthday,
  makeSelectOwnerSerial,
  makeSelectOwnerNumber,
  makeSelectOwnerRegion,
  makeSelectOwnerCity,
  makeSelectOwnerApiId,
  makeSelectInsurerName,
  makeSelectInsurerSurname,
  makeSelectInsurerPatronymic,
  makeSelectInsurerBirthday,
  makeSelectInsurerSerial,
  makeSelectInsurerNumber,
  makeSelectInsurerRegion,
  makeSelectInsurerCity,
  makeSelectInsurerApiId,
};
