import { call, put, select, takeEvery } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import * as Sentry from '@sentry/browser';
import request from 'utils/request';

import { changeName } from 'containers/FeedbackPage/actions';

import {
  LOAD_REGIONS,
  LOAD_OWNER_CITIES,
  LOAD_INSURER_CITIES,
  CHECK_OWNER,
  CHECK_INSURER,
  CHANGE_INSURER_NAME,
} from './constants';

import {
  ownerRegionsLoaded,
  ownerRegionsLoadingError,
  ownerCitiesLoaded,
  ownerCitiesLoadingError,
  insurerCitiesLoaded,
  insurerCitiesLoadingError,
  checkOwnerSuccess,
  checkOwnerError,
  checkInsurer as checkInsurerAction,
  checkInsurerSuccess,
  checkInsurerError,
} from './actions';

import {
  makeSelectOwnerName,
  makeSelectOwnerSurname,
  makeSelectOwnerPatronymic,
  makeSelectOwnerBirthday,
  makeSelectOwnerSerial,
  makeSelectOwnerNumber,
  makeSelectOwnerRegion,
  makeSelectOwnerCity,
  makeSelectInsurerName,
  makeSelectInsurerSurname,
  makeSelectInsurerPatronymic,
  makeSelectInsurerBirthday,
  makeSelectInsurerSerial,
  makeSelectInsurerNumber,
  makeSelectInsurerRegion,
  makeSelectInsurerCity,
} from './selectors';

export function* checkOwner() {
  const surname = yield select(makeSelectOwnerSurname());
  const name = yield select(makeSelectOwnerName());
  const patronymic = yield select(makeSelectOwnerPatronymic());
  const birthDateText = yield select(makeSelectOwnerBirthday());
  const serial = yield select(makeSelectOwnerSerial());
  const number = yield select(makeSelectOwnerNumber());
  const country = '643';
  const zip = yield select(makeSelectOwnerCity());
  const state = yield select(makeSelectOwnerRegion());
  const city = yield select(makeSelectOwnerCity());
  const isOwner = '1';

  const owner = {
    surname,
    name,
    patronymic,
    birthDateText,
    serial,
    number,
    country,
    zip: zip.get('selected').zip.toString(),
    state: state.get('selected').title,
    city: city.get('selected').title,
    isOwner,
  };

  const requestURL = 'owner';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(owner),
  };

  try {
    yield put(showLoading());
    const checkResult = yield call(request, requestURL, options);

    if (process.env.NODE_ENV !== 'development') {
      try {
        window.yaCounter47277876.reachGoal('owner');
      } catch (e) {
        console.log(e);
      }
    }

    yield put(checkOwnerSuccess(checkResult.id));
  } catch (err) {
    yield put(checkOwnerError(err));
    if (process.env.NODE_ENV === 'production') {
      Sentry.captureException(err);
    }
  } finally {
    yield put(checkInsurerAction());
  }
}

export function* checkInsurer() {
  const surname = yield select(makeSelectInsurerSurname());
  const name = yield select(makeSelectInsurerName());
  const patronymic = yield select(makeSelectInsurerPatronymic());
  const birthDateText = yield select(makeSelectInsurerBirthday());
  const serial = yield select(makeSelectInsurerSerial());
  const number = yield select(makeSelectInsurerNumber());
  const country = '643';
  const zip = yield select(makeSelectInsurerCity());
  const state = yield select(makeSelectInsurerRegion());
  const city = yield select(makeSelectInsurerCity());
  const isOwner = '0';

  const insurer = {
    surname,
    name,
    patronymic,
    birthDateText,
    serial,
    number,
    country,
    zip: zip.get('selected').zip.toString(),
    state: state.get('selected').title,
    city: city.get('selected').title,
    isOwner,
  };

  const requestURL = 'owner';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(insurer),
  };

  try {
    const checkResult = yield call(request, requestURL, options);
    yield put(checkInsurerSuccess(checkResult.id));
  } catch (err) {
    yield put(checkInsurerError(err));
    if (process.env.NODE_ENV === 'production') {
      Sentry.captureException(err);
    }
  } finally {
    yield put(hideLoading());
    yield put(push('/drivers'));
  }
}

export function* getRegions() {
  const state = yield select(makeSelectOwnerRegion());
  const requestURL = `region?query=${state.get('input')}`;

  try {
    // Call our request helper (see 'utils/request')
    const regions = yield call(request, requestURL);
    yield put(ownerRegionsLoaded(regions));
  } catch (err) {
    yield put(ownerRegionsLoadingError(err));
  }
}

export function* getOwnerCities() {
  const state = yield select(makeSelectOwnerRegion());
  const city = yield select(makeSelectOwnerCity());
  const requestURL = `city?query=${city.get('input')}&parent=${state.get('selected').kladrId}`;

  try {
    // Call our request helper (see 'utils/request')
    const cities = yield call(request, requestURL);
    yield put(ownerCitiesLoaded(cities));
  } catch (err) {
    yield put(ownerCitiesLoadingError(err));
  }
}

export function* getInsurerCities() {
  const state = yield select(makeSelectInsurerRegion());
  const city = yield select(makeSelectInsurerCity());
  const requestURL = `city?query=${city.get('input')}&parent=${state.get('selected').kladrId}`;

  try {
    // Call our request helper (see 'utils/request')
    const cities = yield call(request, requestURL);
    yield put(insurerCitiesLoaded(cities));
  } catch (err) {
    yield put(insurerCitiesLoadingError(err));
  }
}

export function* changeNameSaga() {
  const name = yield select(makeSelectInsurerName());
  yield put(changeName(name));
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* rootData() {
  yield takeEvery(LOAD_REGIONS, getRegions);
  yield takeEvery(LOAD_OWNER_CITIES, getOwnerCities);
  yield takeEvery(LOAD_INSURER_CITIES, getInsurerCities);
  yield takeEvery(CHECK_OWNER, checkOwner);
  yield takeEvery(CHECK_INSURER, checkInsurer);
  yield takeEvery(CHANGE_INSURER_NAME, changeNameSaga);
}
