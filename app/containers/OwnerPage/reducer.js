import { fromJS } from 'immutable';

import {
  LOAD_REGIONS,
  LOAD_REGIONS_SUCCESS,
  LOAD_REGIONS_ERROR,

  LOAD_OWNER_CITIES,
  LOAD_OWNER_CITIES_SUCCESS,
  LOAD_OWNER_CITIES_ERROR,

  LOAD_INSURER_CITIES,
  LOAD_INSURER_CITIES_SUCCESS,
  LOAD_INSURER_CITIES_ERROR,

  CHANGE_OWNER_NAME,
  CHANGE_OWNER_SURNAME,
  CHANGE_OWNER_PATRONYMIC,
  CHANGE_OWNER_BIRTHDAY,
  CHANGE_OWNER_SERIAL,
  CHANGE_OWNER_NUMBER,
  CHANGE_OWNER_REGION,
  CHANGE_OWNER_CITY,
  CHANGE_OWNER_CITY_INPUT,
  CHECK_OWNER,
  CHECK_OWNER_SUCCESS,
  CHECK_OWNER_ERROR,

  CHANGE_INSURER_NAME,
  CHANGE_INSURER_SURNAME,
  CHANGE_INSURER_PATRONYMIC,
  CHANGE_INSURER_BIRTHDAY,
  CHANGE_INSURER_SERIAL,
  CHANGE_INSURER_NUMBER,
  CHANGE_INSURER_REGION,
  CHANGE_INSURER_CITY,
  CHANGE_INSURER_CITY_INPUT,
  CHECK_INSURER,
  CHECK_INSURER_SUCCESS,
  CHECK_INSURER_ERROR,
} from './constants';

const initialState = fromJS({
  owner: {
    name: false,
    surname: false,
    patronymic: false,
    birthday: false,
    serial: false,
    number: false,
    region: {
      loading: false,
      items: false,
      selected: false,
      input: false,
    },
    city: {
      loading: false,
      items: false,
      selected: false,
      input: false,
    },
    apiId: false,
  },
  insurer: {
    name: false,
    surname: false,
    patronymic: false,
    birthday: false,
    serial: false,
    number: false,
    region: {
      items: false,
      selected: false,
      input: false,
    },
    city: {
      items: false,
      selected: false,
      input: false,
    },
    apiId: false,
  },
  loading: false,
  error: false,
});

function ownerReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_REGIONS:
      return state
        .setIn(['owner', 'region', 'loading'], true)
        .setIn(['owner', 'region', 'items'], false);
    case LOAD_REGIONS_SUCCESS:
      return state
        .setIn(['owner', 'region', 'loading'], false)
        .setIn(['owner', 'region', 'items'], action.regions)
        .setIn(['insurer', 'region', 'items'], action.regions)
        .set('loading', false);
    case LOAD_REGIONS_ERROR:
      return state
        .set('error', action.error)
        .setIn(['owner', 'region', 'loading'], false);
    case LOAD_OWNER_CITIES:
      return state
        .set('error', false)
        .setIn(['owner', 'city', 'loading'], true)
        .setIn(['owner', 'city', 'items'], false);
    case LOAD_OWNER_CITIES_SUCCESS:
      return state
        .setIn(['owner', 'city', 'loading'], false)
        .setIn(['owner', 'city', 'items'], action.cities);
    case LOAD_OWNER_CITIES_ERROR:
      return state
        .set('error', action.error)
        .setIn(['owner', 'city', 'loading'], false);

    case LOAD_INSURER_CITIES:
      return state
        .set('error', false)
        .setIn(['insurer', 'city', 'items'], false)
        .setIn(['insurer', 'city', 'loading'], true);
    case LOAD_INSURER_CITIES_SUCCESS:
      return state
        .setIn(['insurer', 'city', 'items'], action.cities)
        .setIn(['insurer', 'city', 'loading'], false);
    case LOAD_INSURER_CITIES_ERROR:
      return state
        .set('error', action.error)
        .setIn(['insurer', 'city', 'loading'], false);

    case CHANGE_OWNER_NAME:
      return state
        .setIn(['owner', 'name'], action.name);
    case CHANGE_OWNER_SURNAME:
      return state
        .setIn(['owner', 'surname'], action.surname);
    case CHANGE_OWNER_PATRONYMIC:
      return state
        .setIn(['owner', 'patronymic'], action.patronymic);
    case CHANGE_OWNER_BIRTHDAY:
      return state
        .setIn(['owner', 'birthday'], action.birthday);
    case CHANGE_OWNER_SERIAL:
      return state
        .setIn(['owner', 'serial'], action.serial);
    case CHANGE_OWNER_NUMBER:
      return state
        .setIn(['owner', 'number'], action.number);
    case CHANGE_OWNER_REGION:
      return state
        .setIn(['owner', 'region', 'selected'], action.region);
    case CHANGE_OWNER_CITY:
      return state
        .setIn(['owner', 'city', 'selected'], action.city);
    case CHANGE_OWNER_CITY_INPUT:
      return state
        .setIn(['owner', 'city', 'input'], action.cityInput);
    case CHANGE_INSURER_NAME:
      return state
        .setIn(['insurer', 'name'], action.name);
    case CHANGE_INSURER_SURNAME:
      return state
        .setIn(['insurer', 'surname'], action.surname);
    case CHANGE_INSURER_PATRONYMIC:
      return state
        .setIn(['insurer', 'patronymic'], action.patronymic);
    case CHANGE_INSURER_BIRTHDAY:
      return state
        .setIn(['insurer', 'birthday'], action.birthday);
    case CHANGE_INSURER_SERIAL:
      return state
        .setIn(['insurer', 'serial'], action.serial);
    case CHANGE_INSURER_NUMBER:
      return state
        .setIn(['insurer', 'number'], action.number);
    case CHANGE_INSURER_REGION:
      return state
        .setIn(['insurer', 'region', 'selected'], action.region);
    case CHANGE_INSURER_CITY:
      return state
        .setIn(['insurer', 'city', 'selected'], action.city);
    case CHANGE_INSURER_CITY_INPUT:
      return state
        .setIn(['insurer', 'city', 'input'], action.cityInput);
    case CHECK_OWNER:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['owner', 'apiId'], false);
    case CHECK_OWNER_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .setIn(['owner', 'apiId'], action.apiId);
    case CHECK_OWNER_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    case CHECK_INSURER:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['insurer', 'apiId'], false);
    case CHECK_INSURER_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .setIn(['insurer', 'apiId'], action.apiId);
    case CHECK_INSURER_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default ownerReducer;
