import React from 'react';
import styled from 'styled-components';

import Img from 'components/Img';
import CarImagePng from 'images/car.png';
import CarImage2xPng from 'images/car_2x.png';

const CartImageWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 170px;

  & img {
    width: 100%;
    max-height: 170px;
  }
`;

const CarImage = () =>
  (
    <CartImageWrapper>
      <Img src={CarImagePng} srcset={CarImage2xPng} alt="Автомобиль" />
    </CartImageWrapper>
  );

export default CarImage;
