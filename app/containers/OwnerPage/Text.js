import styled from 'styled-components';

const Text = styled.span`
  &.after-input {
    font-size: 14px;
    font-family: 'Ubuntu', sans-serif;
    color: #07a1f4;
    padding-left: 6px;
  }
`;

export default Text;
