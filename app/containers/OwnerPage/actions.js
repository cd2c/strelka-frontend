import {
  LOAD_REGIONS,
  LOAD_REGIONS_SUCCESS,
  LOAD_REGIONS_ERROR,

  LOAD_OWNER_CITIES,
  LOAD_OWNER_CITIES_SUCCESS,
  LOAD_OWNER_CITIES_ERROR,

  LOAD_INSURER_CITIES,
  LOAD_INSURER_CITIES_SUCCESS,
  LOAD_INSURER_CITIES_ERROR,

  CHANGE_OWNER_NAME,
  CHANGE_OWNER_SURNAME,
  CHANGE_OWNER_PATRONYMIC,
  CHANGE_OWNER_BIRTHDAY,
  CHANGE_OWNER_SERIAL,
  CHANGE_OWNER_NUMBER,
  CHANGE_OWNER_REGION,
  CHANGE_OWNER_CITY,
  CHANGE_OWNER_CITY_INPUT,
  CHECK_OWNER,
  CHECK_OWNER_SUCCESS,
  CHECK_OWNER_ERROR,

  CHANGE_INSURER_NAME,
  CHANGE_INSURER_SURNAME,
  CHANGE_INSURER_PATRONYMIC,
  CHANGE_INSURER_BIRTHDAY,
  CHANGE_INSURER_SERIAL,
  CHANGE_INSURER_NUMBER,
  CHANGE_INSURER_REGION,
  CHANGE_INSURER_CITY,
  CHANGE_INSURER_CITY_INPUT,
  CHECK_INSURER,
  CHECK_INSURER_SUCCESS,
  CHECK_INSURER_ERROR,
} from './constants';

export function loadOwnerRegions() {
  return {
    type: LOAD_REGIONS,
  };
}

export function ownerRegionsLoaded(regions) {
  return {
    type: LOAD_REGIONS_SUCCESS,
    regions,
  };
}

export function ownerRegionsLoadingError(error) {
  return {
    type: LOAD_REGIONS_ERROR,
    error,
  };
}

export function loadOwnerCities() {
  return {
    type: LOAD_OWNER_CITIES,
  };
}

export function ownerCitiesLoaded(cities) {
  return {
    type: LOAD_OWNER_CITIES_SUCCESS,
    cities,
  };
}

export function ownerCitiesLoadingError(error) {
  return {
    type: LOAD_OWNER_CITIES_ERROR,
    error,
  };
}

export function loadInsurerCities() {
  return {
    type: LOAD_INSURER_CITIES,
  };
}

export function insurerCitiesLoaded(cities) {
  return {
    type: LOAD_INSURER_CITIES_SUCCESS,
    cities,
  };
}

export function insurerCitiesLoadingError(error) {
  return {
    type: LOAD_INSURER_CITIES_ERROR,
    error,
  };
}

export function changeOwnerName(name) {
  return {
    type: CHANGE_OWNER_NAME,
    name,
  };
}

export function changeOwnerSurname(surname) {
  return {
    type: CHANGE_OWNER_SURNAME,
    surname,
  };
}

export function changeOwnerPatronymic(patronymic) {
  return {
    type: CHANGE_OWNER_PATRONYMIC,
    patronymic,
  };
}

export function changeOwnerBirthday(birthday) {
  return {
    type: CHANGE_OWNER_BIRTHDAY,
    birthday,
  };
}

export function changeOwnerSerial(serial) {
  return {
    type: CHANGE_OWNER_SERIAL,
    serial,
  };
}

export function changeOwnerNumber(number) {
  return {
    type: CHANGE_OWNER_NUMBER,
    number,
  };
}

export function changeOwnerRegion(region) {
  return {
    type: CHANGE_OWNER_REGION,
    region,
  };
}

export function changeOwnerCity(city) {
  return {
    type: CHANGE_OWNER_CITY,
    city,
  };
}

export function changeOwnerCityInput(cityInput) {
  return {
    type: CHANGE_OWNER_CITY_INPUT,
    cityInput,
  };
}

export function changeInsurerName(name) {
  return {
    type: CHANGE_INSURER_NAME,
    name,
  };
}

export function changeInsurerSurname(surname) {
  return {
    type: CHANGE_INSURER_SURNAME,
    surname,
  };
}

export function changeInsurerPatronymic(patronymic) {
  return {
    type: CHANGE_INSURER_PATRONYMIC,
    patronymic,
  };
}

export function changeInsurerBirthday(birthday) {
  return {
    type: CHANGE_INSURER_BIRTHDAY,
    birthday,
  };
}

export function changeInsurerSerial(serial) {
  return {
    type: CHANGE_INSURER_SERIAL,
    serial,
  };
}

export function changeInsurerNumber(number) {
  return {
    type: CHANGE_INSURER_NUMBER,
    number,
  };
}

export function changeInsurerRegion(region) {
  return {
    type: CHANGE_INSURER_REGION,
    region,
  };
}

export function changeInsurerCity(city) {
  return {
    type: CHANGE_INSURER_CITY,
    city,
  };
}

export function changeInsurerCityInput(cityInput) {
  return {
    type: CHANGE_INSURER_CITY_INPUT,
    cityInput,
  };
}

export function checkOwner() {
  return {
    type: CHECK_OWNER,
  };
}

export function checkOwnerSuccess(apiId) {
  return {
    type: CHECK_OWNER_SUCCESS,
    apiId,
  };
}

export function checkOwnerError(error) {
  return {
    type: CHECK_OWNER_ERROR,
    error,
  };
}

export function checkInsurer() {
  return {
    type: CHECK_INSURER,
  };
}

export function checkInsurerSuccess(apiId) {
  return {
    type: CHECK_INSURER_SUCCESS,
    apiId,
  };
}

export function checkInsurerError(error) {
  return {
    type: CHECK_INSURER_ERROR,
    error,
  };
}
