import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import { Helmet } from 'react-helmet';
import Spinner from 'react-spinkit';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { validator } from 'utils/validation';

import AsyncSelect from 'components/AsyncSelect';
import Background from 'components/Background';
import CarImage from 'components/CarImage';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Step from 'components/Step';
import Form from 'components/Form';
import FormGroup from 'components/FormGroup';
import FormElement from 'components/FormElement';
import Switch from 'components/Switch';
import CitySelect from 'components/CitySelect';

import Input from 'components/Input';
import H3 from 'components/H3';
import Button from 'components/Button';

import { makeSelectMark, makeSelectModel, makeSelectYear } from 'containers/HomePage/selectors';

import reducer from './reducer';
import {
  makeSelectLoading,
  makeSelectError,
  makeSelectOwnerName,
  makeSelectOwnerSurname,
  makeSelectOwnerPatronymic,
  makeSelectOwnerBirthday,
  makeSelectOwnerSerial,
  makeSelectOwnerNumber,
  makeSelectOwnerRegion,
  makeSelectOwnerCity,
  makeSelectInsurerName,
  makeSelectInsurerSurname,
  makeSelectInsurerPatronymic,
  makeSelectInsurerBirthday,
  makeSelectInsurerSerial,
  makeSelectInsurerNumber,
  makeSelectInsurerRegion,
  makeSelectInsurerCity,
} from './selectors';
import {
  loadOwnerRegions,
  loadOwnerCities,
  loadInsurerCities,
  checkOwner,
  changeOwnerName,
  changeOwnerSurname,
  changeOwnerPatronymic,
  changeOwnerBirthday,
  changeOwnerSerial,
  changeOwnerNumber,
  changeOwnerRegion,
  changeOwnerCity,
  changeOwnerCityInput,
  changeInsurerName,
  changeInsurerSurname,
  changeInsurerPatronymic,
  changeInsurerBirthday,
  changeInsurerSerial,
  changeInsurerNumber,
  changeInsurerRegion,
  changeInsurerCity,
  changeInsurerCityInput,
} from './actions';
import saga from './saga';

import OwnerPageWrapper from './OwnerPageWrapper';
import OwnerForm from './OwnerForm';
import ButtonWrapper from './ButtonWrapper';
import Label from './Label';
import Center from './Center';

export class OwnerPage extends React.PureComponent {
  state = {
    formIsDisabled: false,
    ownerNameInputError: false,
    ownerSurnameInputError: false,
    ownerPatronymicInputError: false,
    ownerBirthdayInputError: false,
    ownerSerialInputError: false,
    ownerNumberInputError: false,
    ownerRegionInputError: false,
    ownerCityInputError: false,
    insurerNameInputError: false,
    insurerSurnameInputError: false,
    insurerPatronymicInputError: false,
    insurerBirthdayInputError: false,
    insurerSerialInputError: false,
    insurerNumberInputError: false,
    insurerRegionInputError: false,
    insurerCityInputError: false,
    isInsurer: true,
  };

  componentWillMount() {
    this.props.loadOwnerRegions();
  }

  setFormDisabled = () => {
    this.setState({
      formIsDisabled: !this.state.formIsDisabled,
    });
  };

  changeOwnerName = (event) => {
    this.setState({
      ownerNameInputError: event.target.value === '',
    });

    if (this.state.isInsurer) {
      this.setState({
        insurerNameInputError: event.target.value === '',
      });

      this.props.changeInsurerName(event.target.value);
    }

    this.props.changeOwnerName(event.target.value);
  };

  changeOwnerSurname = (event) => {
    this.setState({
      ownerSurnameInputError: event.target.value === '',
    });

    if (this.state.isInsurer) {
      this.setState({
        insurerSurnameInputError: event.target.value === '',
      });

      this.props.changeInsurerSurname(event.target.value);
    }

    this.props.changeOwnerSurname(event.target.value);
  };

  changeOwnerPatronymic = (event) => {
    this.setState({
      ownerPatronymicInputError: false,
    });

    if (this.state.isInsurer) {
      this.setState({
        insurerPatronymicInputError: false,
      });

      this.props.changeInsurerPatronymic(event.target.value);
    }

    this.props.changeOwnerPatronymic(event.target.value);
  };

  changeOwnerBirthday = (event) => {
    this.setState({
      ownerBirthdayInputError: event.target.value === '',
    });

    if (this.state.isInsurer) {
      this.setState({
        insurerBirthdayInputError: event.target.value === '',
      });

      this.props.changeInsurerBirthday(event.target.value);
    }

    this.props.changeOwnerBirthday(event.target.value);
  };

  changeOwnerSerial = (event) => {
    this.setState({
      ownerSerialInputError: event.target.value === '',
    });

    if (this.state.isInsurer) {
      this.setState({
        insurerSerialInputError: event.target.value === '',
      });

      this.props.changeInsurerSerial(event.target.value);
    }

    this.props.changeOwnerSerial(event.target.value);
    if (event.target.value.length === 4) {
      this.ownerNumberInput.setFocus();
    }
  };

  changeOwnerNumber = (event) => {
    this.setState({
      ownerNumberInputError: event.target.value === '',
    });

    if (this.state.isInsurer) {
      this.setState({
        insurerNumberInputError: event.target.value === '',
      });

      this.props.changeInsurerNumber(event.target.value);
    }

    this.props.changeOwnerNumber(event.target.value);
  };

  changeOwnerRegion = (result) => {
    this.setState({
      ownerRegionInputError: false,
    });

    this.props.changeOwnerRegion(result);

    if (this.state.isInsurer) {
      this.setState({
        insurerRegionInputError: false,
      });
      this.props.changeInsurerRegion(result);
    }
  };

  changeOwnerCityInput = (result) => {
    this.props.changeOwnerCityInput(result);
    this.props.loadOwnerCities();
  };

  changeOwnerCity = (result) => {
    this.setState({
      ownerCityInputError: false,
    });
    this.props.changeOwnerCity(result);

    if (this.state.isInsurer) {
      this.setState({
        insurerCityInputError: false,
      });
      this.props.changeInsurerCity(result);
    }
  };

  changeInsurerName = (event) => {
    this.setState({
      insurerNameInputError: event.target.value === '',
    });

    this.props.changeInsurerName(event.target.value);
  };

  changeInsurerSurname = (event) => {
    this.setState({
      insurerSurnameInputError: event.target.value === '',
    });

    this.props.changeInsurerSurname(event.target.value);
  };

  changeInsurerPatronymic = (event) => {
    this.setState({
      insurerPatronymicInputError: false,
    });

    this.props.changeInsurerPatronymic(event.target.value);
  };

  changeInsurerBirthday = (event) => {
    this.setState({
      insurerBirthdayInputError: event.target.value === '',
    });

    this.props.changeInsurerBirthday(event.target.value);
  };

  changeInsurerSerial = (event) => {
    this.setState({
      insurerSerialInputError: event.target.value === '',
    });

    this.props.changeInsurerSerial(event.target.value);

    if (event.target.value.length === 4) {
      this.insurerNumberInput.setFocus();
    }
  };

  changeInsurerNumber = (event) => {
    this.setState({
      insurerNumberInputError: event.target.value === '',
    });

    this.props.changeInsurerNumber(event.target.value);
  };

  changeInsurerRegion = (result) => {
    this.setState({
      insurerRegionInputError: false,
    });

    this.props.changeInsurerRegion(result);
    this.props.loadInsurerCities();
  };

  changeInsurerCityInput = (result) => {
    this.props.changeInsurerCityInput(result);
    this.props.loadInsurerCities();
  };

  changeInsurerCity = (result) => {
    this.setState({
      insurerCityInputError: false,
    });

    this.props.changeInsurerCity(result);
  };

  clearInsurer = () => {
    this.props.changeInsurerName(false);
    this.props.changeInsurerSurname(false);
    this.props.changeInsurerPatronymic(false);
    this.props.changeInsurerBirthday(false);
    this.props.changeInsurerSerial(false);
    this.props.changeInsurerNumber(false);
    this.props.changeInsurerRegion(false);
    this.props.changeInsurerCity(false);
  };

  fillInsurer = () => {
    const {
      ownerRegion,
      ownerCity,
    } = this.props;

    this.props.changeInsurerName(this.props.ownerName);
    this.props.changeInsurerSurname(this.props.ownerSurname);
    this.props.changeInsurerPatronymic(this.props.ownerPatronymic);
    this.props.changeInsurerBirthday(this.props.ownerBirthday);
    this.props.changeInsurerSerial(this.props.ownerSerial);
    this.props.changeInsurerNumber(this.props.ownerNumber);
    this.props.changeInsurerRegion(ownerRegion.get('selected'));
    this.props.changeInsurerCity(ownerCity.get('selected'));
  };

  changeIsInsurer = () => {
    if (this.state.isInsurer) {
      this.clearInsurer();
    } else {
      this.fillInsurer();
    }

    this.setState({
      isInsurer: !this.state.isInsurer,
    });
  };

  validateForm = (event) => {
    event.preventDefault();
    let isValid = true;

    if (!validator.name(this.props.ownerName)) {
      isValid = false;
      this.setState({
        ownerNameInputError: true,
      });
    }

    if (!validator.surname(this.props.ownerSurname)) {
      isValid = false;
      this.setState({
        ownerSurnameInputError: true,
      });
    }

    if (!validator.patronymic(this.props.ownerPatronymic)) {
      isValid = false;
      this.setState({
        ownerPatronymicInputError: true,
      });
    }

    if (!validator.ownerBirthday(this.props.ownerBirthday)) {
      isValid = false;
      this.setState({
        ownerBirthdayInputError: true,
      });
    }

    if (!validator.ownerSerial(this.props.ownerSerial)) {
      isValid = false;
      this.setState({
        ownerSerialInputError: true,
      });
    }

    if (!validator.ownerNumber(this.props.ownerNumber)) {
      isValid = false;
      this.setState({
        ownerNumberInputError: true,
      });
    }

    if (this.props.ownerRegion === false) {
      isValid = false;
      this.setState({
        ownerRegionInputError: true,
      });
    }

    if (this.props.ownerCity === false) {
      isValid = false;
      this.setState({
        ownerCityInputError: true,
      });
    }

    if (!this.state.isInsurer) {
      if (!validator.name(this.props.insurerName)) {
        isValid = false;
        this.setState({
          insurerNameInputError: true,
        });
      }

      if (!validator.surname(this.props.insurerSurname)) {
        isValid = false;
        this.setState({
          insurerSurnameInputError: true,
        });
      }

      if (!validator.patronymic(this.props.insurerPatronymic)) {
        isValid = false;
        this.setState({
          insurerPatronymicInputError: true,
        });
      }

      if (!validator.ownerBirthday(this.props.insurerBirthday)) {
        isValid = false;
        this.setState({
          insurerBirthdayInputError: true,
        });
      }

      if (!validator.ownerSerial(this.props.insurerSerial)) {
        isValid = false;
        this.setState({
          insurerSerialInputError: true,
        });
      }

      if (!validator.ownerNumber(this.props.insurerNumber)) {
        isValid = false;
        this.setState({
          insurerNumberInputError: true,
        });
      }

      if (this.props.insurerRegion === false) {
        isValid = false;
        this.setState({
          insurerRegionInputError: true,
        });
      }

      if (this.props.insurerCity === false) {
        isValid = false;
        this.setState({
          insurerCityInputError: true,
        });
      }
    }

    if (isValid === true) {
      this.setFormDisabled();
      this.props.checkOwner();
    }
  };

  render() {
    const {
      loading, error,
      ownerName,
      ownerSurname,
      ownerPatronymic,
      ownerBirthday,
      ownerSerial,
      ownerNumber,
      ownerRegion,
      ownerCity,
      insurerName,
      insurerSurname,
      insurerPatronymic,
      insurerBirthday,
      insurerSerial,
      insurerNumber,
      insurerRegion,
      insurerCity,
    } = this.props;

    const ownerRegionListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.ownerRegionInputError,
      label: 'Регион прописки',
      loading,
      name: 'ownerRegion',
      options: ownerRegion.get('items'),
      selected: ownerRegion.get('selected'),
    };

    const ownerCityListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.ownerCityInputError,
      label: 'Город, населенный пункт',
      loading: ownerCity.get('loading'),
      name: 'ownerCity',
      options: ownerCity.get('items'),
      selected: ownerCity.get('selected'),
    };

    const insurerRegionListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.insurerRegionInputError,
      label: 'Регион прописки',
      loading,
      name: 'insurerRegion',
      options: insurerRegion.get('items'),
      selected: insurerRegion.get('selected'),
    };

    const insurerCityListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.insurerCityInputError,
      label: 'Город, населенный пункт',
      loading: insurerCity.get('loading'),
      name: 'insurerCity',
      options: insurerCity.get('items'),
      selected: insurerCity.get('selected'),
    };

    let buttonText = 'Следующий шаг';
    if (this.state.formIsDisabled) {
      buttonText = (
        <Center>
          <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
        </Center>
      );
    }

    return (
      <Background className="not-main">
        <Helmet>
          <title>Информация о собственнике и страхователе</title>
        </Helmet>
        <Header isMain={false} />
        <OwnerPageWrapper>
          <Step step="owner" />
          <OwnerForm>
            <CarImage
              carId={this.props.mark.id}
              modelId={this.props.model.id}
              year={this.props.year}
            />
            <Form onSubmit={this.validateForm}>
              <H3>Информация о собственнике авто</H3>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.ownerSurnameInputError ? 'error' : ''}
                    id="ownerSurname"
                    label="Фамилия"
                    onChange={this.changeOwnerSurname}
                    ref={(ownerSurnameInput) => { this.ownerSurnameInput = ownerSurnameInput; }}
                    value={ownerSurname}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.ownerNameInputError ? 'error' : ''}
                    id="ownerName"
                    label="Имя"
                    onChange={this.changeOwnerName}
                    ref={(ownerNameInput) => { this.ownerNameInput = ownerNameInput; }}
                    value={ownerName}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.ownerPatronymicInputError ? 'error' : ''}
                    id="ownerPatronymic"
                    label="Отчество"
                    onChange={this.changeOwnerPatronymic}
                    ref={(ownerPatronymicInput) => { this.ownerPatronymicInput = ownerPatronymicInput; }}
                    value={ownerPatronymic}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup className="indented">
                <FormElement>
                  <Input
                    className={this.state.ownerBirthdayInputError ? 'error' : ''}
                    id="ownerBirthday"
                    label="Дата рождения"
                    mask="99.99.9999"
                    maskChar=""
                    onChange={this.changeOwnerBirthday}
                    placeholder="ДД.ММ.ГГГГ"
                    ref={(ownerBirthdayInput) => { this.ownerBirthdayInput = ownerBirthdayInput; }}
                    value={ownerBirthday}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement className="flex flex-space-between">
                  <Input
                    className={this.state.ownerSerialInputError ? 'error small' : 'small'}
                    id="ownerSerial"
                    label="Серия паспорта"
                    mobileLabel="Серия"
                    mask="9999"
                    maskChar=""
                    onChange={this.changeOwnerSerial}
                    placeholder="0000"
                    ref={(ownerSerialInput) => { this.ownerSerialInput = ownerSerialInput; }}
                    value={ownerSerial}
                    disabled={this.state.formIsDisabled}
                  />
                  <Input
                    className={this.state.ownerNumberInputError ? 'error middle' : 'middle'}
                    id="ownerNumber"
                    label="Номер паспорта"
                    mask="999999"
                    maskChar=""
                    onChange={this.changeOwnerNumber}
                    placeholder="000000"
                    ref={(ownerNumberInput) => { this.ownerNumberInput = ownerNumberInput; }}
                    value={ownerNumber}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    onChange={this.changeOwnerRegion}
                    {...ownerRegionListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup className="indented-bottom">
                <FormElement>
                  <CitySelect
                    onChange={this.changeOwnerCity}
                    onChangeInput={this.changeOwnerCityInput}
                    {...ownerCityListProps}
                  />
                </FormElement>
              </FormGroup>

              <H3>Информация о страхователе</H3>
              <FormGroup>
                <FormElement className="flex">
                  <Switch id="equalInsurer" className="equal-insurer" onClick={this.changeIsInsurer} on={this.state.isInsurer} />
                  <Label htmlFor="equalInsurer" onClick={this.changeIsInsurer}>
                    Совпадает с собственником
                  </Label>
                </FormElement>
              </FormGroup>

              {(() => {
                let result;
                if (!this.state.isInsurer) {
                  result = (
                    <div>
                      <FormGroup>
                        <FormElement>
                          <Input
                            className={this.state.insurerSurnameInputError ? 'error' : ''}
                            id="insurerSurname"
                            label="Фамилия"
                            onChange={this.changeInsurerSurname}
                            ref={(insurerSurnameInput) => { this.insurerSurnameInput = insurerSurnameInput; }}
                            value={insurerSurname}
                            disabled={this.state.formIsDisabled}
                          />
                        </FormElement>
                      </FormGroup>
                      <FormGroup>
                        <FormElement>
                          <Input
                            className={this.state.insurerNameInputError ? 'error' : ''}
                            id="insurerName"
                            label="Имя"
                            onChange={this.changeInsurerName}
                            ref={(insurerNameInput) => { this.insurerNameInput = insurerNameInput; }}
                            value={insurerName}
                            disabled={this.state.formIsDisabled}
                          />
                        </FormElement>
                      </FormGroup>
                      <FormGroup>
                        <FormElement>
                          <Input
                            className={this.state.insurerPatronymicInputError ? 'error' : ''}
                            id="insurerPatronymic"
                            label="Отчество"
                            onChange={this.changeInsurerPatronymic}
                            ref={(insurerPatronymicInput) => { this.insurerPatronymicInput = insurerPatronymicInput; }}
                            value={insurerPatronymic}
                            disabled={this.state.formIsDisabled}
                          />
                        </FormElement>
                      </FormGroup>
                      <FormGroup className="indented">
                        <FormElement>
                          <Input
                            className={this.state.insurerBirthdayInputError ? 'error' : ''}
                            id="insurerBirthday"
                            label="Дата рождения"
                            mask="99.99.9999"
                            maskChar=""
                            onChange={this.changeInsurerBirthday}
                            ref={(insurerBirthdayInput) => { this.insurerBirthdayInput = insurerBirthdayInput; }}
                            value={insurerBirthday}
                            disabled={this.state.formIsDisabled}
                          />
                        </FormElement>
                      </FormGroup>
                      <FormGroup>
                        <FormElement className="flex flex-space-between">
                          <Input
                            className={this.state.insurerSerialInputError ? 'error small' : 'small'}
                            id="insurerSerial"
                            label="Серия паспорта"
                            mobileLabel="Серия"
                            mask="9999"
                            maskChar=""
                            onChange={this.changeInsurerSerial}
                            placeholder="0000"
                            ref={(insurerSerialInput) => { this.insurerSerialInput = insurerSerialInput; }}
                            value={insurerSerial}
                            disabled={this.state.formIsDisabled}
                          />
                          <Input
                            className={this.state.insurerNumberInputError ? 'error middle' : 'middle'}
                            id="insurerNumber"
                            label="Номер паспорта"
                            mask="999999"
                            maskChar=""
                            onChange={this.changeInsurerNumber}
                            placeholder="000000"
                            ref={(insurerNumberInput) => { this.insurerNumberInput = insurerNumberInput; }}
                            value={insurerNumber}
                            disabled={this.state.formIsDisabled}
                          />
                        </FormElement>
                      </FormGroup>
                      <FormGroup>
                        <FormElement>
                          <AsyncSelect
                            onChange={this.changeInsurerRegion}
                            {...insurerRegionListProps}
                          />
                        </FormElement>
                      </FormGroup>
                      <FormGroup className="indented-bottom">
                        <FormElement>
                          <CitySelect
                            onChange={this.changeInsurerCity}
                            onChangeInput={this.changeInsurerCityInput}
                            {...insurerCityListProps}
                          />
                        </FormElement>
                      </FormGroup>
                    </div>
                  );
                }
                return result;
              })()}

              <ButtonWrapper>
                <Button type="button" disabled={this.state.formIsDisabled} className="prev" onClick={this.props.pushBackward}>
                  Назад
                </Button>
                <Button type="submit" disabled={this.state.formIsDisabled} className={this.state.formIsDisabled ? 'next next--onload half' : 'next half'} onClick={this.validateForm}>
                  {buttonText}
                </Button>
              </ButtonWrapper>
            </Form>
          </OwnerForm>
        </OwnerPageWrapper>
        <Footer isMain={false} />
      </Background>
    );
  }
}

OwnerPage.propTypes = {
  loading: PropTypes.bool,
  mark: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  model: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  year: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  ownerName: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ownerSurname: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ownerPatronymic: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ownerBirthday: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ownerSerial: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ownerNumber: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ownerRegion: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    input: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
  }),
  ownerCity: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    input: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
    loading: PropTypes.bool,
  }),
  insurerName: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  insurerSurname: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  insurerPatronymic: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  insurerBirthday: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  insurerSerial: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  insurerNumber: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  insurerRegion: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    input: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
  }),
  insurerCity: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    input: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
  }),
  loadOwnerRegions: PropTypes.func,
  loadOwnerCities: PropTypes.func,
  loadInsurerCities: PropTypes.func,
  changeOwnerName: PropTypes.func,
  changeOwnerSurname: PropTypes.func,
  changeOwnerPatronymic: PropTypes.func,
  changeOwnerBirthday: PropTypes.func,
  changeOwnerSerial: PropTypes.func,
  changeOwnerNumber: PropTypes.func,
  changeOwnerRegion: PropTypes.func,
  changeOwnerCity: PropTypes.func,
  changeOwnerCityInput: PropTypes.func,
  changeInsurerName: PropTypes.func,
  changeInsurerSurname: PropTypes.func,
  changeInsurerPatronymic: PropTypes.func,
  changeInsurerBirthday: PropTypes.func,
  changeInsurerSerial: PropTypes.func,
  changeInsurerNumber: PropTypes.func,
  changeInsurerRegion: PropTypes.func,
  changeInsurerCity: PropTypes.func,
  changeInsurerCityInput: PropTypes.func,
  pushBackward: PropTypes.func,
  checkOwner: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    loadOwnerRegions: () => dispatch(loadOwnerRegions()),
    loadOwnerCities: () => dispatch(loadOwnerCities()),
    loadInsurerCities: () => dispatch(loadInsurerCities()),
    changeOwnerName: (value) => dispatch(changeOwnerName(value)),
    changeOwnerSurname: (value) => dispatch(changeOwnerSurname(value)),
    changeOwnerPatronymic: (value) => dispatch(changeOwnerPatronymic(value)),
    changeOwnerBirthday: (value) => dispatch(changeOwnerBirthday(value)),
    changeOwnerSerial: (value) => dispatch(changeOwnerSerial(value)),
    changeOwnerNumber: (value) => dispatch(changeOwnerNumber(value)),
    changeOwnerRegion: (value) => dispatch(changeOwnerRegion(value)),
    changeOwnerCity: (value) => dispatch(changeOwnerCity(value)),
    changeOwnerCityInput: (value) => dispatch(changeOwnerCityInput(value)),
    changeInsurerName: (value) => dispatch(changeInsurerName(value)),
    changeInsurerSurname: (value) => dispatch(changeInsurerSurname(value)),
    changeInsurerPatronymic: (value) => dispatch(changeInsurerPatronymic(value)),
    changeInsurerBirthday: (value) => dispatch(changeInsurerBirthday(value)),
    changeInsurerSerial: (value) => dispatch(changeInsurerSerial(value)),
    changeInsurerNumber: (value) => dispatch(changeInsurerNumber(value)),
    changeInsurerRegion: (value) => dispatch(changeInsurerRegion(value)),
    changeInsurerCity: (value) => dispatch(changeInsurerCity(value)),
    changeInsurerCityInput: (value) => dispatch(changeInsurerCityInput(value)),
    checkOwner: () => dispatch(checkOwner()),
    pushBackward: () => dispatch(push('/vehicle')),
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  mark: makeSelectMark(),
  model: makeSelectModel(),
  year: makeSelectYear(),
  ownerName: makeSelectOwnerName(),
  ownerSurname: makeSelectOwnerSurname(),
  ownerPatronymic: makeSelectOwnerPatronymic(),
  ownerBirthday: makeSelectOwnerBirthday(),
  ownerSerial: makeSelectOwnerSerial(),
  ownerNumber: makeSelectOwnerNumber(),
  ownerRegion: makeSelectOwnerRegion(),
  ownerCity: makeSelectOwnerCity(),
  insurerName: makeSelectInsurerName(),
  insurerSurname: makeSelectInsurerSurname(),
  insurerPatronymic: makeSelectInsurerPatronymic(),
  insurerBirthday: makeSelectInsurerBirthday(),
  insurerSerial: makeSelectInsurerSerial(),
  insurerNumber: makeSelectInsurerNumber(),
  insurerRegion: makeSelectInsurerRegion(),
  insurerCity: makeSelectInsurerCity(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'owner', saga });
const withReducer = injectReducer({ key: 'ownerInsurer', reducer });

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(OwnerPage);
