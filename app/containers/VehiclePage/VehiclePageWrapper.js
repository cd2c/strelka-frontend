import styled from 'styled-components';

const VehiclePageWrapper = styled.div`
  background-color: #ffffff;
  box-shadow: 0 10px 50px 0 rgba(0,0,0,.2);
  display: flex;
  padding: 80px 0 30px 0;
  position: relative;
  margin: 30px auto;
  min-height: calc(100vh - 150px);
  min-width: 320px;
  max-width: 800px;
  width: 100%;

  @media (max-width: 840px) {
    margin: 0 auto;
    min-height: calc(100vh - 238px);
  }
`;

export default VehiclePageWrapper;
