import styled from 'styled-components';

const Text = styled.span`
  &.after-input {
    color: #404040;
    font-family: 'Ubuntu', sans-serif;
    font-size: 14px;
    font-weight: 300;
    letter-spacing: -0.3px;
    padding-left: 6px;
  }
`;

export default Text;
