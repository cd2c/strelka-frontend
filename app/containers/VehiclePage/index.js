import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { validator } from 'utils/validation';
import { Helmet } from 'react-helmet';
import Spinner from 'react-spinkit';

import {
  makeSelectLoading,
  makeSelectError,
  makeSelectMarks,
  makeSelectMark,
  makeSelectModels,
  makeSelectModel,
  makeSelectYear,
  makeSelectCategories,
  makeSelectCategory,
  makeSelectTypes,
  makeSelectType,
  makeSelectPurposes,
  makeSelectPurpose,
  makeSelectLicense,
  makeSelectDocType,
  makeSelectSerial,
  makeSelectNumber,
  makeSelectDocDate,
  makeSelectIdType,
  makeSelectVin,
  makeSelectEngCap,
  makeSelectUseTrailer,
  makeSelectTicketNumber,
  makeSelectTicketDate,
  makeSelectTypeLoading,
  makeSelectMarkLoading,
  makeSelectModelLoading,
  makeSelectCategoryLoading,
  makeSelectPurposeLoading,
} from 'containers/HomePage/selectors';
import {
  loadMarks,
  loadCategories,
  loadPurposes,
  loadTypes,
  changeMark,
  changeModel,
  changePurpose,
  changeYear,
  changeLicensePlate,
  changeCarDocType,
  changeCarSerial,
  changeCarNumber,
  changeCarDocDate,
  changeCarIdType,
  changeCarVin,
  changeCarEngCap,
  changeCarUseTrailer,
  changeCarTicketNumber,
  changeCarTicketDate,
  checkVehicle,
} from 'containers/HomePage/actions';
import reducer from 'containers/HomePage/reducer';
import saga from 'containers/HomePage/saga';

import Background from 'components/Background';
import CarImage from 'components/CarImage';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Step from 'components/Step';
import Form from 'components/Form';
import FormGroup from 'components/FormGroup';
import FormElement from 'components/FormElement';
import AsyncSelect from 'components/AsyncSelect';
import Input from 'components/Input';
import H3 from 'components/H3';
import Button from 'components/Button';
import Switch from 'components/Switch';

import Center from './Center';
import VehiclePageWrapper from './VehiclePageWrapper';
import VehicleForm from './VehicleForm';
import ButtonWrapper from './ButtonWrapper';
import Text from './Text';
import Label from './Label';

export class VehiclePage extends React.PureComponent {
  state = {
    formIsDisabled: false,
    markInputError: false,
    modelInputError: false,
    yearInputError: false,
    categoryInputError: false,
    typeInputError: false,
    purposeInputError: false,
    licensePlateInputError: false,
    documentTypeInputError: false,
    idTypeInputError: false,
    carSerialInputError: false,
    carNumberInputError: false,
    docDateInputError: false,
    carVinInputError: false,
    carEngCapInputError: false,
    carTicketNumberInputError: false,
    carTicketDateInputError: false,
    useTrailer: false,
  };

  componentDidMount() {
    if (this.props.mark.items === false) {
      this.props.loadMarks();
    }

    if (this.props.category.items === false && this.props.model.selected === false) {
      this.props.loadCategories();
    }

    if (this.props.purpose.items === false) {
      this.props.loadPurposes();
    }

    if (this.props.type.items === false) {
      this.props.loadTypes();
    }
  }

  setFormDisabled = () => {
    this.setState({
      formIsDisabled: !this.state.formIsDisabled,
    });
  };

  changeYear = (event) => {
    this.setState({
      yearInputError: event.target.value === '',
    });

    this.props.changeYear(event);
  };

  changeLicensePlate = (event) => {
    this.setState({
      licensePlateInputError: event.target.value === '',
    });

    this.props.changeLicensePlate(event);
  };

  changeCarDocType = (event) => {
    this.setState({
      documentTypeInputError: false,
    });

    this.props.changeCarDocType(event.id.toString());
  };

  changeCarSerial = (event) => {
    this.setState({
      carSerialInputError: event.target.value === '',
    });

    this.props.changeCarSerial(event);

    if (event.target.value.length === 4) {
      this.docNumberInput.setFocus();
    }
  };

  changeCarNumber = (event) => {
    this.setState({
      carNumberInputError: event.target.value === '',
    });

    this.props.changeCarNumber(event);
  };

  changeCarDocDate = (event) => {
    this.setState({
      docDateInputError: event.target.value === '',
    });

    this.props.changeCarDocDate(event);
  };

  changeCarIdType = (event) => {
    this.setState({
      idTypeInputError: false,
    });

    this.props.changeCarIdType(event.id.toString());
  };

  changeCarVin = (event) => {
    this.setState({
      carVinInputError: event.target.value === '',
    });

    this.props.changeCarVin(event);
  };

  changeCarEngCap = (event) => {
    this.setState({
      carEngCapInputError: event.target.value === '',
    });

    this.props.changeCarEngCap(event);
  };

  changeCarUseTrailer = () => {
    this.props.changeCarUseTrailer(!this.props.useTrailer);
  };

  changeCarTicketNumber = (event) => {
    this.setState({
      carTicketNumberInputError: event.target.value === '',
    });

    this.props.changeCarTicketNumber(event);
  };

  changeCarTicketDate = (event) => {
    this.setState({
      carTicketDateInputError: event.target.value === '',
    });

    this.props.changeCarTicketDate(event);
  };

  validateForm = (event) => {
    event.preventDefault();
    let isValid = true;

    if (this.props.mark.selected === false) {
      isValid = false;
      this.setState({
        markInputError: true,
      });
    }

    if (this.props.model.selected === false) {
      isValid = false;
      this.setState({
        modelInputError: true,
      });
    }

    if (!validator.required(this.props.year) || !validator.carYear(this.props.year)) {
      isValid = false;
      this.setState({
        yearInputError: true,
      });
    }

    if (this.props.category.selected === false) {
      isValid = false;
      this.setState({
        categoryInputError: true,
      });
    }

    if (this.props.type.selected === false) {
      isValid = false;
      this.setState({
        typeInputError: true,
      });
    }

    if (this.props.purpose.selected === false) {
      isValid = false;
      this.setState({
        purposeInputError: true,
      });
    }

    if (!validator.licensePlate(this.props.licensePlate)) {
      isValid = false;
      this.setState({
        licensePlateInputError: true,
      });
    }

    if (!validator.serial(this.props.serial)) {
      isValid = false;
      this.setState({
        carSerialInputError: true,
      });
    }

    if (!validator.number(this.props.number)) {
      isValid = false;
      this.setState({
        carNumberInputError: true,
      });
    }

    if (!validator.carDocDate(this.props.docDate, this.props.year)) {
      isValid = false;
      this.setState({
        docDateInputError: true,
      });
    }

    if (!validator.vin(this.props.vin)) {
      isValid = false;
      this.setState({
        carVinInputError: true,
      });
    }

    if (!validator.engCap(this.props.engCap)) {
      isValid = false;
      this.setState({
        carEngCapInputError: true,
      });
    }

    if (!validator.ticketDate(this.props.ticketDate, this.props.year)) {
      isValid = false;
      this.setState({
        carTicketDateInputError: true,
      });
    }

    if (!validator.ticketNumber(this.props.ticketNumber, this.props.year)) {
      isValid = false;
      this.setState({
        carTicketNumberInputError: true,
      });
    }

    if (isValid === true) {
      this.setFormDisabled();
      this.props.checkVehicle();
    }
  };

  render() {
    const {
      mark,
      model,
      year,
      category,
      type, purpose,
      licensePlate,
      serial, number,
      docDate,
      vin,
      engCap,
      ticketNumber,
      ticketDate,
    } = this.props;

    const marksListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.markInputError,
      label: 'Марка',
      loading: mark.loading,
      name: 'marks',
      options: mark.items,
      selected: mark.selected,
    };

    const modelsListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.modelInputError,
      label: 'Модель',
      loading: model.loading,
      name: 'models',
      options: model.items,
      selected: model.selected,
    };

    const categoriesListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.categoryInputError,
      isSearchable: false,
      label: 'Категория авто',
      loading: category.loading,
      name: 'category',
      options: category.items,
      selected: category.selected,
    };

    const typesListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.typeInputError,
      isSearchable: false,
      label: 'Тип авто',
      loading: type.loading,
      name: 'type',
      options: type.items,
      selected: type.selected,
    };

    const purposesListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.purposeInputError,
      isSearchable: false,
      label: 'Цель использования',
      loading: purpose.loading,
      name: 'purpose',
      options: purpose.items,
      selected: purpose.selected,
    };

    const documentTypes = {
      30: {
        id: 30,
        title: 'Паспорт ТС',
      },
      31: {
        id: 31,
        title: 'Свидетельство ТС',
      },
    };

    const idTransportTypes = {
      1: {
        id: 1,
        title: 'VIN',
      },
      2: {
        id: 2,
        title: 'Номер кузова',
      },
      3: {
        id: 3,
        title: 'Номер шасси',
      },
    };

    const documentListProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.documentTypeInputError,
      isSearchable: false,
      label: 'Тип документа',
      loading: false,
      name: 'documentType',
      options: [
        {
          id: 30,
          title: 'Паспорт ТС',
        }, {
          id: 31,
          title: 'Свидетельство ТС',
        },
      ],
      selected: documentTypes[this.props.docType],
    };

    const idTransportProps = {
      disabled: this.state.formIsDisabled,
      isError: this.state.documentTypeInputError,
      isSearchable: false,
      label: 'Идентификатор ТС',
      loading: false,
      name: 'idType',
      options: [
        {
          id: 1,
          title: 'VIN',
        }, {
          id: 2,
          title: 'Номер кузова',
        }, {
          id: 3,
          title: 'Номер шасси',
        },
      ],
      selected: idTransportTypes[this.props.idType],
    };

    let buttonText = 'Следующий шаг';
    if (this.state.formIsDisabled) {
      buttonText = (
        <Center>
          <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
        </Center>
      );
    }

    let carSerialPlaceholder = 'Серия СТС';
    let carNumberPlaceholder = 'Номер СТС';
    let carDocDatePlaceholder = 'Дата выдачи СТС';
    let idNumberPlaceholder = 'VIN';
    if (this.props.docType === '30') {
      carSerialPlaceholder = 'Серия ПТС';
      carNumberPlaceholder = 'Номер ПТС';
      carDocDatePlaceholder = 'Дата выдачи ПТС';
    }

    if (this.props.idType === '2') {
      idNumberPlaceholder = 'Номер кузова';
    } else if (this.props.idType === '3') {
      idNumberPlaceholder = 'Номер шасси';
    }

    let ticketCart = null;
    if ((new Date()).getFullYear() - this.props.year > 2) {
      ticketCart = (
        <div>
          <H3>Диагностическая карта</H3>
          <FormGroup>
            <FormElement>
              <Input
                className={this.state.carTicketNumberInputError ? 'error uppercase' : 'uppercase'}
                id="ticketNumber"
                label="Номер"
                mask="999999999999999"
                maskChar=""
                onChange={this.changeCarTicketNumber}
                placeholder="000000000000000"
                value={ticketNumber}
                disabled={this.state.formIsDisabled}
              />
            </FormElement>
          </FormGroup>
          <FormGroup>
            <FormElement>
              <Input
                className={this.state.carTicketDateInputError ? 'error uppercase' : 'uppercase'}
                id="ticketDate"
                label="Срок действия"
                mask="99.99.9999"
                maskChar=""
                onChange={this.changeCarTicketDate}
                placeholder="ДД.ММ.ГГГГ"
                value={ticketDate}
                disabled={this.state.formIsDisabled}
              />
            </FormElement>
          </FormGroup>
        </div>
      );
    }

    return (
      <Background className="not-main">
        <Helmet>
          <title>Информация об автомобиле</title>
          <meta name="description" content="ОСАГО — купить ОСАГО онлайн" />
        </Helmet>
        <Header isMain={false} />
        <VehiclePageWrapper>
          <Step step="vehicle" />
          <VehicleForm>
            <CarImage
              carId={this.props.mark.selected.id}
              modelId={this.props.model.selected.id}
              year={this.props.year}
            />
            <Form onSubmit={this.validateForm}>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    onChange={this.props.changeMark}
                    {...marksListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    onChange={this.props.changeModel}
                    {...modelsListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.yearInputError ? 'error' : ''}
                    id="year"
                    label="Год выпуска"
                    mask="9999"
                    maskChar=""
                    onChange={this.changeYear}
                    value={year}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    {...categoriesListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    {...typesListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    onChange={this.props.changePurpose}
                    {...purposesListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup className="indented">
                <FormElement>
                  <Input
                    className={this.state.licensePlateInputError ? 'error uppercase' : 'uppercase'}
                    id="licensePlate"
                    label="Гос номер"
                    mask="r 999 rr 999"
                    maskChar=""
                    onChange={this.changeLicensePlate}
                    placeholder="X 000 XX 000"
                    value={licensePlate}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    onChange={this.changeCarDocType}
                    {...documentListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement className="flex flex-space-between">
                  <Input
                    className={this.state.carSerialInputError ? 'error uppercase small' : 'uppercase small'}
                    id="docSerial"
                    label={carSerialPlaceholder}
                    mask="****"
                    maskChar=""
                    onChange={this.changeCarSerial}
                    placeholder="XXXX"
                    value={serial}
                    disabled={this.state.formIsDisabled}
                  />
                  <Input
                    className={this.state.carNumberInputError ? 'error uppercase middle' : 'uppercase middle'}
                    disabled={this.state.formIsDisabled}
                    id="docNumber"
                    label={carNumberPlaceholder}
                    mask="999999"
                    maskChar=""
                    onChange={this.changeCarNumber}
                    placeholder="XXXXXX"
                    ref={(docNumberInput) => { this.docNumberInput = docNumberInput; }}
                    value={number}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.docDateInputError ? 'error uppercase' : 'uppercase'}
                    id="docDate"
                    label={carDocDatePlaceholder}
                    mask="99.99.9999"
                    maskChar=""
                    onChange={this.changeCarDocDate}
                    placeholder="ДД.ММ.ГГГГ"
                    value={docDate}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <AsyncSelect
                    onChange={this.changeCarIdType}
                    {...idTransportProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.carVinInputError ? 'error uppercase' : 'uppercase'}
                    id="vin"
                    label={idNumberPlaceholder}
                    maskChar=""
                    onChange={this.changeCarVin}
                    placeholder=""
                    value={vin}
                    disabled={this.state.formIsDisabled}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.carEngCapInputError ? 'error uppercase middle' : 'uppercase middle'}
                    id="engCap"
                    label="Мощность двигателя"
                    mask="999"
                    maskChar=""
                    onChange={this.changeCarEngCap}
                    placeholder="0"
                    value={engCap}
                    disabled={this.state.formIsDisabled}
                  />
                  <Text className="after-input">— л. с.</Text>
                </FormElement>
              </FormGroup>
              <FormGroup className="indented-bottom">
                <FormElement className="flex">
                  <Switch id="useTrailer" className="equal-insurer" onClick={this.changeCarUseTrailer} on={this.props.useTrailer} />
                  <Label htmlFor="useTrailer" onClick={this.changeCarUseTrailer}>
                    Используется с прицепом
                  </Label>
                </FormElement>
              </FormGroup>

              { ticketCart }
              <ButtonWrapper>
                <Button type="button" disabled={this.state.formIsDisabled} className="prev" onClick={this.props.pushBackward}>
                  Назад
                </Button>
                <Button type="submit" disabled={this.state.formIsDisabled} className={this.state.formIsDisabled ? 'next next--onload half' : 'next half'}>
                  {buttonText}
                </Button>
              </ButtonWrapper>
            </Form>
          </VehicleForm>
        </VehiclePageWrapper>
        <Footer isMain={false} />
      </Background>
    );
  }
}

VehiclePage.propTypes = {
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  mark: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  model: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  year: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  category: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  type: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  purpose: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  licensePlate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  docType: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  serial: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  number: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  docDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  idType: PropTypes.string,
  vin: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  engCap: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  useTrailer: PropTypes.bool,
  ticketNumber: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  ticketDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  loadMarks: PropTypes.func,
  loadCategories: PropTypes.func,
  loadPurposes: PropTypes.func,
  loadTypes: PropTypes.func,
  changeMark: PropTypes.func,
  changeModel: PropTypes.func,
  changePurpose: PropTypes.func,
  changeYear: PropTypes.func,
  changeLicensePlate: PropTypes.func,
  changeCarDocType: PropTypes.func,
  changeCarSerial: PropTypes.func,
  changeCarNumber: PropTypes.func,
  changeCarDocDate: PropTypes.func,
  changeCarIdType: PropTypes.func,
  changeCarVin: PropTypes.func,
  changeCarEngCap: PropTypes.func,
  changeCarUseTrailer: PropTypes.func,
  changeCarTicketNumber: PropTypes.func,
  changeCarTicketDate: PropTypes.func,
  pushBackward: PropTypes.func,
  checkVehicle: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    changeMark: (event) => dispatch(changeMark(event)),
    changeModel: (event) => dispatch(changeModel(event)),
    changePurpose: (event) => dispatch(changePurpose(event)),
    changeYear: (event) => dispatch(changeYear(event.target.value)),
    changeLicensePlate: (event) => dispatch(changeLicensePlate(event.target.value.toUpperCase())),
    changeCarDocType: (event) => dispatch(changeCarDocType(event)),
    changeCarSerial: (event) => dispatch(changeCarSerial(event.target.value.toUpperCase())),
    changeCarNumber: (event) => dispatch(changeCarNumber(event.target.value.toUpperCase())),
    changeCarDocDate: (event) => dispatch(changeCarDocDate(event.target.value)),
    changeCarIdType: (event) => dispatch(changeCarIdType(event)),
    changeCarVin: (event) => dispatch(changeCarVin(event.target.value.toUpperCase())),
    changeCarEngCap: (event) => dispatch(changeCarEngCap(event.target.value)),
    changeCarUseTrailer: (event) => dispatch(changeCarUseTrailer(event)),
    changeCarTicketNumber: (event) => dispatch(changeCarTicketNumber(event.target.value)),
    changeCarTicketDate: (event) => dispatch(changeCarTicketDate(event.target.value)),
    loadMarks: () => dispatch(loadMarks()),
    loadCategories: () => dispatch(loadCategories()),
    loadPurposes: () => dispatch(loadPurposes()),
    loadTypes: () => dispatch(loadTypes()),
    pushBackward: () => dispatch(push('/')),
    pushForward: () => dispatch(push('/owner')),
    checkVehicle: () => dispatch(checkVehicle()),
  };
}

const mapStateToProps = createStructuredSelector({
  mark: createStructuredSelector({
    items: makeSelectMarks(),
    selected: makeSelectMark(),
    loading: makeSelectMarkLoading(),
  }),
  model: createStructuredSelector({
    items: makeSelectModels(),
    selected: makeSelectModel(),
    loading: makeSelectModelLoading(),
  }),
  year: makeSelectYear(),
  category: createStructuredSelector({
    items: makeSelectCategories(),
    selected: makeSelectCategory(),
    loading: makeSelectCategoryLoading(),
  }),
  type: createStructuredSelector({
    items: makeSelectTypes(),
    selected: makeSelectType(),
    loading: makeSelectTypeLoading(),
  }),
  purpose: createStructuredSelector({
    items: makeSelectPurposes(),
    selected: makeSelectPurpose(),
    loading: makeSelectPurposeLoading(),
  }),
  licensePlate: makeSelectLicense(),
  docType: makeSelectDocType(),
  serial: makeSelectSerial(),
  number: makeSelectNumber(),
  docDate: makeSelectDocDate(),
  idType: makeSelectIdType(),
  vin: makeSelectVin(),
  engCap: makeSelectEngCap(),
  useTrailer: makeSelectUseTrailer(),
  ticketNumber: makeSelectTicketNumber(),
  ticketDate: makeSelectTicketDate(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'vehicle', saga });
const withReducer = injectReducer({ key: 'car', reducer });

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(VehiclePage);
