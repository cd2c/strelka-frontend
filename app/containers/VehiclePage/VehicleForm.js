import styled from 'styled-components';

const VehicleForm = styled.section`
  margin: 30px auto;
  width: calc((100px + 100%) / 1.5);
`;

export default VehicleForm;
