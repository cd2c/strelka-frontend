/**
 * Asynchronously loads the component for VehiclePage
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

export default Loadable({
  loader: () => import('./index'),
  delay: 500,
  loading: LoadingIndicator,
});
