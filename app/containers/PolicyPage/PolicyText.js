import styled from 'styled-components';

const PolicyText = styled.div`
  margin: 30px auto;
  width: calc((100px + 100%) / 1.5);
`;

export default PolicyText;
