import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import { Helmet } from 'react-helmet';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import Background from 'components/Background';
import H2 from 'components/H2';
import Img from 'components/Img';
import Text from 'components/Text';
import SmallText from 'components/SmallText';
import Button from 'components/Button';
import AsyncSelect from 'components/AsyncSelect';
import Label from 'components/Label';
import Input from 'components/Input';
import Header from 'components/Header';
import Footer from 'components/Footer';

import { validator } from 'utils/validation';

import {
  makeSelectLoading,
  makeSelectError,
  makeSelectMarks,
  makeSelectMark,
  makeSelectModels,
  makeSelectModel,
  makeSelectYear,
} from './selectors';
import {
  loadMarks,
  loadCategories,
  changeMark,
  changeModel,
  changeYear,
} from './actions';
import reducer from './reducer';
import saga from './saga';

import HomePageWrapper from './HomePageWrapper';
import FormGroup from './FormGroup';
import FormElement from './FormElement';
import ImgWrapper from './ImgWrapper';
import SelectWrapper from './SelectWrapper';
import IconsWrapper from './IconsWrapper';
import Icon from './Icon';
import Form from './Form';
import Passport from './images/Icon_Passport.png';
import Passport2x from './images/Icon_Passport_2x.png';
import Sts from './images/Icon_STS.png';
import Sts2x from './images/Icon_STS_2x.png';
import License from './images/Icon_VU.png';
import License2x from './images/Icon_VU_2x.png';
import Technical from './images/Icon_DK.png';
import Technical2x from './images/Icon_DK_2x.png';

export class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      markInputError: false,
      modelInputError: false,
      yearInputError: false,
    };
  }

  componentDidMount() {
    if (this.props.mark.items === false) {
      this.props.loadMarks();
    }
  }

  changeYear = (event) => {
    this.setState({
      yearInputError: event.target.value === '',
    });

    this.props.changeYear(event);
  };

  validateForm = (event) => {
    event.preventDefault();
    let isValid = true;

    if (this.props.mark.selected === false) {
      isValid = false;
      this.setState({
        markInputError: true,
      });
    }

    if (this.props.model.selected === false) {
      isValid = false;
      this.setState({
        modelInputError: true,
      });
    }

    if (!validator.required(this.props.year) || !validator.carYear(this.props.year)) {
      isValid = false;
      this.setState({
        yearInputError: true,
      });
    }

    if (isValid === true) {
      this.props.submitForm();
    }
  };

  render() {
    const { loading, error, mark, model, year } = this.props;

    const marksListProps = {
      loading,
      error,
      options: mark.items,
      selected: mark.selected,
      className: this.state.markInputError ? 'error' : '',
    };

    const modelsListProps = {
      loading,
      error,
      options: model.items,
      selected: model.selected,
      className: this.state.modelInputError ? 'error' : '',
    };

    return (
      <Background>
        <Header isMain />
        <HomePageWrapper>
          <H2>
            Без очередей и&nbsp;навязчивых услуг
          </H2>
          <SelectWrapper>
            <section>
              <Text>
                Мы рекомендуем приготовить документы:
              </Text>
            </section>
            <IconsWrapper>
              <Icon>
                <ImgWrapper>
                  <Img src={Passport} srcset={Passport2x} alt="Паспорт" />
                </ImgWrapper>
                <SmallText>Паспорт</SmallText>
              </Icon>
              <Icon>
                <ImgWrapper>
                  <Img src={Sts} srcset={Sts2x} alt="СТС" />
                </ImgWrapper>
                <SmallText>СТС или ПТС</SmallText>
              </Icon>
              <Icon>
                <ImgWrapper>
                  <Img src={License} srcset={License2x} alt="Права" />
                </ImgWrapper>
                <SmallText>Права</SmallText>
              </Icon>
              <Icon>
                <ImgWrapper>
                  <Img src={Technical} srcset={Technical2x} alt="Диагностическая карта" />
                </ImgWrapper>
                <SmallText>Диагностическая карта</SmallText>
              </Icon>
            </IconsWrapper>
            <Form onSubmit={this.validateForm}>
              <FormGroup>
                <Label>
                  Марка
                </Label>
                <FormElement>
                  <AsyncSelect
                    isValid={this.state.markInputError}
                    name="marks"
                    ref={(markInput) => { this.markInput = markInput; }}
                    onChange={this.props.changeMark}
                    {...marksListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <Label>
                  Модель
                </Label>
                <FormElement>
                  <AsyncSelect
                    isValid={this.state.modelInputError}
                    name="models"
                    ref={(modelInput) => { this.modelInput = modelInput; }}
                    onChange={this.props.changeModel}
                    {...modelsListProps}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <Label>
                  Год выпуска
                </Label>
                <FormElement>
                  <Input
                    mask="9999"
                    maskChar=""
                    value={year}
                    className={this.state.yearInputError ? 'error' : ''}
                    ref={(yearInput) => { this.yearInput = yearInput; }}
                    onChange={this.changeYear}
                  />
                </FormElement>
              </FormGroup>
              <Button type="submit" className="next">
                Оформить полис Е-ОСАГО
              </Button>
            </Form>
          </SelectWrapper>
        </HomePageWrapper>
        <Footer isMain />
      </Background>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  mark: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  model: PropTypes.shape({
    items: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
    ]),
    selected: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }),
  year: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  loadMarks: PropTypes.func,
  changeMark: PropTypes.func,
  changeModel: PropTypes.func,
  changeYear: PropTypes.func,
  submitForm: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    changeMark: (event) => dispatch(changeMark(event)),
    changeModel: (event) => dispatch(changeModel(event)),
    changeYear: (event) => dispatch(changeYear(event.target.value)),
    loadMarks: () => dispatch(loadMarks()),
    loadCategories: () => dispatch(loadCategories()),
    submitForm: () => dispatch(push('/vehicle')),
  };
}

const mapStateToProps = createStructuredSelector({
  mark: createStructuredSelector({
    items: makeSelectMarks(),
    selected: makeSelectMark(),
  }),
  model: createStructuredSelector({
    items: makeSelectModels(),
    selected: makeSelectModel(),
  }),
  year: makeSelectYear(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'home', saga });
const withReducer = injectReducer({ key: 'car', reducer });

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(HomePage);
