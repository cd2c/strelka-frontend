/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOAD_MARKS,
  LOAD_MARKS_SUCCESS,
  LOAD_MARKS_ERROR,
  LOAD_MODELS,
  LOAD_MODELS_SUCCESS,
  LOAD_MODELS_ERROR,
  LOAD_CATEGORIES,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_CATEGORIES_ERROR,
  LOAD_TYPES,
  LOAD_TYPES_SUCCESS,
  LOAD_TYPES_ERROR,
  LOAD_PURPOSES,
  LOAD_PURPOSES_SUCCESS,
  LOAD_PURPOSES_ERROR,
  CHECK_VEHICLE,
  CHECK_VEHICLE_SUCCESS,
  CHECK_VEHICLE_ERROR,
  CHANGE_MARKA,
  CHANGE_MODEL,
  CHANGE_YEAR,
  CHANGE_CATEGORY,
  CHANGE_TYPE,
  CHANGE_PURPOSE,
  CHANGE_LICENSE_PLATE,
  CHANGE_CAR_DOC_TYPE,
  CHANGE_CAR_SERIAL,
  CHANGE_CAR_NUMBER,
  CHANGE_CAR_DOC_DATE,
  CHANGE_CAR_ID_TYPE,
  CHANGE_CAR_VIN,
  CHANGE_CAR_ENG_CAP,
  CHANGE_CAR_USE_TRAILER,
  CHANGE_CAR_TICKET_NUMBER,
  CHANGE_CAR_TICKET_DATE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  mark: {
    items: false,
    selected: false,
    loading: false,
  },
  model: {
    items: false,
    selected: false,
    loading: false,
  },
  year: false,
  category: {
    items: false,
    selected: false,
    loading: false,
  },
  type: {
    items: false,
    selected: false,
    loading: false,
  },
  purpose: {
    items: false,
    selected: false,
    loading: false,
  },
  licensePlate: false,
  docType: '31',
  serial: false,
  number: false,
  docDate: false,
  idType: '1',
  vin: false,
  engCap: false,
  useTrailer: false,
  ticketNumber: false,
  ticketDate: false,
  apiId: false,

  loading: false,
  error: false,
  checkLoading: false,
});

function carReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_MARKS:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['mark', 'items'], false)
        .setIn(['mark', 'selected'], false)
        .setIn(['mark', 'loading'], true);
    case LOAD_MARKS_SUCCESS:
      return state
        .setIn(['mark', 'items'], action.marks)
        .setIn(['mark', 'loading'], false);
    case LOAD_MARKS_ERROR:
      return state
        .set('error', action.error)
        .setIn(['mark', 'loading'], false);
    case LOAD_MODELS:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['model', 'items'], false)
        .setIn(['model', 'selected'], false)
        .setIn(['model', 'loading'], true);
    case LOAD_MODELS_SUCCESS:
      return state
        .setIn(['model', 'items'], action.models)
        .setIn(['model', 'loading'], false);
    case LOAD_MODELS_ERROR:
      return state
        .set('error', action.error)
        .setIn(['model', 'loading'], false);
    case LOAD_CATEGORIES:
      return state
        .set('error', false)
        .setIn(['category', 'loading'], true)
        .setIn(['category', 'items'], false)
        .setIn(['category', 'selected'], false);
    case LOAD_CATEGORIES_SUCCESS:
      return state
        .setIn(['category', 'items'], action.categories)
        .setIn(['category', 'loading'], false);
    case LOAD_CATEGORIES_ERROR:
      return state
        .set('error', action.error)
        .setIn(['category', 'loading'], false);
    case LOAD_TYPES:
      return state
        .set('error', false)
        .setIn(['type', 'loading'], true)
        .setIn(['type', 'items'], false);
    case LOAD_TYPES_SUCCESS:
      return state
        .setIn(['type', 'items'], action.types)
        .setIn(['type', 'loading'], false);
    case LOAD_TYPES_ERROR:
      return state
        .set('error', action.error)
        .setIn(['type', 'loading'], false);
    case LOAD_PURPOSES:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['purpose', 'items'], false)
        .setIn(['purpose', 'selected'], false)
        .setIn(['purpose', 'loading'], true);
    case LOAD_PURPOSES_SUCCESS:
      return state
        .setIn(['purpose', 'items'], action.purposes)
        .setIn(['purpose', 'loading'], false);
    case LOAD_PURPOSES_ERROR:
      return state
        .set('error', action.error)
        .setIn(['purpose', 'loading'], false);
    case CHANGE_MARKA:
      return state
        .setIn(['mark', 'selected'], action.marka)
        .setIn(['model', 'loading'], true);
    case CHANGE_MODEL:
      return state
        .setIn(['model', 'selected'], action.model);
    case CHANGE_YEAR:
      return state
        .setIn(['year'], action.year);
    case CHANGE_CATEGORY:
      return state
        .setIn(['category', 'selected'], action.category);
    case CHANGE_TYPE:
      return state
        .setIn(['type', 'selected'], action.typeObj);
    case CHANGE_PURPOSE:
      return state
        .setIn(['purpose', 'selected'], action.purpose);
    case CHANGE_LICENSE_PLATE:
      return state
        .set('licensePlate', action.license);
    case CHANGE_CAR_DOC_TYPE:
      return state
        .set('docType', action.docType);
    case CHANGE_CAR_SERIAL:
      return state
        .set('serial', action.serial);
    case CHANGE_CAR_NUMBER:
      return state
        .set('number', action.number);
    case CHANGE_CAR_DOC_DATE:
      return state
        .set('docDate', action.docDate);
    case CHANGE_CAR_ID_TYPE:
      return state
        .set('idType', action.idType);
    case CHANGE_CAR_VIN:
      return state
        .set('vin', action.vin);
    case CHANGE_CAR_ENG_CAP:
      return state
        .set('engCap', action.engCap);
    case CHANGE_CAR_USE_TRAILER:
      return state
        .set('useTrailer', action.useTrailer);
    case CHANGE_CAR_TICKET_NUMBER:
      return state
        .set('ticketNumber', action.ticketNumber);
    case CHANGE_CAR_TICKET_DATE:
      return state
        .set('ticketDate', action.ticketDate);
    case CHECK_VEHICLE:
      return state
        .set('checkLoading', true)
        .set('error', false)
        .set('apiId', false);
    case CHECK_VEHICLE_SUCCESS:
      return state
        .set('checkLoading', false)
        .set('error', false)
        .set('apiId', action.apiId);
    case CHECK_VEHICLE_ERROR:
      return state
        .set('error', action.error)
        .set('checkLoading', false);
    default:
      return state;
  }
}

export default carReducer;
