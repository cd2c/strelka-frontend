import styled from 'styled-components';

const SelectWrapper = styled.section`
  padding: 15px;
  background: rgba(255, 255, 255, .8);
`;

export default SelectWrapper;
