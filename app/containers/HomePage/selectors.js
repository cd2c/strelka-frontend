import { createSelector } from 'reselect';

const selectCar = (state) => state.get('car');

const makeSelectLoading = () => createSelector(
  selectCar,
  (carState) => carState.get('loading')
);

const makeSelectError = () => createSelector(
  selectCar,
  (carState) => carState.get('error')
);

const makeSelectMarks = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['mark', 'items'])
);

const makeSelectMark = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['mark', 'selected'])
);

const makeSelectMarkLoading = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['mark', 'loading'])
);

const makeSelectModels = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['model', 'items'])
);

const makeSelectModel = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['model', 'selected'])
);

const makeSelectModelLoading = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['model', 'loading'])
);

const makeSelectYear = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['year'])
);

const makeSelectCategories = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['category', 'items'])
);

const makeSelectCategory = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['category', 'selected'])
);

const makeSelectCategoryLoading = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['category', 'loading'])
);

const makeSelectTypes = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['type', 'items'])
);

const makeSelectType = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['type', 'selected'])
);

const makeSelectTypeLoading = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['type', 'loading'])
);

const makeSelectPurposes = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['purpose', 'items'])
);

const makeSelectPurpose = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['purpose', 'selected'])
);

const makeSelectPurposeLoading = () => createSelector(
  selectCar,
  (carState) => carState.getIn(['purpose', 'loading'])
);


const makeSelectLicense = () => createSelector(
  selectCar,
  (carState) => carState.get('licensePlate')
);

const makeSelectDocType = () => createSelector(
  selectCar,
  (carState) => carState.get('docType')
);

const makeSelectSerial = () => createSelector(
  selectCar,
  (carState) => carState.get('serial')
);

const makeSelectNumber = () => createSelector(
  selectCar,
  (carState) => carState.get('number')
);

const makeSelectDocDate = () => createSelector(
  selectCar,
  (carState) => carState.get('docDate')
);

const makeSelectIdType = () => createSelector(
  selectCar,
  (carState) => carState.get('idType')
);

const makeSelectVin = () => createSelector(
  selectCar,
  (carState) => carState.get('vin')
);

const makeSelectEngCap = () => createSelector(
  selectCar,
  (carState) => carState.get('engCap')
);

const makeSelectUseTrailer = () => createSelector(
  selectCar,
  (carState) => carState.get('useTrailer')
);

const makeSelectTicketNumber = () => createSelector(
  selectCar,
  (carState) => carState.get('ticketNumber')
);

const makeSelectTicketDate = () => createSelector(
  selectCar,
  (carState) => carState.get('ticketDate')
);

const makeSelectApiId = () => createSelector(
  selectCar,
  (carState) => carState.get('apiId')
);


export {
  selectCar,
  makeSelectLoading,
  makeSelectError,
  makeSelectMarks,
  makeSelectMark,
  makeSelectModels,
  makeSelectModel,
  makeSelectYear,
  makeSelectCategories,
  makeSelectCategory,
  makeSelectTypes,
  makeSelectType,
  makeSelectPurposes,
  makeSelectPurpose,
  makeSelectLicense,
  makeSelectSerial,
  makeSelectNumber,
  makeSelectDocDate,
  makeSelectIdType,
  makeSelectVin,
  makeSelectEngCap,
  makeSelectUseTrailer,
  makeSelectTicketNumber,
  makeSelectTicketDate,
  makeSelectApiId,
  makeSelectDocType,
  makeSelectTypeLoading,
  makeSelectMarkLoading,
  makeSelectModelLoading,
  makeSelectCategoryLoading,
  makeSelectPurposeLoading,
};
