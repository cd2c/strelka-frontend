import styled from 'styled-components';

const ImgWrapper = styled.div`
  padding: 4px 0;
`;

export default ImgWrapper;
