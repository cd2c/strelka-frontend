import styled from 'styled-components';

const HomePageWrapper = styled.section`
  margin: 0 auto 40px;
  padding: 0 20px;
  max-width: 400px;
`;

export default HomePageWrapper;
