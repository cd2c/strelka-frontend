import styled from 'styled-components';

const IconsWrapper = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export default IconsWrapper;
