/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_MARKS = 'e-osago/Feedback/LOAD_MARKS';
export const LOAD_MARKS_SUCCESS = 'e-osago/Feedback/LOAD_MARKS_SUCCESS';
export const LOAD_MARKS_ERROR = 'e-osago/Feedback/LOAD_MARKS_ERROR';

export const LOAD_MODELS = 'e-osago/Feedback/LOAD_MODELS';
export const LOAD_MODELS_SUCCESS = 'e-osago/Feedback/LOAD_MODELS_SUCCESS';
export const LOAD_MODELS_ERROR = 'e-osago/Feedback/LOAD_MODELS_ERROR';

export const LOAD_CATEGORIES = 'e-osago/Feedback/LOAD_CATEGORIES';
export const LOAD_CATEGORIES_SUCCESS = 'e-osago/Feedback/LOAD_CATEGORIES_SUCCESS';
export const LOAD_CATEGORIES_ERROR = 'e-osago/Feedback/LOAD_CATEGORIES_ERROR';

export const LOAD_TYPES = 'e-osago/Feedback/LOAD_TYPES';
export const LOAD_TYPES_SUCCESS = 'e-osago/Feedback/LOAD_TYPES_SUCCESS';
export const LOAD_TYPES_ERROR = 'e-osago/Feedback/LOAD_TYPES_ERROR';

export const LOAD_PURPOSES = 'e-osago/Feedback/LOAD_PURPOSES';
export const LOAD_PURPOSES_SUCCESS = 'e-osago/Feedback/LOAD_PURPOSES_SUCCESS';
export const LOAD_PURPOSES_ERROR = 'e-osago/Feedback/LOAD_PURPOSES_ERROR';

export const CHECK_VEHICLE = 'e-osago/Feedback/CHECK_VEHICLE';
export const CHECK_VEHICLE_SUCCESS = 'e-osago/Feedback/CHECK_VEHICLE_SUCCESS';
export const CHECK_VEHICLE_ERROR = 'e-osago/Feedback/CHECK_VEHICLE_ERROR';

export const CHANGE_MARKA = 'e-osago/Feedback/CHANGE_MARKA';
export const CHANGE_MODEL = 'e-osago/Feedback/CHANGE_MODEL';
export const CHANGE_YEAR = 'e-osago/Feedback/CHANGE_YEAR';
export const CHANGE_CATEGORY = 'e-osago/Feedback/CHANGE_CATEGORY';
export const CHANGE_TYPE = 'e-osago/Feedback/CHANGE_TYPE';
export const CHANGE_PURPOSE = 'e-osago/Feedback/CHANGE_PURPOSE';
export const CHANGE_LICENSE_PLATE = 'e-osago/Feedback/CHANGE_LICENSE_PLATE';
export const CHANGE_CAR_DOC_TYPE = 'e-osago/Feedback/CHANGE_CAR_DOC_TYPE';
export const CHANGE_CAR_SERIAL = 'e-osago/Feedback/CHANGE_CAR_SERIAL';
export const CHANGE_CAR_NUMBER = 'e-osago/Feedback/CHANGE_CAR_NUMBER';
export const CHANGE_CAR_DOC_DATE = 'e-osago/Feedback/CHANGE_CAR_DOC_DATE';
export const CHANGE_CAR_ID_TYPE = 'e-osago/Feedback/CHANGE_CAR_ID_TYPE';
export const CHANGE_CAR_VIN = 'e-osago/Feedback/CHANGE_CAR_VIN';
export const CHANGE_CAR_ENG_CAP = 'e-osago/Feedback/CHANGE_CAR_ENG_CAP';
export const CHANGE_CAR_USE_TRAILER = 'e-osago/Feedback/CHANGE_CAR_USE_TRAILER';
export const CHANGE_CAR_TICKET_NUMBER = 'e-osago/Feedback/CHANGE_CAR_TICKET_NUMBER';
export const CHANGE_CAR_TICKET_DATE = 'e-osago/Feedback/CHANGE_CAR_TICKET_DATE';

export const DEFAULT_LOCALE = 'en';
