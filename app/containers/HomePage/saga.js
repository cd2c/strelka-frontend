import { call, put, select, takeEvery } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import * as Sentry from '@sentry/browser';
import request from 'utils/request';

import {
  LOAD_MARKS,
  CHANGE_MARKA,
  CHANGE_MODEL,
  LOAD_CATEGORIES,
  LOAD_PURPOSES,
  LOAD_TYPES,
  CHECK_VEHICLE,
} from './constants';
import {
  marksLoaded,
  marksLoadingError,
  modelsLoaded,
  modelsLoadingError,
  typesLoaded,
  typesLoadingError,
  categoriesLoaded,
  categoriesLoadingError,
  purposesLoaded,
  purposesLoadingError,
  changeCategory,
  changeType,
  checkVehicleSuccess,
  checkVehicleError,
} from './actions';

import {
  makeSelectMark,
  makeSelectModel,
  makeSelectYear,
  makeSelectPurpose,
  makeSelectLicense,
  makeSelectDocType,
  makeSelectSerial,
  makeSelectNumber,
  makeSelectDocDate,
  makeSelectVin,
  makeSelectEngCap,
  makeSelectUseTrailer,
  makeSelectTicketNumber,
  makeSelectTicketDate,
  makeSelectIdType,
} from './selectors';

/**
 * Marks request/response handler
 */
export function* getMarks() {
  // Select username from store
  const requestURL = 'marka';

  try {
    // Call our request helper (see 'utils/request')
    const marks = yield call(request, requestURL);
    yield put(marksLoaded(marks));
  } catch (err) {
    yield put(marksLoadingError(err));
  }
}

/**
 * Model request/response handler
 */
export function* getModels() {
  // Select mark from store
  const mark = yield select(makeSelectMark());
  const requestURL = `model/${mark.id}`;

  try {
    // Call our request helper (see 'utils/request')
    const models = yield call(request, requestURL);
    yield put(modelsLoaded(models));
  } catch (err) {
    yield put(modelsLoadingError(err));
  }
}

export function* getCategoriesAndTypes() {
  const model = yield select(makeSelectModel());
  const categoryRequest = `category/${model.categoryId}`;
  const typeRequest = `type/${model.typeId}`;

  try {
    const categories = {
      selected: yield call(request, categoryRequest),
    };
    yield put(changeCategory(categories.selected));

    const types = {
      selected: yield call(request, typeRequest),
    };
    yield put(changeType(types.selected));
  } catch (err) {
    yield put(categoriesLoadingError(err));
  }
}

export function* getCategories() {
  const categoryRequest = 'category';

  try {
    const categories = yield call(request, categoryRequest);
    yield put(categoriesLoaded(categories));
  } catch (err) {
    yield put(categoriesLoadingError(err));
  }
}

export function* getPurposes() {
  const requestURL = 'purpose';

  try {
    const purposes = yield call(request, requestURL);
    yield put(purposesLoaded(purposes));
  } catch (err) {
    yield put(purposesLoadingError(err));
  }
}

export function* getTypes() {
  const requestURL = 'type';

  try {
    const types = yield call(request, requestURL);
    yield put(typesLoaded(types));
  } catch (err) {
    yield put(typesLoadingError(err));
  }
}

export function* checkVehicle() {
  const mark = yield select(makeSelectMark());
  const model = yield select(makeSelectModel());
  const licensePlate = yield select(makeSelectLicense());
  const idType = yield select(makeSelectIdType());
  const vin = yield select(makeSelectVin());
  const yearIssue = yield select(makeSelectYear());
  const serial = yield select(makeSelectSerial());
  const number = yield select(makeSelectNumber());
  const documentCar = yield select(makeSelectDocType());
  const carDateText = yield select(makeSelectDocDate());
  const engCap = yield select(makeSelectEngCap());
  const useTrailer = yield select(makeSelectUseTrailer());
  const purpose = yield select(makeSelectPurpose());
  let ticketNumber = yield select(makeSelectTicketNumber());
  let ticketDateText = yield select(makeSelectTicketDate());

  if (ticketNumber === false) {
    ticketNumber = null;
  }

  if (ticketDateText === false) {
    ticketDateText = 0;
  }

  const vehicle = {
    markaId: mark.id,
    modelId: model.id,
    purposeId: purpose.id,
    licensePlate: licensePlate.replace(/\s/g, ''),
    yearIssue,
    documentCar,
    serial,
    number,
    carDateText,
    engCap,
    useTrailer,
    ticketNumber,
    ticketDateText,
  };

  if (idType === '1') {
    vehicle.vin = vin;
  } else if (idType === '2') {
    vehicle.bodyNumber = vin;
  } else if (idType === '3') {
    vehicle.chassisNumber = vin;
  }

  const requestURL = 'vehicle';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(vehicle),
  };

  try {
    yield put(showLoading());
    const checkResult = yield call(request, requestURL, options);

    if (process.env.NODE_ENV !== 'development') {
      try {
        window.yaCounter47277876.reachGoal('vehicle');
      } catch (e) {
        console.log(e);
      }
    }

    yield put(checkVehicleSuccess(checkResult.id));
  } catch (err) {
    yield put(checkVehicleError(err));
    if (process.env.NODE_ENV === 'production') {
      Sentry.captureException(err);
    }
  } finally {
    yield put(hideLoading());
    yield put(push('/owner'));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* marksData() {
  yield takeEvery(LOAD_MARKS, getMarks);
  yield takeEvery(LOAD_CATEGORIES, getCategories);
  yield takeEvery(LOAD_TYPES, getTypes);
  yield takeEvery(CHANGE_MARKA, getModels);
  yield takeEvery(CHANGE_MODEL, getCategoriesAndTypes);
  yield takeEvery(LOAD_PURPOSES, getPurposes);
  yield takeEvery(CHECK_VEHICLE, checkVehicle);
}
