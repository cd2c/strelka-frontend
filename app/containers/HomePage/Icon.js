import styled from 'styled-components';

const Icon = styled.div`
  max-width: 85px;
  text-align: center;
`;

export default Icon;
