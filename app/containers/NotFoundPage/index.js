import React from 'react';
import { Helmet } from 'react-helmet';

import Background from 'components/Background';
import H2 from 'components/H2';
import Header from 'components/Header';
import Footer from 'components/Footer';

import Button from './styled/Button';
import Inner from './styled/Inner';
import Wrapper from './styled/Wrapper';
import Text from './styled/Text';

export default class NotFound extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Background isNotFound>
        <Helmet>
          <title>Не найдено</title>
        </Helmet>
        <Header isNotFound isMain />
        <Wrapper>
          <Inner>
            <H2>
              Гав-гав, 404!
            </H2>
            <Text>
              Клиент был в состоянии общаться
              с&nbsp;сервером, но сервер не может найти данные согласно запросу.
            </Text>
            <Button to="/">На главную</Button>
          </Inner>
        </Wrapper>
        <Footer isMain />
      </Background>
    );
  }
}
