import styled from 'styled-components';

const Inner = styled.section`
  margin: 0 auto;
  max-width: 600px;
  min-width: 280px;
  width: 100%;
`;

export default Inner;
