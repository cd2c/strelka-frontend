import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Button = styled(Link)`
  align-items: center;
  background-color: #07A1F4;
  border: 1px solid #fff;
  border-radius: 12px;
  display: flex;
  cursor: pointer;
  position: relative;

  color: #fff;
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  height: 60px;
  justify-content: center;
  letter-spacing: -0.5px;
  margin-top: 25px;
  padding: 0 30px;
  text-decoration: none;
  transition: border .3s ease;
  width: fit-content;

  &:hover {
    border: 1px solid rgba(255, 255, 255, .5);
  }

  &:active, &:focus {
    border: none;
    outline: none;
  }
`;

export default Button;
