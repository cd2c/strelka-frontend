import styled from 'styled-components';

const Text = styled.p`
  color: #FFFFFF;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  margin-top: 40px;
  max-width: 300px;
`;

export default Text;
