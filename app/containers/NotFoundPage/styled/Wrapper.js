import styled from 'styled-components';

const Wrapper = styled.section`
  margin: 0 auto;
  padding: 30px 0;
  min-height: calc(100vh - 150px);
  width: 100%;
`;

export default Wrapper;
