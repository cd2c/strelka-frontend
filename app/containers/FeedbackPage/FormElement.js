import styled from 'styled-components';

const FormElement = styled.div`
  width: 100%;
`;

export default FormElement;
