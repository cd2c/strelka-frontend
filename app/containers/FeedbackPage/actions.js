import {
  CHANGE_NAME,
  CHANGE_EMAIL,
  CHANGE_PHONE,
  CHANGE_TEXT,
  CHANGE_CALC_ID,
  SEND_FEEDBACK_REQUEST,
  SEND_FEEDBACK_REQUEST_SUCCESS,
  SEND_FEEDBACK_REQUEST_FAIL,
} from './constants';

export function changeName(payload) {
  return {
    type: CHANGE_NAME,
    name: payload,
  };
}

export function changeEmail(payload) {
  return {
    type: CHANGE_EMAIL,
    email: payload,
  };
}

export function changePhone(payload) {
  return {
    type: CHANGE_PHONE,
    phone: payload,
  };
}

export function changeText(payload) {
  return {
    type: CHANGE_TEXT,
    text: payload,
  };
}

export function changeCalcId(payload) {
  return {
    type: CHANGE_CALC_ID,
    calcId: payload,
  };
}

export function sendFeedback() {
  return {
    type: SEND_FEEDBACK_REQUEST,
  };
}

export function sendFeedbackSuccess() {
  return {
    type: SEND_FEEDBACK_REQUEST_SUCCESS,
  };
}

export function sendFeedbackFail() {
  return {
    type: SEND_FEEDBACK_REQUEST_FAIL,
  };
}
