export const CHANGE_NAME = 'e-osago/Feedback/CHANGE_NAME';
export const CHANGE_EMAIL = 'e-osago/Feedback/CHANGE_EMAIL';
export const CHANGE_PHONE = 'e-osago/Feedback/CHANGE_PHONE';
export const CHANGE_TEXT = 'e-osago/Feedback/CHANGE_TEXT';
export const CHANGE_CALC_ID = 'e-osago/Feedback/CHANGE_CALC_ID';
export const SEND_FEEDBACK_REQUEST = 'e-osago/Feedback/SEND_FEEDBACK_REQUEST';
export const SEND_FEEDBACK_REQUEST_SUCCESS = 'e-osago/Feedback/SEND_FEEDBACK_REQUEST_SUCCESS';
export const SEND_FEEDBACK_REQUEST_FAIL = 'e-osago/Feedback/SEND_FEEDBACK_REQUEST_FAIL';
