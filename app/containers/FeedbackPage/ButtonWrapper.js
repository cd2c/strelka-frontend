import styled from 'styled-components';

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 40px 0 45px;
`;

export default ButtonWrapper;
