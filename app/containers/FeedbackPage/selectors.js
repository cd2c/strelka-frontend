import { createSelector } from 'reselect';

const selectFeedback = (state) => state.get('feedback');

const makeSelectLoading = () => createSelector(
  selectFeedback,
  (feedbackState) => feedbackState.get('isLoading')
);

const makeSelectName = () => createSelector(
  selectFeedback,
  (feedbackState) => feedbackState.get('name')
);

const makeSelectEmail = () => createSelector(
  selectFeedback,
  (feedbackState) => feedbackState.get('email')
);

const makeSelectPhone = () => createSelector(
  selectFeedback,
  (feedbackState) => feedbackState.get('phone')
);

const makeSelectText = () => createSelector(
  selectFeedback,
  (feedbackState) => feedbackState.get('text')
);

const makeSelectCalcId = () => createSelector(
  selectFeedback,
  (feedbackState) => feedbackState.get('calcId')
);

export {
  makeSelectName,
  makeSelectEmail,
  makeSelectPhone,
  makeSelectText,
  makeSelectCalcId,
  makeSelectLoading,
};
