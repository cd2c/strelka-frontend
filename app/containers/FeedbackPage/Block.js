import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  background: #FFFFFF;
  box-shadow: 0 0 24px 0 rgba(0, 0, 0, .1);
  display: flex;
  margin: 20px auto;
  padding: 40px 100px 40px;
  width: 800px;

  @media (max-width: 840px) {
    padding: 40px 0 0;
    margin-bottom: 0;
    margin-top: 0;
    width: 100%;
  }
`;

const BlockInner = styled.div`
  width: 100%;

  @media (max-width: 840px) {
    margin: auto;
    width: calc((100px + 100%) / 1.5);
  }
`;

const Block = (props) => (
  <Wrapper>
    <BlockInner>
      {props.children}
    </BlockInner>
  </Wrapper>
);

Block.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Block;
