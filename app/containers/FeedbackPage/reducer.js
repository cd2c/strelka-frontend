import { fromJS } from 'immutable';

import {
  CHANGE_NAME,
  CHANGE_EMAIL,
  CHANGE_PHONE,
  CHANGE_TEXT,
  CHANGE_CALC_ID,
  SEND_FEEDBACK_REQUEST,
  SEND_FEEDBACK_REQUEST_SUCCESS,
  SEND_FEEDBACK_REQUEST_FAIL,
} from './constants';

const initialState = fromJS({
  name: '',
  email: '',
  phone: '',
  text: '',
  calcId: null,
  isLoading: false,
});

function feedbackReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_NAME:
      return state.set('name', action.name);
    case CHANGE_EMAIL:
      return state.set('email', action.email);
    case CHANGE_PHONE:
      return state.set('phone', action.phone);
    case CHANGE_TEXT:
      return state.set('text', action.text);
    case CHANGE_CALC_ID:
      return state.set('calcId', action.calcId);
    case SEND_FEEDBACK_REQUEST:
      return state.set('isLoading', true);
    case SEND_FEEDBACK_REQUEST_SUCCESS:
      return state.set('isLoading', false);
    case SEND_FEEDBACK_REQUEST_FAIL:
      return state.set('isLoading', false);
    default:
      return state;
  }
}

export default feedbackReducer;
