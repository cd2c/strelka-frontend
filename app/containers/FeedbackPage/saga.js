import { call, put, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import request from 'utils/request';

import {
  SEND_FEEDBACK_REQUEST,
} from './constants';
import {
  sendFeedbackSuccess,
  sendFeedbackFail,
} from './actions';

import {
  makeSelectName,
  makeSelectEmail,
  makeSelectPhone,
  makeSelectText,
  makeSelectCalcId,
} from './selectors';

export function* sendFeedbackSaga() {
  const name = yield select(makeSelectName());
  const email = yield select(makeSelectEmail());
  const phone = yield select(makeSelectPhone());
  const text = yield select(makeSelectText());
  const calcId = yield select(makeSelectCalcId());

  const feedback = {
    name,
    email,
    phone,
    text,
    calcId,
  };

  const requestURL = 'feedback';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(feedback),
  };

  try {
    yield call(request, requestURL, options);
    yield put(sendFeedbackSuccess());
  } catch (err) {
    yield put(sendFeedbackFail());
  } finally {
    yield put(push('/feedbackSuccess'));
  }
}

export default function* feedbackSaga() {
  yield takeLatest(SEND_FEEDBACK_REQUEST, sendFeedbackSaga);
}
