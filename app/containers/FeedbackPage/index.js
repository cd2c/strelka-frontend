import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Spinner from 'react-spinkit';

import injectSaga from 'utils/injectSaga';
import { validator } from 'utils/validation';

import Background from 'components/Background';
import Button from 'components/Button';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Input from 'components/Input';
import Textarea from 'components/Textarea';

import saga from './saga';
import {
  makeSelectName,
  makeSelectEmail,
  makeSelectPhone,
  makeSelectText,
  makeSelectLoading,
} from './selectors';
import {
  changeName,
  changeEmail,
  changePhone,
  changeText,
  sendFeedback,
} from './actions';

import Block from './Block';
import ButtonWrapper from './ButtonWrapper';
import Center from './Center';
import FeedbackPageWrapper from './FeedbackPageWrapper';
import Form from './Form';
import FormElement from './FormElement';
import FormGroup from './FormGroup';
import Headline from './Headline';

class FeedbackPage extends React.PureComponent {
  state = {
    nameInputError: false,
    emailInputError: false,
    phoneInputError: false,
    textInputError: false,
  };

  changeName = (e) => {
    this.setState({
      nameInputError: false,
    });

    this.props.changeName(e.target.value);
  };

  changeEmail = (e) => {
    this.setState({
      emailInputError: false,
    });

    this.props.changeEmail(e.target.value);
  };

  changePhone = (e) => {
    this.setState({
      phoneInputError: false,
    });

    this.props.changePhone(e.target.value);
  };

  changeText = (e) => {
    this.setState({
      textInputError: false,
    });

    this.props.changeText(e.target.value);
  };

  validateForm = (e) => {
    e.preventDefault();
    let isValid = true;

    if (!validator.required(this.props.name)) {
      isValid = false;
      this.setState({
        nameInputError: true,
      });
    }

    if (!validator.email(this.props.email)) {
      isValid = false;
      this.setState({
        emailInputError: true,
      });
    }

    if (!validator.phone(this.props.phone)) {
      isValid = false;
      this.setState({
        phoneInputError: true,
      });
    }

    if (!validator.required(this.props.text)) {
      isValid = false;
      this.setState({
        textInputError: true,
      });
    }

    if (isValid) {
      this.props.sendFeedback();
    }
  };

  render() {
    let buttonText = 'Отправить';
    if (this.props.isLoading) {
      buttonText = (
        <Center>
          <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
        </Center>
      );
    }

    return (
      <Background className="not-main">
        <Helmet>
          <title>обратная связь</title>
        </Helmet>
        <Header isMain />
        <FeedbackPageWrapper>
          <Block>
            <Headline>Обратная связь</Headline>
            <Form>
              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.nameInputError ? 'error' : ''}
                    id="name"
                    label="Имя"
                    onChange={this.changeName}
                    value={this.props.name}
                    disabled={this.props.isLoading}
                  />
                </FormElement>
              </FormGroup>

              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.emailInputError ? 'error' : ''}
                    id="email"
                    label="Электронная почта"
                    onChange={this.changeEmail}
                    value={this.props.email}
                    disabled={this.props.isLoading}
                  />
                </FormElement>
              </FormGroup>

              <FormGroup>
                <FormElement>
                  <Input
                    className={this.state.phoneInputError ? 'error' : ''}
                    id="phone"
                    mask="+7 999 999-99-99"
                    maskChar=""
                    placeholder="+7 000 000-00-00"
                    label="Телефон"
                    onChange={this.changePhone}
                    value={this.props.phone}
                    disabled={this.props.isLoading}
                  />
                </FormElement>
              </FormGroup>

              <FormGroup>
                <FormElement>
                  <Textarea
                    className={this.state.textInputError ? 'error' : ''}
                    id="text"
                    label="Сообщение"
                    onChange={this.changeText}
                    value={this.props.text}
                    disabled={this.props.isLoading}
                  />
                </FormElement>
              </FormGroup>

              <ButtonWrapper>
                <Button type="submit" disabled={this.props.isLoading} className={this.props.isLoading ? 'next next--onload' : 'next'} onClick={this.validateForm}>
                  {buttonText}
                </Button>
              </ButtonWrapper>
            </Form>
          </Block>
        </FeedbackPageWrapper>
        <Footer isMain={false} />
      </Background>
    );
  }
}

FeedbackPage.propTypes = {
  changeName: PropTypes.func,
  changeEmail: PropTypes.func,
  changePhone: PropTypes.func,
  changeText: PropTypes.func,
  sendFeedback: PropTypes.func,
  name: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  text: PropTypes.string,
  isLoading: PropTypes.bool,
};

export function mapDispatchToProps(dispatch) {
  return {
    changeName: (value) => dispatch(changeName(value)),
    changeEmail: (value) => dispatch(changeEmail(value)),
    changePhone: (value) => dispatch(changePhone(value)),
    changeText: (value) => dispatch(changeText(value)),
    sendFeedback: () => dispatch(sendFeedback()),
  };
}

const mapStateToProps = createStructuredSelector({
  name: makeSelectName(),
  email: makeSelectEmail(),
  phone: makeSelectPhone(),
  text: makeSelectText(),
  isLoading: makeSelectLoading(),
});

const withSaga = injectSaga({ key: 'feedback', saga });
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withSaga,
  withConnect,
)(FeedbackPage);
