import styled from 'styled-components';

const Headline = styled.div`
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  color: #404040;
  letter-spacing: -0.5px;
  line-height: 23px;
`;

export default Headline;
