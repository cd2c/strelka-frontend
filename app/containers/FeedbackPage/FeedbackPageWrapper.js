import styled from 'styled-components';

const FeedbackPageWrapper = styled.section`
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  padding: 30px 0 30px 0;
  position: relative;
  margin: 0 auto;
  min-height: calc(100vh - 150px);
  min-width: 320px;
  max-width: 800px;
  width: 100%;

  @media (max-width: 840px) {
    margin: 0 auto;
    min-height: calc(100vh - 238px);
    padding-bottom: 0;
    padding-top: 0;
  }
`;

export default FeedbackPageWrapper;

