import {
  GET_CONTRACT_ID,
  GET_CONTRACT_ID_SUCCESS,
  GET_CONTRACT_ID_ERROR,
} from './constants';

export function getContractId(payload) {
  return {
    type: GET_CONTRACT_ID,
    ...payload,
  };
}

export function getContractIdSuccess() {
  return {
    type: GET_CONTRACT_ID_SUCCESS,
  };
}

export function getContractIdFail() {
  return {
    type: GET_CONTRACT_ID_ERROR,
  };
}
