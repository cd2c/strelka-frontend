import { fromJS } from 'immutable';

import {
  GET_CONTRACT_ID,
} from './constants';

const initialState = fromJS({
  orderId: null,
});

function successReducer(state = initialState, action) {
  switch (action.type) {
    case GET_CONTRACT_ID:
      return state.set('orderId', action.orderId);
    default:
      return state;
  }
}

export default successReducer;
