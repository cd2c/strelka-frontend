import { call, put, select, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';

import {
  GET_CONTRACT_ID,
} from './constants';

import {
  getContractIdSuccess,
  getContractIdFail,
} from './actions';

import {
  makeSelectOrderId,
} from './selectors';

export function* getContractIdSaga() {
  const orderId = yield select(makeSelectOrderId());

  try {
    const requestURL = 'success';
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        orderId,
      }),
    };

    yield call(request, requestURL, options);

    if (process.env.NODE_ENV !== 'development') {
      try {
        window.yaCounter47277876.reachGoal('success');
      } catch (e) {
        console.log(e);
      }
    }
    yield put(getContractIdSuccess());
  } catch (err) {
    yield put(getContractIdFail());
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* successSaga() {
  yield takeEvery(GET_CONTRACT_ID, getContractIdSaga);
}
