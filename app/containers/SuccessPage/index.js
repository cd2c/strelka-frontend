import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Background from 'components/Background';
import Header from 'components/Header';
import Text from 'components/Text';
import H2 from 'components/H2';
import Footer from 'components/Footer';
import Step from 'components/Step';
import Link from 'components/Link';

import { getParam } from 'utils/getParam';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import {
  getContractId,
} from './actions';
import reducer from './reducer';
import saga from './saga';

import ButtonLink from './Link';
import Dog from './success.png';
import Image from './Image';
import SuccessPageWrapper from './SuccessPageWrapper';
import SuccessText from './SuccessText';

class SuccessPage extends React.Component {
  componentDidMount() {
    const result = getParam('orderId');

    this.props.getContractId({
      orderId: result,
    });
  }

  render() {
    return (
      <Background className="not-main">
        <Helmet>
          <title>Полис оформлен</title>
        </Helmet>
        <Header isMain={false} />
        <SuccessPageWrapper>
          <Step step="success" />
          <SuccessText>
            <H2 className="black">Хорошего дня и&nbsp;удачи&nbsp;на&nbsp;дорогах!</H2>
            <Text className="success-text">Поздравляем! Ваш полис Е-ОСАГО успешно оформлен сроком на 1 год.</Text>
            <Text className="success-text">Мы пришлем полис вам на указанную почту и обязательно напомним о его продлении.</Text>
            <Text className="success-text">Во время проверки документов, инспектору ДПС необходимо будет предоставить только номер страхового полиса. Но лучше с собой возить и&nbsp;распечатанную версию.</Text>
            <Text className="success-text">Если вы не получили полис, <Link to="/feedback">напишите нам</Link></Text>
            <Text className="success-text">
              <ButtonLink to="/vehicle">Оформить еще один полис</ButtonLink>
            </Text>
          </SuccessText>
          <Image src={Dog} alt="" />
        </SuccessPageWrapper>
        <Footer isMain={false} />
      </Background>
    );
  }
}

SuccessPage.propTypes = {
  getContractId: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    getContractId: (value) => dispatch(getContractId(value)),
  };
}

const mapStateToProps = createStructuredSelector({});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'success', saga });
const withReducer = injectReducer({ key: 'success', reducer });

export default compose(
  withRouter,
  withSaga,
  withReducer,
  withConnect,
)(SuccessPage);
