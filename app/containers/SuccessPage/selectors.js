import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('success');

const makeSelectOrderId = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('orderId')
);

export {
  makeSelectOrderId,
};
