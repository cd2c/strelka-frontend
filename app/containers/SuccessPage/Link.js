import styled from 'styled-components';
import { Link as NormalLink } from 'react-router-dom';

const Link = styled(NormalLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 330px;

  height: 60px;
  background-color: transparent;
  position: relative;
  border: 1px solid #ddd;
  border-radius: 12px;
  cursor: pointer;
  transition: border-color .3s ease;
  text-align: center;

  color: #666;
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  text-decoration: none;
  letter-spacing: -0.5px;

  &:hover {
    border-color: #666;
  }

  &:active, &:focus {
    border-color: #666;
    outline: none;
  }

  @media (max-width: 840px) {
    width: 100%;
  }
`;

export default Link;
