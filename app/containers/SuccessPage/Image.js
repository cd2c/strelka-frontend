import styled from 'styled-components';

const Image = styled.img`
  margin-top: auto;
  margin-left: auto;
  margin-right: auto;
  width: calc((100px + 100%) / 1.5);
`;

export default Image;
