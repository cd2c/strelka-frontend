import React from 'react';
import { Helmet } from 'react-helmet';

import Background from 'components/Background';
import Header from 'components/Header';
import Text from 'components/Text';
import H2 from 'components/H2';
import Footer from 'components/Footer';
import Step from 'components/Step';
import Link from 'components/Link';

import FailPageWrapper from './FailPageWrapper';
import FailText from './FailText';

const FailPage = () => (
  <Background className="not-main">
    <Helmet>
      <title>ошибка</title>
    </Helmet>
    <Header isMain={false} />
    <FailPageWrapper>
      <Step step="fail" />
      <FailText>
        <H2 className="black">Что-то пошло не так</H2>
        <Text className="success-text">
          В процессе оплаты полиса произошла ошибка. <Link to="/feedback">Обратитесь к нам</Link> для более подробной информации.
        </Text>
      </FailText>
    </FailPageWrapper>
    <Footer isMain={false} />
  </Background>
);

export default FailPage;
