import { call, put, select, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';

import {
  SEND_REMIND,
} from './constants';

import {
  makeSelectEmail,
  makeSelectDate,
} from './selectors';

import {
  sendRemindSuccess,
  sendRemindFail,
} from './actions';

function* remindSaga() {
  const email = yield select(makeSelectEmail());
  const dateEndText = yield select(makeSelectDate());

  const remind = {
    email,
    dateEndText,
  };

  const requestURL = 'remind';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(remind),
  };

  try {
    yield call(request, requestURL, options);
    yield put(sendRemindSuccess());
  } catch (err) {
    yield put(sendRemindFail(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* rootData() {
  yield takeEvery(SEND_REMIND, remindSaga);
}
