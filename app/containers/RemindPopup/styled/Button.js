import styled from 'styled-components';
import { lighten } from 'polished';


const Button = styled.button`
  &.send {
    align-items: center;
    background-color: #07A1F4;
    border-radius: 12px;
    display: flex;
    cursor: pointer;
    position: relative;
    
    color: #fff;
    font-family: 'Ubuntu', sans-serif;
    font-size: 20px;
    font-weight: 300;
    height: 60px;
    justify-content: center;
    letter-spacing: -0.5px;
    padding: 0 30px;
    text-decoration: none;
    transition: background-color .3s ease;
  
    &:hover {
      background-color: ${lighten(0.1, '#07A1F4')};
    }
    
    &:active, &:focus {
      border: none;
      outline: none;
    }

    &--onload {
      background-color: #f0f0f0;

      &:hover {
        background-color: #f0f0f0;
      }
    }

    @media (max-width: 840px) {
      width: 100%;
    }
  }
`;

export default Button;
