import styled from 'styled-components';

const Header = styled.p`
  font-family: 'Ubuntu', sans-serif;
  font-size: 20px;
  font-weight: 300;
  color: #404040;
  letter-spacing: -0.5px;
  margin: 0;
`;

export default Header;
