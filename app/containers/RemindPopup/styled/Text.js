import styled from 'styled-components';

const Text = styled.p`
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 18px;
  margin-top: 10px;
`;

export default Text;
