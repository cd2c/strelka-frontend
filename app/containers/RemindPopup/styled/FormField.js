import styled from 'styled-components';

const FormField = styled.div`
  display: flex;
  justify-content: ${(props) => props.center ? 'center' : 'flex-start'};
  margin-top: 10px;

  &:last-child {
    margin-top: 20px;
  }
`;

export default FormField;
