import styled from 'styled-components';

const Popup = styled.div`
  background: #FFFFFF;
  box-shadow: 0 0 23px 0 rgba(0, 0, 0, .2);
  left: 50%;
  margin: auto;
  max-width: 600px;
  min-width: 280px;
  padding: 30px;
  position: fixed;
  opacity: ${(props) => props.active ? '1' : '0'};
  transform: translate(-50%, -50%);
  transition: opacity, z-index .3s ease-out;
  top: 50%;
  width: 100%;
  z-index: ${(props) => props.active ? '100' : '-1'};

  @media (max-width: 840px) {
    display: flex;
    flex-direction: column;
    height: 100vh;
    justify-content: center;
    left: 0;
    top: 0;
    transform: none;
  }
`;

export default Popup;
