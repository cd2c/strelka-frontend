import {
  CHANGE_EMAIL,
  CHANGE_DATE,
  CHANGE_WAS_SEND,
  SEND_REMIND,
  SEND_REMIND_SUCCESS,
  SEND_REMIND_ERROR,
} from './constants';

export function changeEmail(payload) {
  return {
    type: CHANGE_EMAIL,
    ...payload,
  };
}

export function changeDate(payload) {
  return {
    type: CHANGE_DATE,
    ...payload,
  };
}

export function changeWasSend(payload) {
  return {
    type: CHANGE_WAS_SEND,
    ...payload,
  };
}

export function sendRemind() {
  return {
    type: SEND_REMIND,
  };
}

export function sendRemindSuccess() {
  return {
    type: SEND_REMIND_SUCCESS,
  };
}

export function sendRemindFail() {
  return {
    type: SEND_REMIND_ERROR,
  };
}
