import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Spinner from 'react-spinkit';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { validator } from 'utils/validation';

import Input from 'components/Input';

import saga from './saga';
import reducer from './reducer';

import {
  changeEmail,
  changeDate,
  sendRemind,
} from './actions';

import {
  makeSelectEmail,
  makeSelectDate,
  makeSelectLoading,
  makeSelectWasSend,
} from './selectors';

import Button from './styled/Button';
import Center from './styled/Center';
import Close from './styled/Close';
import Form from './styled/Form';
import FormField from './styled/FormField';
import Header from './styled/Header';
import Popup from './styled/Popup';
import Text from './styled/Text';

class RemindPopup extends React.Component {
  state = {
    emailInputError: false,
    dateEndInputError: false,
  };

  changeEmail = (event) => {
    this.setState({
      emailInputError: false,
    });

    this.props.changeEmail({
      email: event.target.value,
    });
  };

  changeDate = (event) => {
    this.setState({
      dateEndInputError: false,
    });

    this.props.changeDate({
      date: event.target.value,
    });
  };

  handleForm = (event) => {
    event.preventDefault();
    let isValid = true;

    if (!validator.email(this.props.email)) {
      isValid = false;

      this.setState({
        emailInputError: true,
      });
    }

    if (!validator.datePoliceEnd(this.props.date)) {
      isValid = false;

      this.setState({
        dateEndInputError: true,
      });
    }

    if (isValid) {
      this.props.sendRemind();
    }
  };

  render() {
    let buttonText = 'Напомнить';
    if (this.props.loading) {
      buttonText = (
        <Center>
          <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
        </Center>
      );
    }

    let mainText = (
      <div>
        <Text>
          Заполните данные и мы пришлем вам напоминание о необходимости продлить полис
        </Text>
        <Form onSubmit={this.handleForm}>
          <FormField>
            <Input
              className={this.state.emailInputError ? 'error' : ''}
              id="email"
              label="Email"
              onChange={this.changeEmail}
              value={this.props.email}
              disabled={this.props.loading}
            />
          </FormField>
          <FormField>
            <Input
              className={this.state.dateEndInputError ? 'error' : ''}
              id="dateEnd"
              label="Дата окончания полиса"
              mask="99.99.9999"
              maskChar=""
              onChange={this.changeDate}
              value={this.props.date}
              disabled={this.props.loading}
            />
          </FormField>
          <FormField center>
            <Button
              disabled={this.props.loading}
              className={this.props.loading ? 'send send--onload' : 'send'}
              type="submit"
            >
              {buttonText}
            </Button>
          </FormField>
        </Form>
      </div>
    );
    if (this.props.wasSend) {
      mainText = (
        <Text>
          Спасибо! Мы напомним вам о продлении полиса заранее!
        </Text>
      );
    }

    return (
      <Popup active={this.props.isActive}>
        <Close onClick={this.props.onClose} />
        <Header>
          Напомнить продлить ОСАГО
        </Header>

        {mainText}
      </Popup>
    );
  }
}

RemindPopup.propTypes = {
  changeDate: PropTypes.func,
  changeEmail: PropTypes.func,
  date: PropTypes.string,
  email: PropTypes.string,
  isActive: PropTypes.bool,
  loading: PropTypes.bool,
  onClose: PropTypes.func,
  sendRemind: PropTypes.func,
  wasSend: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  changeDate: (value) => dispatch(changeDate(value)),
  changeEmail: (value) => dispatch(changeEmail(value)),
  sendRemind: () => dispatch(sendRemind()),
});

const mapStateToProps = createStructuredSelector({
  email: makeSelectEmail(),
  date: makeSelectDate(),
  loading: makeSelectLoading(),
  wasSend: makeSelectWasSend(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'remindPopup', saga });
const withReducer = injectReducer({ key: 'remindPopup', reducer });

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(RemindPopup);
