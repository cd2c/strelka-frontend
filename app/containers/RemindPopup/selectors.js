import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('remindPopup');

const makeSelectEmail = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('email')
);

const makeSelectDate = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('date')
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectWasSend = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('wasSend')
);

export {
  makeSelectEmail,
  makeSelectDate,
  makeSelectLoading,
  makeSelectWasSend,
};
