export const CHANGE_EMAIL = 'e-osago/RemindPopup/CHANGE_EMAIL';
export const CHANGE_DATE = 'e-osago/RemindPopup/CHANGE_DATE';
export const CHANGE_WAS_SEND = 'e-osago/RemindPopup/CHANGE_WAS_SEND';

export const SEND_REMIND = 'e-osago/RemindPopup/SEND_REMIND';
export const SEND_REMIND_SUCCESS = 'e-osago/RemindPopup/SEND_REMIND_SUCCESS';
export const SEND_REMIND_ERROR = 'e-osago/RemindPopup/SEND_REMIND_ERROR';
