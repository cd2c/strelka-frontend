import styled from 'styled-components';
import { lighten } from 'polished';

const Link = styled.a`
  color: #772490;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  text-decoration: none;
  transition: color .3s ease-out;

  &:hover {
    color: ${lighten(0.1, '#782490')};
  }
`;

export default Link;
