import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  min-height: calc(100vh - 150px);
`;

export default Wrapper;
