import styled from 'styled-components';

const P = styled.p`
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 20px;
  padding: 0 20px;

  &:first-child {
    margin-top: 0;
  }
`;

export default P;
