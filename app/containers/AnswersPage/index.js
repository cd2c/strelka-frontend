import React from 'react';

import LandingBlock from 'components/LandingBlock';
import Footer from 'components/Footer';
import Header from 'components/Header';

import Answer from './Answer';
import Answers from './Answers';
import Headline from './Headline';
import Link from './Link';
import P from './P';
import Wrapper from './Wrapper';

class AnswersPage extends React.PureComponent {
  state = {
    openedAnswer: null,
  };

  toggleAnswer = (id) => {
    if (id === this.state.openedAnswer) {
      this.setState({
        openedAnswer: null,
      });
    } else {
      this.setState({
        openedAnswer: id,
      });
    }
  };

  render() {
    return (
      <div>
        <Header isMain />
        <Wrapper>
          <LandingBlock id="answers">
            <Headline>Вопрос-ответ</Headline>
            <Answers>
              <Answer
                id={2}
                isOpened={this.state.openedAnswer === 2}
                maxHeight="211px"
                onClick={this.toggleAnswer}
                title="Как получить диагностическую карту?"
              >
                <div>
                  <P>
                    Найдите в вашем городе ближайший пункт технического осмотра и пройдите проверку состояния вашего автомобиля. При необходимости - устраните все недостатки.
                  </P>
                  <P>
                    После успешного прохождения техосмотра вам выдадут диагностическую карту на определенный срок. Номер и срок действия диагностической карты необходим для оформления полиса ОСАГО.
                  </P>
                </div>
              </Answer>
              <Answer
                id={3}
                isOpened={this.state.openedAnswer === 3}
                maxHeight="115px"
                onClick={this.toggleAnswer}
                title="В каких случаях не нужна диагностическая карта?"
              >
                <div>
                  <P>
                    Диагностическая карта не требуется для транспортных средств не старше трех лет.
                  </P>
                </div>
              </Answer>
              <Answer
                id={4}
                isOpened={this.state.openedAnswer === 4}
                maxHeight="191px"
                onClick={this.toggleAnswer}
                title="Что делать, если попал в ДТП?"
              >
                <div>
                  <P>
                    Если вы попали в ДТП, сначала убедитесь, что нет пострадавших и жизни людей ничего не угрожает. Обратитесь в ту страховую компанию, в которой вы оформляли полис. Телефоны для обращения указаны в вашем полисе.
                  </P>
                  <P>
                    АльфаСтрахование - <b>8 800 333 0 999</b>
                    <br />
                    Ингосстрах - <b>8 495 956 55 55</b>
                  </P>
                </div>
              </Answer>
              <Answer
                id={5}
                isOpened={this.state.openedAnswer === 5}
                maxHeight="343px"
                onClick={this.toggleAnswer}
                title="Как защитить себя от мошенников?"
              >
                <div>
                  <P>К сожалению, с оформлением полисов ОСАГО онлайн связано много мошеннических сайтов.</P>
                  <P>
                    Страховой сервис Стрелка имеет официальные договоренности со страховыми компаниями. Мы не принимаем оплату на свой собственный счет. Вы производите оплату на специализированном сайте страховой компании.
                  </P>
                  <P>
                    Обратите внимание на адрес сайта, на котором производите оплату полиса:
                    для АльфаСтрахование - <b>https://securepayments.sberbank.ru/</b><br />
                    для Ингосстрах - <b>https://www.ingos.ru/</b>
                  </P>
                  <P>
                    Не производите оплату за полис, если адрес сайта отличается или кажется вам подозрительным!
                  </P>
                </div>
              </Answer>
              <Answer
                id={6}
                isOpened={this.state.openedAnswer === 6}
                maxHeight="171px"
                onClick={this.toggleAnswer}
                title="Чем отличается ОСАГО от Е-ОСАГО?"
              >
                <div>
                  <P>Полис ОСАГО и Е-ОСАГО отличается только способом оформления.</P>
                  <P>Е-ОСАГО - это тот же полис ОСАГО (Обязательного Страхования Автогражданской Ответственности), который оформлен электронно - через интернет.</P>
                </div>
              </Answer>
              <Answer
                id={7}
                isOpened={this.state.openedAnswer === 7}
                maxHeight="115px"
                onClick={this.toggleAnswer}
                title="Как проверить подлинность полиса ОСАГО?"
              >
                <div>
                  <P>
                    Полисы ОСАГО, оформленные через интернет можно проверить на сайте <noindex><Link href="https://dkbm-web.autoins.ru/dkbm-web-1.0/bsostate.htm" rel="nofollow" target="_blank">Российского Союза Автостраховщиков</Link></noindex>
                  </P>
                </div>
              </Answer>
              <Answer
                id={8}
                isOpened={this.state.openedAnswer === 8}
                maxHeight="267px"
                onClick={this.toggleAnswer}
                title="Зачем оформлять полис в Стрелке?"
              >
                <div>
                  <P>
                    Да, вы можете оформить полис на официальном сайте страховой компании, мы вас не ограничиваем :)
                  </P>
                  <P>
                    Полис, оформленный в Стрелке ничем не отличается от полиса, оформленного на официальном сайте страховой компании.
                    Сервис Стрелка - удобнее. Вы заполняете данные для полиса один раз, мы отправляем запрос в разные страховые компании, вам остается только выбрать и оплатить полис.
                  </P>
                  <P>
                    Это экономит время и нервы.
                  </P>
                </div>
              </Answer>
              <Answer
                id={9}
                isOpened={this.state.openedAnswer === 9}
                maxHeight="151px"
                onClick={this.toggleAnswer}
                title="Как оформить полис ОСАГО для юридических лиц?"
              >
                <div>
                  <P>
                    Для оформления полиса ОСАГО юридическому лицу необходимо обратиться в страховую компанию.
                  </P>
                  <P>
                    Стрелка помогает в оформление полиса только физическим лицам.
                  </P>
                </div>
              </Answer>
              <Answer
                id={10}
                isOpened={this.state.openedAnswer === 10}
                maxHeight="115px"
                onClick={this.toggleAnswer}
                title="А как же страхование жизни?"
              >
                <div>
                  <P>
                    Мы не вынуждаем своих клиентов оформлять дополнительные страховые услуги при оформление полиса ОСАГО.
                  </P>
                </div>
              </Answer>
              <Answer
                id={11}
                isOpened={this.state.openedAnswer === 11}
                maxHeight="172px"
                onClick={this.toggleAnswer}
                title="Почему доступны не все страховые компании?"
              >
                <div>
                  <P>
                    По техническим причинам (например, долгого ответа от сервера страховой компании) бывают доступны не все страховые компании.
                  </P>
                  <P>
                    Вы можете повторить запрос через некоторое время или выбрать компанию из доступных и оплатить полис.
                  </P>
                </div>
              </Answer>
            </Answers>
          </LandingBlock>
        </Wrapper>
        <Footer />
      </div>
    );
  }
}

export default AnswersPage;
