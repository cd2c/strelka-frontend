import { HashLink } from 'react-router-hash-link';
import styled from 'styled-components';
import { lighten } from 'polished';

const Link = styled(HashLink)`
  color: #07a1f4;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -0.5px;
  text-decoration: none;
  transition: all .3s ease-out;

  &:hover {
    color: ${lighten(0.1, '#07a1f4')};
  }
`;

export default Link;
