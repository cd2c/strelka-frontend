import React, { Component } from 'react';
import styled from 'styled-components';

import H3 from 'components/H3';

import Answer from './Answer';
import Answers from './Answers';

const Flex = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  margin: 10px 0;
  padding: 0 20px;
  width: 100%;
`;

const FlexItem = styled.div`
  font-family: 'Ubuntu', sans-serif;
  font-weight: 300;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 18px;
  margin-right: 20px;
  width: 210px;

  &:last-child {
    font-weight: 400;
    margin-right: 0;
  }
`;

const Title = styled.div`
  color: #404040;
  font-family: 'Ubuntu', sans-serif;
  font-weight: 300;
  font-size: 18px;
  letter-spacing: -.5px;
  margin: 20px 0;
  padding: 0 20px;
`;

const Table = styled.div`
  padding: 0 20px;
  margin: 10px 0 30px;
`;

const Tr = styled.div`
  border-top: 1px solid #DDDDDD;
  border-left: 1px solid #DDDDDD;
  border-right: 1px solid #DDDDDD;
  display: flex;
  width: 100%;

  &:last-child {
    border-bottom: 1px solid #DDDDDD;
  }
`;

const Td = styled.div`
  border-right: 1px solid #DDDDDD;
  color: #404040;
  font-size: 16px;
  letter-spacing: -0.5px;
  line-height: 18px;
  text-align: center;
  width: 10%;
  ${(props) => props.header ? 'background-color: #F2FAFE;font-weight: 300;padding: 10px 0;' : 'font-weight: 400;padding: 20px 0;'}

  &:last-child {
    border-right: none;
  }
`;

class CheckData extends Component {
  state = {
    openedAnswer: null,
  };

  toggleAnswer = (id) => {
    if (id === this.state.openedAnswer) {
      this.setState({
        openedAnswer: null,
      });
    } else {
      this.setState({
        openedAnswer: id,
      });
    }
  };

  render() {
    return (
      <div>
        <H3>Проверить данные</H3>
        <Answers>
          <Answer
            id={1}
            isOpened={this.state.openedAnswer === 1}
            maxHeight="811px"
            onClick={this.toggleAnswer}
            title="Информация об автомобиле"
          >
            <Flex>
              <FlexItem>
                Марка
              </FlexItem>
              <FlexItem>
                Mercedes-Benz
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Модель
              </FlexItem>
              <FlexItem>
                C 200
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Год выпуска
              </FlexItem>
              <FlexItem>
                2016
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Категория авто
              </FlexItem>
              <FlexItem>
                B - легковые
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Тип авто
              </FlexItem>
              <FlexItem>
                Легковой а/м
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Цель использования
              </FlexItem>
              <FlexItem>
                Личные
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Гос номер
              </FlexItem>
              <FlexItem>
                К 070 КТ 54
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Тип документа
              </FlexItem>
              <FlexItem>
                Свидетельство ТС
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Серия, номер ТС
              </FlexItem>
              <FlexItem>
                5414 155944
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Дата выдачи СТС
              </FlexItem>
              <FlexItem>
                21.10.2014
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                VIN
              </FlexItem>
              <FlexItem>
                WDB2030451A13FA93
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Мощность двигателя
              </FlexItem>
              <FlexItem>
                163
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Используется с прицепом
              </FlexItem>
              <FlexItem>
                Нет
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Номер диагностической карты
              </FlexItem>
              <FlexItem>
                123102394230143412341
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Срок действия диагностической карты
              </FlexItem>
              <FlexItem>
                11.10.2019
              </FlexItem>
            </Flex>
          </Answer>
          <Answer
            id={2}
            isOpened={this.state.openedAnswer === 2}
            maxHeight="811px"
            onClick={this.toggleAnswer}
            title="Информация о собственнике и страхователе"
          >
            <Title>
              Собственник
            </Title>
            <Flex>
              <FlexItem>
                Фамилия
              </FlexItem>
              <FlexItem>
                Пономарев
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Имя
              </FlexItem>
              <FlexItem>
                Владислав
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Отчество
              </FlexItem>
              <FlexItem>
                Витальевич
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Дата рождения
              </FlexItem>
              <FlexItem>
                28.11.1995
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Серия, номер паспорта
              </FlexItem>
              <FlexItem>
                5017 666424
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Регион прописки
              </FlexItem>
              <FlexItem>
                Новосибирская область
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Населенный пункт
              </FlexItem>
              <FlexItem>
                г. Новосибирск
              </FlexItem>
            </Flex>

            <Title>
              Страхователь
            </Title>
            <Flex>
              <FlexItem>
                Фамилия
              </FlexItem>
              <FlexItem>
                Пономарев
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Имя
              </FlexItem>
              <FlexItem>
                Владислав
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Отчество
              </FlexItem>
              <FlexItem>
                Витальевич
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Дата рождения
              </FlexItem>
              <FlexItem>
                28.11.1995
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Серия, номер паспорта
              </FlexItem>
              <FlexItem>
                5017 666424
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Регион прописки
              </FlexItem>
              <FlexItem>
                Новосибирская область
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Населенный пункт
              </FlexItem>
              <FlexItem>
                г. Новосибирск
              </FlexItem>
            </Flex>
          </Answer>
          <Answer
            id={3}
            isOpened={this.state.openedAnswer === 3}
            maxHeight="811px"
            onClick={this.toggleAnswer}
            title="Информация о водителях"
          >
            <Title>
              Водитель №1
            </Title>
            <Flex>
              <FlexItem>
                Фамилия
              </FlexItem>
              <FlexItem>
                Пономарев
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Имя
              </FlexItem>
              <FlexItem>
                Владислав
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Отчество
              </FlexItem>
              <FlexItem>
                Витальевич
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Дата рождения
              </FlexItem>
              <FlexItem>
                28.11.1995
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Дата начала водительского стажа
              </FlexItem>
              <FlexItem>
                28.01.2014
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                Серия, номер ВУ
              </FlexItem>
              <FlexItem>
                5017 666424
              </FlexItem>
            </Flex>
          </Answer>
          <Answer
            id={4}
            isOpened={this.state.openedAnswer === 4}
            isOpenedWhite
            maxHeight="811px"
            onClick={this.toggleAnswer}
            title="Таблица коэффициентов"
          >
            <Table>
              <Tr>
                <Td header>Тб</Td>
                <Td header>Кт</Td>
                <Td header>Кбм</Td>
                <Td header>Квс</Td>
                <Td header>Кс</Td>
                <Td header>Кп</Td>
                <Td header>Км</Td>
                <Td header>Кн</Td>
                <Td header>Ко</Td>
                <Td header>Кпр</Td>
              </Tr>
              <Tr>
                <Td>4118</Td>
                <Td>1.1</Td>
                <Td>0.9</Td>
                <Td>1</Td>
                <Td>1</Td>
                <Td>1</Td>
                <Td>1.4</Td>
                <Td>1</Td>
                <Td>1</Td>
                <Td>1</Td>
              </Tr>
            </Table>
          </Answer>
        </Answers>
      </div>
    );
  }
}

export default CheckData;

