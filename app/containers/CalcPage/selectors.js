import { createSelector } from 'reselect';

const selectCalc = (state) => state.get('calc');

const makeSelectStartDate = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('startDate')
);

const makeSelectCost = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('cost')
);

const makeSelectPhone = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('phone')
);

const makeSelectEmail = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('email')
);

const makeSelectAgree = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('agree')
);

const makeSelectApiId = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('apiId')
);

const makeSelectCalcLoading = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('calcLoading')
);

const makeSelectLoading = () => createSelector(
  selectCalc,
  (calcState) => calcState.get('loading')
);

export {
  selectCalc,
  makeSelectStartDate,
  makeSelectCost,
  makeSelectPhone,
  makeSelectEmail,
  makeSelectAgree,
  makeSelectApiId,
  makeSelectCalcLoading,
  makeSelectLoading,
};
