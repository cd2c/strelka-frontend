import {
  CHANGE_CALC_START_DATE,
  CHANGE_CALC_PHONE,
  CHANGE_CALC_EMAIL,
  CHANGE_CALC_AGREE,
  CHECK_CALC,
  CHECK_CALC_SUCCESS,
  CHECK_CALC_ERROR,
  LOAD_CONTRACT,
  LOAD_CONTRACT_SUCCESS,
  LOAD_CONTRACT_ERROR,
} from './constants';

export function changeStartDate(startDate) {
  return {
    type: CHANGE_CALC_START_DATE,
    startDate,
  };
}

export function changePhone(phone) {
  return {
    type: CHANGE_CALC_PHONE,
    phone,
  };
}

export function changeEmail(email) {
  return {
    type: CHANGE_CALC_EMAIL,
    email,
  };
}

export function changeAgree(agree) {
  return {
    type: CHANGE_CALC_AGREE,
    agree,
  };
}

export function checkCalc() {
  return {
    type: CHECK_CALC,
  };
}

export function checkCalcSuccess(cost, apiId) {
  return {
    type: CHECK_CALC_SUCCESS,
    cost,
    apiId,
  };
}

export function checkCalcError(error) {
  return {
    type: CHECK_CALC_ERROR,
    error,
  };
}

export function loadContract() {
  return {
    type: LOAD_CONTRACT,
  };
}

export function loadContractSuccess(id) {
  return {
    type: LOAD_CONTRACT_SUCCESS,
    id,
  };
}

export function loadContractError(error) {
  return {
    type: LOAD_CONTRACT_ERROR,
    error,
  };
}
