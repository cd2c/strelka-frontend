import { fromJS } from 'immutable';

import {
  CHANGE_CALC_START_DATE,
  CHANGE_CALC_PHONE,
  CHANGE_CALC_EMAIL,
  CHANGE_CALC_AGREE,
  CHECK_CALC,
  CHECK_CALC_SUCCESS,
  CHECK_CALC_ERROR,
  LOAD_CONTRACT,
  LOAD_CONTRACT_SUCCESS,
  LOAD_CONTRACT_ERROR,
} from './constants';

const initialState = fromJS({
  phone: false,
  email: false,
  agree: false,
  cost: false,
  startDate: '29.07.2018',
  apiId: false,
  loading: false,
  calcLoading: false,
  error: false,
  contractId: false,
});

function calcReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_CALC_START_DATE:
      return state
        .set('startDate', action.startDate);
    case CHANGE_CALC_PHONE:
      return state
        .set('phone', action.phone);
    case CHANGE_CALC_EMAIL:
      return state
        .set('email', action.email);
    case CHANGE_CALC_AGREE:
      return state
        .set('agree', action.agree);
    case CHECK_CALC:
      return state
        .set('calcLoading', true)
        .set('error', false)
        .set('cost', false)
        .set('apiId', false);
    case CHECK_CALC_SUCCESS:
      return state
        .set('calcLoading', false)
        .set('cost', action.cost)
        .set('apiId', action.apiId);
    case CHECK_CALC_ERROR:
      return state
        .set('calcLoading', false)
        .set('error', action.error);
    case LOAD_CONTRACT:
      return state
        .set('loading', true)
        .set('error', false)
        .set('contractId', false);
    case LOAD_CONTRACT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('contractId', action.id);
    case LOAD_CONTRACT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error);
    default:
      return state;
  }
}

export default calcReducer;
