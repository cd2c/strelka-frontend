import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const InsurerWrapper = styled.div`
  background-color: ${(props) => props.isUnavailable ? '#F0F0F0' : '#FFFFFF'};
  background-image: url(${(props) => props.logo});
  background-position: center;
  background-size: contain;
  border: ${(props) => {
    if (props.isSelected) {
      return '2px solid #07a1f4';
    } else if (props.isUnavailable) {
      return '1px solid #DDDDDD';
    }

    return '1px solid #dddddd';
  }};
  border-radius: 5px;
  box-sizing: border-box;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  height: 150px;
  padding: ${(props) => props.isSelected ? '0 15px' : '1px 16px'};
  width: calc(50% - 10px);

  @media (max-width: 840px) {
    width: calc(50% - 5px);
  }
`;

const CostWrapper = styled.div`
  border-top: 1px solid #DDDDDD;
  margin: auto auto 0 auto;
  padding-bottom: 10px;
  width: 100%;
`;

const Label = styled.div`
  color: #666666;
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  font-weight: 300;
  letter-spacing: -.2px;
  margin: 10px 0 0;
  text-align: center;
`;

const Cost = styled.div`
  color: ${(props) => props.isUnavailable ? '#666666' : '#404040'};
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  letter-spacing: -.5px;
  line-height: 18px;
  text-align: center;
`;

const Logo = styled.div`
  background-image: url(${(props) => props.image});
  background-position: center;
  background-size: contain;
  height: 90px;
  margin-bottom: auto;
  margin-top: auto;
  opacity: ${(props) => props.isUnavailable ? '.4' : '1'};
  filter: ${(props) => props.isUnavailable ? 'saturate(0)' : 'saturate(1)'};
  transition: .3s opacity ease-out;
  width: 100%;
`;

class Insurer extends React.PureComponent {
  selectInsurer = () => {
    if (this.props.cost > 0) {
      this.props.onClick(this.props.id);
    }
  };

  render() {
    let isUnavailable = true;
    let cost = 'нет данных';

    if (this.props.cost > 0) {
      isUnavailable = false;
      cost = `${this.props.cost.toLocaleString()} ₽`;
    }

    return (
      <InsurerWrapper
        isSelected={this.props.isSelected}
        isUnavailable={isUnavailable}
        onClick={this.selectInsurer}
      >
        <Logo
          image={this.props.logo}
          isUnavailable={isUnavailable}
        />
        <CostWrapper>
          <Label>Стоимость полиса</Label>
          <Cost
            isUnavailable={isUnavailable}
          >{cost}</Cost>
        </CostWrapper>
      </InsurerWrapper>
    );
  }
}

Insurer.defaultProps = {
  cost: 0,
};

Insurer.propTypes = {
  cost: PropTypes.number,
  id: PropTypes.number,
  isSelected: PropTypes.bool,
  logo: PropTypes.string,
  onClick: PropTypes.func,
};

export default Insurer;
