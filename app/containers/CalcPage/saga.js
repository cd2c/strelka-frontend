import { call, put, select, takeEvery } from 'redux-saga/effects';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import * as Sentry from '@sentry/browser';
import request from 'utils/request';
import moment from 'moment';

import {
  makeSelectOwnerApiId,
  makeSelectInsurerApiId,
} from 'containers/OwnerPage/selectors';

import {
  makeSelectApiId,
} from 'containers/HomePage/selectors';

import {
  makeSelectDriversList,
  makeSelectMultidrive,
} from 'containers/DriverPage/selectors';

import { changeEmail, changePhone, changeCalcId } from 'containers/FeedbackPage/actions';

import {
  CHANGE_CALC_PHONE,
  CHANGE_CALC_EMAIL,
  CHECK_CALC,
  LOAD_CONTRACT,
} from './constants';
import {
  changeStartDate,
  checkCalcSuccess,
  checkCalcError,
  loadContractSuccess,
  loadContractError,
} from './actions';

import {
  makeSelectStartDate,
  makeSelectPhone,
  makeSelectEmail,
  makeSelectApiId as makeSelectContractId,
} from './selectors';

export function* checkCalc() {
  let startFromState = yield select(makeSelectStartDate());

  if (startFromState === false) {
    startFromState = moment().add(1, 'd').format('DD.MM.YYYY');
    yield put(changeStartDate(startFromState));
  }

  const startDateText = moment(startFromState, 'DD.MM.YYYY').format('YYYY-MM-DD');
  const endDateText = moment(startFromState, 'DD.MM.YYYY').subtract(1, 'd').add(1, 'y').format('YYYY-MM-DD');
  const ownerId = yield select(makeSelectOwnerApiId());
  const insurerId = yield select(makeSelectInsurerApiId());
  const vehicleId = yield select(makeSelectApiId());
  const multidrive = yield select(makeSelectMultidrive());

  let drivers = [];
  if (!multidrive) {
    const driversList = yield select(makeSelectDriversList());
    drivers = driversList.filter((item) => item.get('isShowed') === true).map((item) => item.get('apiId')).toArray();
  }

  const calc = {
    startDateText,
    endDateText,
    ownerId,
    insurerId,
    vehicleId,
    drivers,
  };

  const requestURL = 'calc-osago';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(calc),
  };

  try {
    const checkResult = yield call(request, requestURL, options);

    if (process.env.NODE_ENV !== 'development' && checkResult.insuranceBonus !== 0) {
      try {
        window.yaCounter47277876.reachGoal('calc');
      } catch (e) {
        console.log(e);
      }
    }
    yield put(checkCalcSuccess(checkResult.insuranceBonus, checkResult.id));
    yield put(changeCalcId(checkResult.id));
  } catch (err) {
    if (process.env.NODE_ENV === 'production') {
      Sentry.captureException(err);
    }
    yield put(checkCalcError(err));
  }
}

export function* loadContract() {
  const startFromState = yield select(makeSelectStartDate());

  const phone = yield select(makeSelectPhone());
  const email = yield select(makeSelectEmail());
  const dateRevisionText = moment().format('YYYY-MM-DD');
  const dateActionBegText = moment(startFromState, 'DD.MM.YYYY').format('YYYY-MM-DD');
  const dateActionEndText = moment(startFromState, 'DD.MM.YYYY').subtract(1, 'd').add(1, 'y').format('YYYY-MM-DD');
  const calcId = yield select(makeSelectContractId());

  const contract = {
    phone,
    email,
    dateRevisionText,
    dateActionBegText,
    dateActionEndText,
    calcId,
  };

  const requestURL = 'load-contract';
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(contract),
  };

  try {
    yield put(showLoading());
    const result = yield call(request, requestURL, options);
    yield put(loadContractSuccess(result.id));
    yield sendToPaymentPage(result.paymentURL);
  } catch (err) {
    if (process.env.NODE_ENV === 'production') {
      Sentry.captureException(err);
    }
    yield put(loadContractError(err));
  } finally {
    yield put(hideLoading());
  }
}

export function* sendToPaymentPage(paymentUrl) {
  window.location.replace(paymentUrl);
}

export function* changePhoneSaga() {
  const phone = yield select(makeSelectPhone());

  yield put(changePhone(phone));
}

export function* changeEmailSaga() {
  const email = yield select(makeSelectEmail());

  yield put(changeEmail(email));
}

export default function* rootData() {
  yield takeEvery(CHECK_CALC, checkCalc);
  yield takeEvery(LOAD_CONTRACT, loadContract);
  yield takeEvery(CHANGE_CALC_EMAIL, changeEmailSaga);
  yield takeEvery(CHANGE_CALC_PHONE, changePhoneSaga);
}
