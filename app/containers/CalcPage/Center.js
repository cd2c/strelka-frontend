import styled from 'styled-components';

const Center = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  margin: 20px 0;
  width: 100%;
`;

export default Center;
