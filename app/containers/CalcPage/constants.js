export const CHANGE_CALC_START_DATE = 'e-osago/Calc/CHANGE_CALC_START_DATE';
export const CHANGE_CALC_PHONE = 'e-osago/Calc/CHANGE_CALC_PHONE';
export const CHANGE_CALC_EMAIL = 'e-osago/Calc/CHANGE_CALC_EMAIL';
export const CHANGE_CALC_AGREE = 'e-osago/Calc/CHANGE_CALC_AGREE';

export const CHECK_CALC = 'e-osago/Calc/CHECK_CALC';
export const CHECK_CALC_SUCCESS = 'e-osago/Calc/CHECK_CALC_SUCCESS';
export const CHECK_CALC_ERROR = 'e-osago/Calc/CHECK_CALC_ERROR';

export const LOAD_CONTRACT = 'e-osago/Calc/LOAD_CONTRACT';
export const LOAD_CONTRACT_SUCCESS = 'e-osago/Calc/LOAD_CONTRACT_SUCCESS';
export const LOAD_CONTRACT_ERROR = 'e-osago/Calc/LOAD_CONTRACT_ERROR';
