import React from 'react';
import PropTypes from 'prop-types';
import { injectGlobal } from 'styled-components';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Helmet } from 'react-helmet';
import moment from 'moment';
import 'moment/src/locale/ru';
import isNull from 'lodash/isNull';
import Spinner from 'react-spinkit';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { validator } from 'utils/validation';

import Background from 'components/Background';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Step from 'components/Step';
import Form from 'components/Form';
import FormGroup from 'components/FormGroup';
import FormElement from 'components/FormElement';
import Input from 'components/Input';
import H3 from 'components/H3';
import Switch from 'components/Switch';
import Button from 'components/Button';
import InputValidity from 'components/InputValidity';

import reducer from './reducer';
import saga from './saga';
import {
  makeSelectStartDate,
  makeSelectCost,
  makeSelectPhone,
  makeSelectEmail,
  makeSelectAgree,
  makeSelectCalcLoading,
  makeSelectLoading,
} from './selectors';
import {
  changeStartDate,
  changePhone,
  changeEmail,
  changeAgree,
  checkCalc,
  loadContract,
} from './actions';

import CalcPageWrapper from './CalcPageWrapper';
import CalcForm from './CalcForm';
import Center from './Center';
import Label from './Label';
import Link from './Link';
import Insurer from './Insurer';
import ButtonWrapper from './ButtonWrapper';
import Text from './Text';

import Calendar from './images/calendar.svg';
import LogoAS from './images/logo_alfa.png';
import LogoIngos from './images/logo_ingos.png';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  .react-datepicker-wrapper,
  .react-datepicker__input-container {
    display: block;
  }

  .react-datepicker-popper {
    z-index: 1000;
  }

  .react-datepicker__current-month {
    text-transform: capitalize;
  }

  .react-datepicker__day-name {
    text-transform: capitalize;
  }

  .react-datepicker__input-container {
    position: relative;

    &::after {
      content: '';
      display: block;
      position: absolute;
      right: 13px;
      top: 20px;
      height: 20px;
      width: 20px;
      background-image: url(${Calendar});
      background-size: contain;
    }
  }
`;

class CalcPage extends React.PureComponent {
  state = {
    startDate: null,
    startDateInputError: false,
    phoneInputError: false,
    emailInputError: false,
    agreeInputError: false,
    selectedInsurer: null,
  };

  changePhone = (event) => {
    this.setState({
      phoneInputError: false,
    });

    this.props.changePhone(event.target.value);
  };

  changeEmail = (event) => {
    this.setState({
      emailInputError: false,
    });

    this.props.changeEmail(event.target.value);
  };

  changeAgree = () => {
    this.setState({
      agreeInputError: false,
    });

    this.props.changeAgree(!this.props.agree);
  };

  blurStartDate = () => {
    if (validator.dateStart(this.props.startDate)) {
      this.props.checkCalc();
    } else {
      this.setState({
        startDateInputError: true,
      });
    }
  };

  validateForm = (event) => {
    event.preventDefault();
    let isValid = true;

    if (!validator.phone(this.props.phone)) {
      isValid = false;
      this.setState({
        phoneInputError: true,
      });
    }

    if (!validator.email(this.props.email)) {
      isValid = false;
      this.setState({
        emailInputError: true,
      });
    }

    if (this.props.agree === false) {
      isValid = false;
      this.setState({
        agreeInputError: true,
      });
    }

    if (!validator.dateStart(this.props.startDate)) {
      isValid = false;
      this.setState({
        startDateInputError: true,
      });
    }

    if (isValid) {
      this.props.loadContract();
    }
  };

  toggleInsurer = (id) => {
    if (id === this.state.selectedInsurer) {
      this.setState({
        selectedInsurer: null,
      });
    } else {
      this.setState({
        selectedInsurer: id,
      });
    }
  };

  changeDate = (date) => {
    this.setState({
      startDate: date,
      startDateInputError: false,
    });

    this.props.changeStartDate(date.format('DD.MM.YYYY'));
    this.props.checkCalc();
  };

  render() {
    const cost = this.props.cost ? this.props.cost : 0;
    const endDate = moment(this.props.startDate, 'DD.MM.YYYY').subtract(1, 'd').add(1, 'y').format('DD.MM.YYYY');

    let switchLabelClassName = '';
    if (this.props.agree) {
      switchLabelClassName += 'active';
    }

    if (this.state.agreeInputError) {
      switchLabelClassName += ' error';
    }

    let insurer = (
      <Center>
        <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
      </Center>
    );
    let button = null;
    let buttonText = 'Перейти к оплате';
    if (this.props.isLoading) {
      buttonText = (
        <Center>
          <Spinner name="chasing-dots" fadeIn="none" color="#772490" />
        </Center>
      );
    }
    let text = '';
    if (!this.props.insurersLoading) {
      if (cost === 0 && !isNull(this.state.startDate)) {
        text = (
          <div>
            <Text>
              К сожалению, мы не получили информацию по вашему запросу.<br />
              <Link to="/#calc">Повторите</Link> попытку через некоторое время.
            </Text>
          </div>
        );
      }

      if (cost !== 0) {
        button = (
          <ButtonWrapper>
            <Button type="submit" className={this.props.agree && !isNull(this.state.selectedInsurer) && !this.props.isLoading ? 'next' : 'next next--disabled'} disabled={!this.props.agree} onClick={this.validateForm}>
              {buttonText}
            </Button>
          </ButtonWrapper>
        );
      }

      if (!isNull(this.state.startDate)) {
        insurer = (
          <FormGroup>
            <Insurer
              cost={cost}
              disabled={this.props.isLoading}
              id={1}
              isSelected={this.state.selectedInsurer === 1}
              logo={LogoAS}
              onClick={this.toggleInsurer}
            />
            <Insurer
              cost={null}
              disabled={this.props.isLoading}
              id={2}
              isSelected={this.state.selectedInsurer === 2}
              logo={LogoIngos}
              onClick={this.toggleInsurer}
            />
          </FormGroup>
        );
      } else {
        insurer = <div />;
      }
    }

    let info = null;
    if (!isNull(this.state.selectedInsurer)) {
      info = (
        <div>
          <H3>Информация о покупателе</H3>
          <FormGroup>
            <FormElement>
              <Input
                className={this.state.phoneInputError ? 'error' : ''}
                disabled={this.props.isLoading}
                id="phone"
                label="Номер телефона для уведомления от РСА"
                mobileLabel="Номер телефона"
                mask="+7 999 999-99-99"
                maskChar=""
                onChange={this.changePhone}
                placeholder="+7 000 000-00-00"
                value={this.props.phone}
              />
            </FormElement>
          </FormGroup>
          <FormGroup>
            <FormElement>
              <Input
                className={this.state.emailInputError ? 'error' : ''}
                disabled={this.props.isLoading}
                id="email"
                label="Электронная почта, куда будет отправлен полис"
                mobileLabel="Электронная почта"
                onChange={this.changeEmail}
                value={this.props.email}
              />
            </FormElement>
          </FormGroup>
          <FormGroup className="reverse">
            <FormElement className="flex">
              <Switch className="equal-insurer" onClick={this.changeAgree} on={this.props.agree} />
              <Label
                className={switchLabelClassName}
                htmlFor="isAgree"
                onClick={this.changeAgree}
              >
                Я согласен на передачу и обработку персональных данных
              </Label>
            </FormElement>
          </FormGroup>
        </div>
      );
    }

    const inputCalendar = (
      <Input
        className={this.state.startDateInputError ? 'error' : ''}
        id="startDate"
        label="Дата начала действия полиса"
        onChange={this.changeStartDate}
        placeholder="ДД.ММ.ГГГГ"
        disabled={this.props.isLoading}
        value={this.props.startDate}
      />
    );
    return (
      <Background className="not-main">
        <Helmet>
          <title>Расчет и выбор страховой компании</title>
        </Helmet>
        <Header isMain={false} />
        <CalcPageWrapper>
          <Step step="payment" />
          <CalcForm>
            <Form onSubmit={this.validateForm}>
              <H3>Данные полиса</H3>
              <FormGroup>
                <FormElement block>
                  <DatePicker
                    className="calendar"
                    customInput={inputCalendar}
                    minDate={moment().add('4', 'd')}
                    onChange={this.changeDate}
                    readOnly
                    selected={this.state.startDate}
                  />
                </FormElement>
              </FormGroup>
              <FormGroup>
                <FormElement>
                  <InputValidity
                    label="Срок действия"
                  >
                    1 год
                    {!isNull(this.state.startDate) &&
                      <span className="notify">до {endDate}</span>
                    }
                  </InputValidity>
                </FormElement>
              </FormGroup>
              {!isNull(this.state.startDate) &&
                <H3>Страховая компания</H3>
              }
              {insurer}
              {text}
              {info}
              {button}
            </Form>
          </CalcForm>
        </CalcPageWrapper>
        <Footer isMain={false} />
      </Background>
    );
  }
}

CalcPage.propTypes = {
  startDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  cost: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]),
  phone: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  email: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  agree: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  changeStartDate: PropTypes.func,
  changePhone: PropTypes.func,
  changeEmail: PropTypes.func,
  changeAgree: PropTypes.func,
  checkCalc: PropTypes.func,
  loadContract: PropTypes.func,
  insurersLoading: PropTypes.bool,
  isLoading: PropTypes.bool,
};

export function mapDispatchToProps(dispatch) {
  return {
    changeStartDate: (value) => dispatch(changeStartDate(value)),
    changePhone: (value) => dispatch(changePhone(value)),
    changeEmail: (value) => dispatch(changeEmail(value)),
    changeAgree: (value) => dispatch(changeAgree(value)),
    checkCalc: () => dispatch(checkCalc()),
    loadContract: () => dispatch(loadContract()),
  };
}

const mapStateToProps = createStructuredSelector({
  startDate: makeSelectStartDate(),
  cost: makeSelectCost(),
  phone: makeSelectPhone(),
  email: makeSelectEmail(),
  agree: makeSelectAgree(),
  insurersLoading: makeSelectCalcLoading(),
  isLoading: makeSelectLoading(),
});

const withSaga = injectSaga({ key: 'calc', saga });
const withReducer = injectReducer({ key: 'calc', reducer });
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withSaga,
  withReducer,
  withConnect,
)(CalcPage);
